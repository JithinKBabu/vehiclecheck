function addWidgetsContent() {
    Content.setDefaultUnit(kony.flex.DP);
    var flxMainContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "id": "flxMainContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "flxWhiteBgSkin",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxMainContainer.setDefaultUnit(kony.flex.DP);
    var flxHeaderContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "55dp",
        "id": "flxHeaderContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "flxBlueBgSkin",
        "top": "0dp",
        "width": "100%"
    }, {}, {});
    flxHeaderContainer.setDefaultUnit(kony.flex.DP);
    var imgBurgerIcon = new kony.ui.Image2({
        "centerY": "50%",
        "height": "35dp",
        "id": "imgBurgerIcon",
        "isVisible": true,
        "left": "4%",
        "skin": "slImage",
        "src": "burger_icon.png",
        "width": "35dp"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnMenuOpen = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "btnTransparentSkin",
        "height": "50dp",
        "id": "btnMenuOpen",
        "isVisible": true,
        "left": "2%",
        "onClick": AS_Button_7cbd927421e2489a96d6f2dfbffb1397,
        "skin": "btnTransparentSkin",
        "text": "Button",
        "width": "50dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": false,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblTitle = new kony.ui.Label({
        "centerY": "50.00%",
        "id": "lblTitle",
        "isVisible": true,
        "left": "61dp",
        "skin": "lblWhiteTitleSkin",
        "text": "Vehicle Check",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxHeaderContainer.add(imgBurgerIcon, btnMenuOpen, lblTitle);
    var flxContentContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "id": "flxContentContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "right": "0dp",
        "skin": "slFbox",
        "top": "55dp",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    flxContentContainer.setDefaultUnit(kony.flex.DP);
    var browserContent = new kony.ui.Browser({
        "bottom": "3%",
        "detectTelNumber": true,
        "enableZoom": false,
        "htmlString": "<p><b>Rentokil Pest Control</b> is a trading division of Rentokil Initial UK Limited, a company registered in England and Wales with registration number 301044 and whose registered office is at Riverbank, Meadows Business Park, Blackwater, Camberley, Surrey GU17 9AB.</p>",
        "id": "browserContent",
        "isVisible": true,
        "left": "4%",
        "right": "4%",
        "top": "3%",
        "width": "92%"
    }, {}, {});
    flxContentContainer.add(browserContent);
    flxMainContainer.add(flxHeaderContainer, flxContentContainer);
    var flxSettingsContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "centerX": "-50%",
        "clipBounds": true,
        "id": "flxSettingsContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": 0,
        "onClick": AS_FlexContainer_e865e39c1cde4d28b9529c0e68d7578c,
        "right": 0,
        "skin": "flxSettingsBgSkin",
        "top": "0dp",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    flxSettingsContainer.setDefaultUnit(kony.flex.DP);
    var flxBurgerLayer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "id": "flxBurgerLayer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "flxWhiteBgSkin",
        "top": "0dp",
        "width": "80%",
        "zIndex": 1
    }, {}, {});
    flxBurgerLayer.setDefaultUnit(kony.flex.DP);
    var flxTopContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "30%",
        "id": "flxTopContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "flxBlueBgSkin",
        "top": "0dp",
        "width": "100%"
    }, {}, {});
    flxTopContainer.setDefaultUnit(kony.flex.DP);
    var lblVersion = new kony.ui.Label({
        "id": "lblVersion",
        "isVisible": true,
        "right": "4%",
        "skin": "lbllWhiteTinySkin",
        "text": "V 1.0",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "4%",
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var imgLogo = new kony.ui.Image2({
        "height": "55dp",
        "id": "imgLogo",
        "isVisible": true,
        "left": "4%",
        "skin": "slImage",
        "src": "logo_rentokil.png",
        "top": "30%",
        "width": "122dp"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblUsername = new kony.ui.Label({
        "id": "lblUsername",
        "isVisible": true,
        "left": "5%",
        "skin": "lbllWhiteNormalSkin",
        "text": "SVeetil",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "72%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxTopContainer.add(lblVersion, imgLogo, lblUsername);
    var flxBottomContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "id": "flxBottomContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "29.98%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxBottomContainer.setDefaultUnit(kony.flex.DP);
    var flxContainerOne = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "46dp",
        "id": "flxContainerOne",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "onClick": AS_FlexContainer_8e7ecb0203e94467a817c072fb21cba5,
        "skin": "slFbox",
        "top": "3%",
        "width": "100%"
    }, {}, {});
    flxContainerOne.setDefaultUnit(kony.flex.DP);
    var imgIcon1 = new kony.ui.Image2({
        "centerY": "50.00%",
        "height": "40dp",
        "id": "imgIcon1",
        "isVisible": true,
        "left": "4%",
        "skin": "slImage",
        "src": "about_icon.png",
        "width": "40dp"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblAbout = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblAbout",
        "isVisible": true,
        "left": "60dp",
        "skin": "lblBlackBoldSkin",
        "text": "About",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxContainerOne.add(imgIcon1, lblAbout);
    var flxContainerTwo = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "46dp",
        "id": "flxContainerTwo",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "onClick": AS_FlexContainer_692d1b528c15446aaaafd8d135615f5e,
        "skin": "slFbox",
        "top": "15%",
        "width": "100%"
    }, {}, {});
    flxContainerTwo.setDefaultUnit(kony.flex.DP);
    var imgIcon2 = new kony.ui.Image2({
        "centerY": "50.00%",
        "height": "40dp",
        "id": "imgIcon2",
        "isVisible": true,
        "left": "4%",
        "skin": "slImage",
        "src": "sync_icon.png",
        "width": "40dp"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblSync = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblSync",
        "isVisible": true,
        "left": "60dp",
        "skin": "lblBlackBoldSkin",
        "text": "Sync",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxContainerTwo.add(imgIcon2, lblSync);
    var flxContainerThree = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "46dp",
        "id": "flxContainerThree",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "onClick": AS_FlexContainer_61e8e1e0f8aa4f729c904270f2213403,
        "skin": "slFbox",
        "top": "27%",
        "width": "100%"
    }, {}, {});
    flxContainerThree.setDefaultUnit(kony.flex.DP);
    var imgIcon3 = new kony.ui.Image2({
        "centerY": "50.00%",
        "height": "40dp",
        "id": "imgIcon3",
        "isVisible": true,
        "left": "4%",
        "skin": "slImage",
        "src": "logout_icon.png",
        "width": "40dp"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblLogout = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblLogout",
        "isVisible": true,
        "left": "60dp",
        "skin": "lblBlackBoldSkin",
        "text": "Logout",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxContainerThree.add(imgIcon3, lblLogout);
    var flxLastSyncContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "height": "60dp",
        "id": "flxLastSyncContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxLastSyncContainer.setDefaultUnit(kony.flex.DP);
    var lblLastSync = new kony.ui.Label({
        "centerY": "30%",
        "id": "lblLastSync",
        "isVisible": true,
        "left": "5%",
        "skin": "lblGrayNormalSkin",
        "text": "Last Sync:",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblLastSyncDate = new kony.ui.Label({
        "centerY": "65%",
        "id": "lblLastSyncDate",
        "isVisible": true,
        "left": "5%",
        "skin": "lblGrayNormalSkin",
        "text": "25/05/2016 10:20PM",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxLastSyncContainer.add(lblLastSync, lblLastSyncDate);
    flxBottomContainer.add(flxContainerOne, flxContainerTwo, flxContainerThree, flxLastSyncContainer);
    flxBurgerLayer.add(flxTopContainer, flxBottomContainer);
    flxSettingsContainer.add(flxBurgerLayer);
    Content.add(flxMainContainer, flxSettingsContainer);
};

function ContentGlobals() {
    Content = new kony.ui.Form2({
        "addWidgets": addWidgetsContent,
        "allowHorizontalBounce": false,
        "enabledForIdleTimeout": false,
        "id": "Content",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "postShow": AS_Form_25267fa367ab4843bad85aae3bb7ac5b,
        "preShow": AS_Form_c51fbd6104a14ea59b9e875276278efd,
        "skin": "BlueBgFormSkin",
        "verticalScrollIndicator": false
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": false,
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "onDeviceBack": AS_Form_015985df7e094525be30f489089d5df7,
        "retainScrollPosition": false,
        "titleBar": false,
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
    Content.info = {
        "kuid": "c8bc0df090704eb28cb189ca6d61b5f4"
    };
};