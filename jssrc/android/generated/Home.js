function addWidgetsHome() {
    Home.setDefaultUnit(kony.flex.DP);
    var flxMainContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "id": "flxMainContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "flxWhiteBgSkin",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxMainContainer.setDefaultUnit(kony.flex.DP);
    var flxHeaderContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "55dp",
        "id": "flxHeaderContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "flxBlueBgSkin",
        "top": "0dp",
        "width": "100%"
    }, {}, {});
    flxHeaderContainer.setDefaultUnit(kony.flex.DP);
    var imgBurgerIcon = new kony.ui.Image2({
        "centerY": "50%",
        "height": "70%",
        "id": "imgBurgerIcon",
        "isVisible": true,
        "left": "4%",
        "skin": "slImage",
        "src": "burger_icon.png",
        "width": "10%"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnMenuOpen = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "btnTransparentSkin",
        "height": "50dp",
        "id": "btnMenuOpen",
        "isVisible": true,
        "left": "2%",
        "onClick": AS_Button_7cbd927421e2489a96d6f2dfbffb1397,
        "skin": "btnTransparentSkin",
        "text": "Button",
        "width": "50dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": false,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblTitle = new kony.ui.Label({
        "centerY": "50.00%",
        "id": "lblTitle",
        "isVisible": true,
        "left": "61dp",
        "skin": "lblWhiteTitleSkin",
        "text": "Vehicle Check",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var camWidget = new kony.ui.Camera({
        "centerY": "50%",
        "height": "40dp",
        "id": "camWidget",
        "isVisible": true,
        "onCapture": AS_Button_c4c212e2791a4a2db1143c15cf6cb980,
        "right": "63dp",
        "skin": "CopyslCamera00898df47559f47",
        "width": "40dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "accessMode": constants.CAMERA_IMAGE_ACCESS_MODE_PUBLIC,
        "enableOverlay": false,
        "enablePhotoCropFeature": false
    });
    var imgSave = new kony.ui.Image2({
        "centerY": "50%",
        "height": "40dp",
        "id": "imgSave",
        "isVisible": true,
        "right": "3%",
        "skin": "slImage",
        "src": "icon_save.png",
        "width": "40dp"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnSave = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "btnTransparentSkin",
        "height": "50dp",
        "id": "btnSave",
        "isVisible": true,
        "onClick": AS_Button_7111904ae9264f2880c0137f7ea4e0c7,
        "right": "2%",
        "skin": "btnTransparentSkin",
        "text": "Button",
        "width": "50dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": false,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxHeaderContainer.add(imgBurgerIcon, btnMenuOpen, lblTitle, camWidget, imgSave, btnSave);
    var flxContentContiner = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "id": "flxContentContiner",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "55dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxContentContiner.setDefaultUnit(kony.flex.DP);
    var flxVehicleNumberContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "flxVehicleNumberContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "flxWhiteBgSkin",
        "top": "0dp",
        "width": "100%"
    }, {}, {});
    flxVehicleNumberContainer.setDefaultUnit(kony.flex.DP);
    var txtVehicleRegNumber = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_ALL,
        "centerY": "50%",
        "height": "20dp",
        "id": "txtVehicleRegNumber",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "left": "4%",
        "maxTextLength": 9,
        "placeholder": "Reg Number",
        "secureTextEntry": false,
        "skin": "txtNormalSkin",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "width": "103dp"
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [2, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DEFAULT,
        "placeholderSkin": "txtNormalSkin",
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var txtMileage = new kony.ui.TextBox2({
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "centerY": "50%",
        "height": "20dp",
        "id": "txtMileage",
        "isVisible": true,
        "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
        "left": "34%",
        "maxTextLength": 6,
        "placeholder": "Mileage",
        "secureTextEntry": false,
        "skin": "txtNormalSkin",
        "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
        "width": "75dp",
        "zIndex": 1
    }, {
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [3, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "autoFilter": false,
        "keyboardActionLabel": constants.TEXTBOX_KEYBOARD_LABEL_DONE,
        "placeholderSkin": "txtNormalSkin",
        "viewType": constants.TEXTBOX_VIEW_TYPE_DEFAULT
    });
    var lblUnderLine = new kony.ui.Label({
        "height": "2%",
        "id": "lblUnderLine",
        "isVisible": true,
        "left": "4%",
        "skin": "lblLineSkin",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "35dp",
        "width": "82dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblAllChecksOk = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblAllChecksOk",
        "isVisible": true,
        "right": "16%",
        "skin": "lblBlackBoldSkin",
        "text": "All checks ok?",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var flxAllCheckContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "28dp",
        "id": "flxAllCheckContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "onClick": AS_FlexContainer_97a7faded1f14832bdb9146672900824,
        "right": "4.63%",
        "skin": "slFbox",
        "width": "30dp",
        "zIndex": 1
    }, {}, {});
    flxAllCheckContainer.setDefaultUnit(kony.flex.DP);
    var imgAllCheck = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "50%",
        "height": "35dp",
        "id": "imgAllCheck",
        "isVisible": true,
        "skin": "slImage",
        "src": "icon_checkbox_off.png",
        "width": "35dp"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxAllCheckContainer.add(imgAllCheck);
    var lblNMileageUnderline = new kony.ui.Label({
        "height": "2%",
        "id": "lblNMileageUnderline",
        "isVisible": true,
        "left": "34%",
        "skin": "lblLineSkin",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "35dp",
        "width": "55dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxVehicleNumberContainer.add(txtVehicleRegNumber, txtMileage, lblUnderLine, lblAllChecksOk, flxAllCheckContainer, lblNMileageUnderline);
    var flxVehicleCheckDetailsContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "id": "flxVehicleCheckDetailsContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "52dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxVehicleCheckDetailsContainer.setDefaultUnit(kony.flex.DP);
    var tabPaneOuter = new kony.ui.TabPane({
        "activeFocusSkin": "tabSkin",
        "activeSkin": "tabSkin",
        "activeTabs": [0],
        "bottom": "0dp",
        "id": "tabPaneOuter",
        "inactiveSkin": "tabSkin",
        "isVisible": true,
        "layoutType": constants.CONTAINER_LAYOUT_BOX,
        "left": "0dp",
        "top": "0dp",
        "viewConfig": {
            "collapsibleViewConfig": {
                "collapsedImage": "icon_plus.png",
                "collapsedimage": "icon_plus.png",
                "expandedImage": "icon_minus.png",
                "expandedimage": "icon_minus.png",
                "imagePosition": constants.TABPANE_COLLAPSIBLE_IMAGE_POSITION_RIGHT,
                "imageposition": "right",
                "tabNameAlignment": constants.TABPANE_COLLAPSIBLE_TABNAME_ALIGNMENT_LEFT,
                "tabnamealignment": "left",
                "toggleTabs": true,
                "toggletabs": true
            },
            "collapsibleviewconfig": {
                "collapsedImage": "icon_plus.png",
                "collapsedimage": "icon_plus.png",
                "expandedImage": "icon_minus.png",
                "expandedimage": "icon_minus.png",
                "imagePosition": constants.TABPANE_COLLAPSIBLE_IMAGE_POSITION_RIGHT,
                "imageposition": "right",
                "tabNameAlignment": constants.TABPANE_COLLAPSIBLE_TABNAME_ALIGNMENT_LEFT,
                "tabnamealignment": "left",
                "toggleTabs": true,
                "toggletabs": true
            },
            "pageViewConfig": {
                "needPageIndicator": true
            },
            "tabViewConfig": {
                "headerPosition": constants.TAB_HEADER_POSITION_TOP,
                "image1": "tableftarrow.png",
                "image2": "tabrightarrow.png"
            },
        },
        "viewType": constants.TABPANE_VIEW_TYPE_COLLAPSIBLEVIEW,
        "width": "100%",
        "zIndex": 1
    }, {
        "layoutType": constants.CONTAINER_LAYOUT_BOX,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "tabHeaderHeight": 40
    });
    var tabOne = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "85%",
        "id": "tabOne",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "orientation": constants.BOX_LAYOUT_VERTICAL,
        "skin": "slTab",
        "tabName": kony.i18n.getLocalizedString("common.category.inCab"),
        "width": "100%"
    }, {
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    tabOne.setDefaultUnit(kony.flex.DP);
    var segVehicleChecksOne = new kony.ui.SegmentedUI2({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "data": [{
            "imgNo": "",
            "imgOk": "",
            "lblItem": "",
            "lblNo": "",
            "lblOk": ""
        }],
        "groupCells": false,
        "id": "segVehicleChecksOne",
        "isVisible": true,
        "left": "0dp",
        "needPageIndicator": true,
        "onRowClick": AS_Segment_a24c5ef0c36c4768896bdd55499f543d,
        "pageOffDotImage": "pageoffdot.png",
        "pageOnDotImage": "pageondot.png",
        "retainSelection": false,
        "rowTemplate": flxVCheckRowContainer,
        "scrollingEvents": {},
        "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
        "separatorColor": "64646400",
        "separatorRequired": true,
        "separatorThickness": 1,
        "showScrollbars": false,
        "top": "0dp",
        "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
        "widgetDataMap": {
            "flxItemContainer": "flxItemContainer",
            "flxNoContainer": "flxNoContainer",
            "flxOkContainer": "flxOkContainer",
            "flxVCheckRowContainer": "flxVCheckRowContainer",
            "imgNo": "imgNo",
            "imgOk": "imgOk",
            "lblItem": "lblItem",
            "lblNo": "lblNo",
            "lblOk": "lblOk"
        },
        "width": "100%"
    }, {
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "dockSectionHeaders": true
    });
    tabOne.add(segVehicleChecksOne);
    tabPaneOuter.addTab("tabOne", kony.i18n.getLocalizedString("common.category.inCab"), null, tabOne, null);
    var tabTwo = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "85%",
        "id": "tabTwo",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "orientation": constants.BOX_LAYOUT_VERTICAL,
        "skin": "slTab",
        "tabName": kony.i18n.getLocalizedString("common.category.underBonnet"),
        "width": "100%"
    }, {
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    tabTwo.setDefaultUnit(kony.flex.DP);
    var segVehicleChecksTwo = new kony.ui.SegmentedUI2({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "data": [{
            "imgNo": "",
            "imgOk": "",
            "lblItem": "",
            "lblNo": "",
            "lblOk": ""
        }],
        "groupCells": false,
        "id": "segVehicleChecksTwo",
        "isVisible": true,
        "left": "0dp",
        "needPageIndicator": true,
        "onRowClick": AS_Segment_a24c5ef0c36c4768896bdd55499f543d,
        "pageOffDotImage": "pageoffdot.png",
        "pageOnDotImage": "pageondot.png",
        "retainSelection": false,
        "rowTemplate": flxVCheckRowContainer,
        "scrollingEvents": {},
        "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
        "separatorColor": "64646400",
        "separatorRequired": true,
        "separatorThickness": 1,
        "showScrollbars": false,
        "top": "0dp",
        "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
        "widgetDataMap": {
            "flxItemContainer": "flxItemContainer",
            "flxNoContainer": "flxNoContainer",
            "flxOkContainer": "flxOkContainer",
            "flxVCheckRowContainer": "flxVCheckRowContainer",
            "imgNo": "imgNo",
            "imgOk": "imgOk",
            "lblItem": "lblItem",
            "lblNo": "lblNo",
            "lblOk": "lblOk"
        },
        "width": "100%"
    }, {
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "dockSectionHeaders": true
    });
    tabTwo.add(segVehicleChecksTwo);
    tabPaneOuter.addTab("tabTwo", kony.i18n.getLocalizedString("common.category.underBonnet"), null, tabTwo, null);
    var tabThree = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "85%",
        "id": "tabThree",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "orientation": constants.BOX_LAYOUT_VERTICAL,
        "skin": "slTab",
        "tabName": kony.i18n.getLocalizedString("common.category.exterior"),
        "width": "100%"
    }, {
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    tabThree.setDefaultUnit(kony.flex.DP);
    var segVehicleChecksThree = new kony.ui.SegmentedUI2({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "data": [{
            "imgNo": "",
            "imgOk": "",
            "lblItem": "",
            "lblNo": "",
            "lblOk": ""
        }],
        "groupCells": false,
        "id": "segVehicleChecksThree",
        "isVisible": true,
        "left": "0dp",
        "needPageIndicator": true,
        "onRowClick": AS_Segment_a24c5ef0c36c4768896bdd55499f543d,
        "pageOffDotImage": "pageoffdot.png",
        "pageOnDotImage": "pageondot.png",
        "retainSelection": false,
        "rowTemplate": flxVCheckRowContainer,
        "scrollingEvents": {},
        "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
        "separatorColor": "64646400",
        "separatorRequired": true,
        "separatorThickness": 1,
        "showScrollbars": false,
        "top": "0dp",
        "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
        "widgetDataMap": {
            "flxItemContainer": "flxItemContainer",
            "flxNoContainer": "flxNoContainer",
            "flxOkContainer": "flxOkContainer",
            "flxVCheckRowContainer": "flxVCheckRowContainer",
            "imgNo": "imgNo",
            "imgOk": "imgOk",
            "lblItem": "lblItem",
            "lblNo": "lblNo",
            "lblOk": "lblOk"
        },
        "width": "100%"
    }, {
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "dockSectionHeaders": true
    });
    tabThree.add(segVehicleChecksThree);
    tabPaneOuter.addTab("tabThree", kony.i18n.getLocalizedString("common.category.exterior"), null, tabThree, null);
    var tabFour = new kony.ui.FlexContainer({
        "clipBounds": true,
        "height": "85%",
        "id": "tabFour",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "orientation": constants.BOX_LAYOUT_VERTICAL,
        "skin": "slTab",
        "tabName": kony.i18n.getLocalizedString("common.label.photo"),
        "width": "100%"
    }, {
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    tabFour.setDefaultUnit(kony.flex.DP);
    var imgPhoto = new kony.ui.Image2({
        "bottom": "3%",
        "id": "imgPhoto",
        "isVisible": true,
        "left": "5%",
        "right": "5%",
        "skin": "slImage",
        "src": "icon_noimage.png",
        "top": "3%"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    tabFour.add(imgPhoto);
    tabPaneOuter.addTab("tabFour", kony.i18n.getLocalizedString("common.label.photo"), null, tabFour, null);
    flxVehicleCheckDetailsContainer.add(tabPaneOuter);
    flxContentContiner.add(flxVehicleNumberContainer, flxVehicleCheckDetailsContainer);
    var flxPopupOverlay = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "id": "flxPopupOverlay",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "onClick": AS_FlexContainer_c0d7710cb256423cac4dce3d6a0d0868,
        "skin": "flxOverlaySkin",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxPopupOverlay.setDefaultUnit(kony.flex.DP);
    var flxPopupContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "200dp",
        "id": "flxPopupContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "11%",
        "right": "11%",
        "skin": "flxPopupBgSkin",
        "top": "156dp"
    }, {}, {});
    flxPopupContainer.setDefaultUnit(kony.flex.DP);
    var flxPopupTitleContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "40dp",
        "id": "flxPopupTitleContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxPopupTitleContainer.setDefaultUnit(kony.flex.DP);
    var lblPopupTitle = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "id": "lblPopupTitle",
        "isVisible": true,
        "left": "4%",
        "skin": "lblBlackBoldBigSkin",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 2, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxPopupTitleContainer.add(lblPopupTitle);
    var flxPopupContentContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "100dp",
        "id": "flxPopupContentContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "40dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxPopupContentContainer.setDefaultUnit(kony.flex.DP);
    var lblMessage = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblMessage",
        "isVisible": true,
        "maxNumberOfLines": 5,
        "skin": "lblGrayNormalSkin",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "25dp",
        "width": "80%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxPopupContentContainer.add(lblMessage);
    var flxPopupButtonContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "60dp",
        "id": "flxPopupButtonContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "140dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxPopupButtonContainer.setDefaultUnit(kony.flex.DP);
    var btnOk = new kony.ui.Button({
        "centerX": "50%",
        "centerY": "50%",
        "focusSkin": "btnNormalSkin",
        "height": "36dp",
        "id": "btnOk",
        "isVisible": true,
        "onClick": AS_Button_d2d34bf60a214e22b7fb01ef9fce7496,
        "skin": "btnNormalSkin",
        "text": "Ok",
        "width": "100dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxPopupButtonContainer.add(btnOk);
    flxPopupContainer.add(flxPopupTitleContainer, flxPopupContentContainer, flxPopupButtonContainer);
    flxPopupOverlay.add(flxPopupContainer);
    flxMainContainer.add(flxHeaderContainer, flxContentContiner, flxPopupOverlay);
    var flxSettingsContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "centerX": "-50%",
        "clipBounds": true,
        "id": "flxSettingsContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": 0,
        "onClick": AS_FlexContainer_e865e39c1cde4d28b9529c0e68d7578c,
        "right": 0,
        "skin": "flxSettingsBgSkin",
        "top": "0dp",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    flxSettingsContainer.setDefaultUnit(kony.flex.DP);
    var flxBurgerLayer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "id": "flxBurgerLayer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "flxWhiteBgSkin",
        "top": "0dp",
        "width": "80%",
        "zIndex": 1
    }, {}, {});
    flxBurgerLayer.setDefaultUnit(kony.flex.DP);
    var flxTopContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "30%",
        "id": "flxTopContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "onTouchStart": AS_FlexContainer_37beab6ee81a4fbea5d8ac4236eeb4d7,
        "skin": "flxBlueBgSkin",
        "top": "0dp",
        "width": "100%"
    }, {}, {});
    flxTopContainer.setDefaultUnit(kony.flex.DP);
    var lblVersion = new kony.ui.Label({
        "id": "lblVersion",
        "isVisible": true,
        "right": "4%",
        "skin": "lbllWhiteTinySkin",
        "text": "V 1.0",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "4%",
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var imgLogo = new kony.ui.Image2({
        "height": "55dp",
        "id": "imgLogo",
        "isVisible": true,
        "left": "4%",
        "skin": "slImage",
        "src": "logo_rentokil.png",
        "top": "30%",
        "width": "122dp"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblUsername = new kony.ui.Label({
        "id": "lblUsername",
        "isVisible": true,
        "left": "5%",
        "skin": "lbllWhiteNormalSkin",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "72%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxTopContainer.add(lblVersion, imgLogo, lblUsername);
    var flxBottomContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "id": "flxBottomContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "29.98%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxBottomContainer.setDefaultUnit(kony.flex.DP);
    var flxContainerOne = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "46dp",
        "id": "flxContainerOne",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "onClick": AS_FlexContainer_8ee945d5713b4ea2aa763cc0a5e4ccd4,
        "skin": "slFbox",
        "top": "3%",
        "width": "100%"
    }, {}, {});
    flxContainerOne.setDefaultUnit(kony.flex.DP);
    var imgIcon1 = new kony.ui.Image2({
        "centerY": "50.00%",
        "height": "40dp",
        "id": "imgIcon1",
        "isVisible": true,
        "left": "4%",
        "skin": "slImage",
        "src": "about_icon.png",
        "width": "40dp"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblAbout = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblAbout",
        "isVisible": true,
        "left": "60dp",
        "skin": "lblBlackBoldSkin",
        "text": "About",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxContainerOne.add(imgIcon1, lblAbout);
    var flxContainerTwo = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "46dp",
        "id": "flxContainerTwo",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "onClick": AS_FlexContainer_e98ab66c02224030908c7ab809fd67be,
        "skin": "slFbox",
        "top": "15%",
        "width": "100%"
    }, {}, {});
    flxContainerTwo.setDefaultUnit(kony.flex.DP);
    var imgIcon2 = new kony.ui.Image2({
        "centerY": "50.00%",
        "height": "40dp",
        "id": "imgIcon2",
        "isVisible": true,
        "left": "4%",
        "skin": "slImage",
        "src": "sync_icon.png",
        "width": "40dp"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblSync = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblSync",
        "isVisible": true,
        "left": "60dp",
        "skin": "lblBlackBoldSkin",
        "text": "Sync",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxContainerTwo.add(imgIcon2, lblSync);
    var flxContainerThree = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "46dp",
        "id": "flxContainerThree",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "onClick": AS_FlexContainer_eee9853718e142ed919e016f9bec0453,
        "skin": "slFbox",
        "top": "27%",
        "width": "100%"
    }, {}, {});
    flxContainerThree.setDefaultUnit(kony.flex.DP);
    var imgIcon3 = new kony.ui.Image2({
        "centerY": "50.00%",
        "height": "40dp",
        "id": "imgIcon3",
        "isVisible": true,
        "left": "4%",
        "skin": "slImage",
        "src": "logout_icon.png",
        "width": "40dp"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblLogout = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblLogout",
        "isVisible": true,
        "left": "60dp",
        "skin": "lblBlackBoldSkin",
        "text": "Logout",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxContainerThree.add(imgIcon3, lblLogout);
    var flxLastSyncContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "height": "60dp",
        "id": "flxLastSyncContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxLastSyncContainer.setDefaultUnit(kony.flex.DP);
    var lblLastSync = new kony.ui.Label({
        "centerY": "30%",
        "id": "lblLastSync",
        "isVisible": true,
        "left": "5%",
        "skin": "lblGrayNormalSkin",
        "text": "Last Sync:",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblLastSyncDate = new kony.ui.Label({
        "centerY": "65%",
        "id": "lblLastSyncDate",
        "isVisible": true,
        "left": "5%",
        "skin": "lblGrayTinySkin",
        "text": "25/05/2016 10:20PM",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxLastSyncContainer.add(lblLastSync, lblLastSyncDate);
    flxBottomContainer.add(flxContainerOne, flxContainerTwo, flxContainerThree, flxLastSyncContainer);
    flxBurgerLayer.add(flxTopContainer, flxBottomContainer);
    flxSettingsContainer.add(flxBurgerLayer);
    var flxBottomMsgContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "id": "flxBottomMsgContainer",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "onClick": AS_FlexContainer_c0d7710cb256423cac4dce3d6a0d0868,
        "onTouchStart": AS_FlexContainer_541dd236ddc3402b80e672fd46eba168,
        "skin": "flxOverlaySkin",
        "top": "0dp",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    flxBottomMsgContainer.setDefaultUnit(kony.flex.DP);
    var flxWarningMsgContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "height": "18%",
        "id": "flxWarningMsgContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "flxBlueBgSkin",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxWarningMsgContainer.setDefaultUnit(kony.flex.DP);
    var lblWarningMsg = new kony.ui.Label({
        "centerY": "50%",
        "height": "100dp",
        "id": "lblWarningMsg",
        "isVisible": true,
        "left": "1%",
        "skin": "lblWhiteBoldNormalSkin",
        "text": "Label",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "96.04%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [2, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnYes = new kony.ui.Button({
        "centerY": "77%",
        "focusSkin": "slButtonGlossRed",
        "height": "40dp",
        "id": "btnYes",
        "isVisible": true,
        "onClick": AS_Button_08ef175f59574e8ba91064bbfb36fd06,
        "right": "27%",
        "skin": "btnTransparentSkinYellow",
        "text": "Yes",
        "top": "13dp",
        "width": "40dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnNo = new kony.ui.Button({
        "centerY": "77%",
        "focusSkin": "slButtonGlossRed",
        "height": "40dp",
        "id": "btnNo",
        "isVisible": true,
        "onClick": AS_Button_f428781056ff45f4a67f2d5344602855,
        "right": "14%",
        "skin": "btnTransparentSkin",
        "text": "No",
        "top": "13dp",
        "width": "40dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxWarningMsgContainer.add(lblWarningMsg, btnYes, btnNo);
    flxBottomMsgContainer.add(flxWarningMsgContainer);
    Home.add(flxMainContainer, flxSettingsContainer, flxBottomMsgContainer);
};

function HomeGlobals() {
    Home = new kony.ui.Form2({
        "addWidgets": addWidgetsHome,
        "allowHorizontalBounce": false,
        "enabledForIdleTimeout": false,
        "id": "Home",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "postShow": AS_Form_7e9bb2d8123449118638f96221faacf5,
        "preShow": AS_Form_c321a2e940374a828ee2c71acc2d2957,
        "skin": "BlueBgFormSkin",
        "verticalScrollIndicator": false
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": false,
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "onDeviceBack": AS_Form_f2d890fdf8574df4bec315488e4586cd,
        "retainScrollPosition": false,
        "titleBar": false,
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
    Home.info = {
        "kuid": "f2a1d30b0ab54c9fb9b5b2a5e66cc500"
    };
};