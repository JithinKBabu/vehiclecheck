function addWidgetsPopupConfirmation() {
    var hbxTitleContainer = new kony.ui.Box({
        "id": "hbxTitleContainer",
        "isVisible": true,
        "orientation": constants.BOX_LAYOUT_HORIZONTAL,
        "position": constants.BOX_POSITION_AS_NORMAL,
        "skin": "slHbox"
    }, {
        "containerWeight": 100,
        "layoutType": constants.CONTAINER_LAYOUT_BOX,
        "margin": [0, 0, 0, 0],
        "marginInPixel": false,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false,
        "percent": true,
        "vExpand": false,
        "widgetAlignment": constants.WIDGET_ALIGN_TOP_LEFT
    }, {});
    var lblTitle = new kony.ui.Label({
        "id": "lblTitle",
        "isVisible": true,
        "text": "Label",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        }
    }, {
        "containerWeight": 100,
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "hExpand": true,
        "margin": [1, 2, 1, 2],
        "marginInPixel": false,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false,
        "vExpand": false,
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER
    }, {
        "textCopyable": false
    });
    hbxTitleContainer.add(lblTitle);
    var hbxContentContainer = new kony.ui.Box({
        "id": "hbxContentContainer",
        "isVisible": true,
        "orientation": constants.BOX_LAYOUT_HORIZONTAL,
        "position": constants.BOX_POSITION_AS_NORMAL,
        "skin": "slHbox"
    }, {
        "containerWeight": 100,
        "layoutType": constants.CONTAINER_LAYOUT_BOX,
        "margin": [0, 0, 0, 0],
        "marginInPixel": false,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false,
        "percent": true,
        "vExpand": false,
        "widgetAlignment": constants.WIDGET_ALIGN_TOP_LEFT
    }, {});
    var lblMessage = new kony.ui.Label({
        "id": "lblMessage",
        "isVisible": true,
        "text": "Label",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        }
    }, {
        "containerWeight": 100,
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "hExpand": true,
        "margin": [2, 2, 2, 2],
        "marginInPixel": false,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false,
        "vExpand": false,
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER
    }, {
        "textCopyable": false
    });
    hbxContentContainer.add(lblMessage);
    var hbxButtonContainer = new kony.ui.Box({
        "id": "hbxButtonContainer",
        "isVisible": true,
        "orientation": constants.BOX_LAYOUT_HORIZONTAL,
        "position": constants.BOX_POSITION_AS_NORMAL,
        "skin": "slHbox"
    }, {
        "containerWeight": 100,
        "layoutType": constants.CONTAINER_LAYOUT_BOX,
        "margin": [0, 0, 0, 0],
        "marginInPixel": false,
        "padding": [0, 0, 0, 3],
        "paddingInPixel": false,
        "percent": true,
        "vExpand": false,
        "widgetAlignment": constants.WIDGET_ALIGN_TOP_LEFT
    }, {});
    var btnCancel = new kony.ui.Button({
        "focusSkin": "btnNormalSkin",
        "id": "btnCancel",
        "isVisible": true,
        "onClick": AS_Button_4971bc3fbdb44fcc8d2d35b8d5198fcc,
        "skin": "btnNormalSkin",
        "text": "Button"
    }, {
        "containerWeight": 50,
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "hExpand": true,
        "margin": [7, 1, 7, 1],
        "marginInPixel": false,
        "padding": [4, 2, 4, 2],
        "paddingInPixel": false,
        "vExpand": false,
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER
    }, {});
    var btnOk = new kony.ui.Button({
        "focusSkin": "btnNormalSkin",
        "id": "btnOk",
        "isVisible": true,
        "onClick": AS_Button_118f150970c14159a9912c46974bf723,
        "skin": "btnNormalSkin",
        "text": "Button"
    }, {
        "containerWeight": 50,
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "hExpand": true,
        "margin": [7, 1, 7, 1],
        "marginInPixel": false,
        "padding": [4, 2, 4, 2],
        "paddingInPixel": false,
        "vExpand": false,
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER
    }, {});
    hbxButtonContainer.add(btnCancel, btnOk);
    PopupConfirmation.add(hbxTitleContainer, hbxContentContainer, hbxButtonContainer);
};

function PopupConfirmationGlobals() {
    PopupConfirmation = new kony.ui.Popup({
        "addWidgets": addWidgetsPopupConfirmation,
        "id": "PopupConfirmation",
        "isModal": true,
        "skin": "popupBgSkin",
        "transparencyBehindThePopup": 60
    }, {
        "containerWeight": 80,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "windowSoftInputMode": constants.POPUP_ADJUST_PAN
    });
    PopupConfirmation.info = {
        "kuid": "fb8277e2123040e28beff719ead06460"
    };
};