//actions.js file 
function ActionContentPostShow(eventobject) {
    return AS_Form_25267fa367ab4843bad85aae3bb7ac5b(eventobject);
}

function AS_Form_25267fa367ab4843bad85aae3bb7ac5b(eventobject) {
    return onPostShowContentScreen.call(this);
}

function ActionContentPreSow(eventobject) {
    return AS_Form_c51fbd6104a14ea59b9e875276278efd(eventobject);
}

function AS_Form_c51fbd6104a14ea59b9e875276278efd(eventobject) {
    return onPreShowContentScreen.call(this);
}

function ActionDeviceBackFromContent(eventobject) {
    return AS_Form_015985df7e094525be30f489089d5df7(eventobject);
}

function AS_Form_015985df7e094525be30f489089d5df7(eventobject) {
    return onDeviceBackButtonCall.call(this);
}

function ActionDeviceBackFromHome(eventobject) {
    return AS_Form_f2d890fdf8574df4bec315488e4586cd(eventobject);
}

function AS_Form_f2d890fdf8574df4bec315488e4586cd(eventobject) {
    return onDeviceBackButtonCall.call(this);
}

function ActionDeviceBackFromLogin(eventobject) {
    return AS_Form_2471fa04c0ee4c05a6f776beba46bf03(eventobject);
}

function AS_Form_2471fa04c0ee4c05a6f776beba46bf03(eventobject) {
    return onDeviceBackButtonCall.call(this);
}

function ActionEmailPopupCancel(eventobject) {
    return AS_Button_80b6d6450b124006b9f87e5b41517e62(eventobject);
}

function AS_Button_80b6d6450b124006b9f87e5b41517e62(eventobject) {
    return onClickEmailsPopupCancelButton.call(this);
}

function ActionEmailPopupOk(eventobject) {
    return AS_Button_1098877f28b84d1e9d5a563dd6573082(eventobject);
}

function AS_Button_1098877f28b84d1e9d5a563dd6573082(eventobject) {
    return onClickEmailsPopupOkButton.call(this);
}

function ActionEmailSegRowClick(eventobject, sectionNumber, rowNumber) {
    return AS_Segment_8e476be8a3d74348981be40a6ad1b00c(eventobject, sectionNumber, rowNumber);
}

function AS_Segment_8e476be8a3d74348981be40a6ad1b00c(eventobject, sectionNumber, rowNumber) {
    return onClickSegEmailRow.call(this);
}

function ActionHomePostShow(eventobject) {
    return AS_Form_7e9bb2d8123449118638f96221faacf5(eventobject);
}

function AS_Form_7e9bb2d8123449118638f96221faacf5(eventobject) {
    return onPostShowHomeScreen.call(this);
}

function ActionHomePreShow(eventobject) {
    return AS_Form_c321a2e940374a828ee2c71acc2d2957(eventobject);
}

function AS_Form_c321a2e940374a828ee2c71acc2d2957(eventobject) {
    return onPreShowHomeScreen.call(this);
}

function ActionHomeSegRowNoRadioBtn(eventobject, context) {
    return AS_FlexContainer_9d8e0d7734dd4bbebdc196e43e4e9e78(eventobject, context);
}

function AS_FlexContainer_9d8e0d7734dd4bbebdc196e43e4e9e78(eventobject, context) {
    return onClickSegRowNoRadioBtn.call(this, eventobject);
}

function ActionHomeSegRowOkRadioBtn(eventobject, context) {
    return AS_FlexContainer_49d56d5e141840f3bbd1827276055a26(eventobject, context);
}

function AS_FlexContainer_49d56d5e141840f3bbd1827276055a26(eventobject, context) {
    return onClickSegRowOkRadioBtn.call(this, eventobject);
}

function ActionLoginPostShow(eventobject) {
    return AS_Form_e7d9850118d0451aacd477132d6229be(eventobject);
}

function AS_Form_e7d9850118d0451aacd477132d6229be(eventobject) {
    return onPostShowLoginScreen.call(this);
}

function ActionLoginPreShow(eventobject) {
    return AS_Form_8b2dd570ab984a1bb2b75b91a5eaddfa(eventobject);
}

function AS_Form_8b2dd570ab984a1bb2b75b91a5eaddfa(eventobject) {
    return onPreShowLoginScreen.call(this);
}

function ActionOkBranchSelection(eventobject) {
    return AS_Button_f3fc4fadbec6480eb4eed178b2578e25(eventobject);
}

function AS_Button_f3fc4fadbec6480eb4eed178b2578e25(eventobject) {
    return onClickBranchPopupOkButton.call(this);
}

function ActionOnCameraCapture(eventobject) {
    return AS_Button_c4c212e2791a4a2db1143c15cf6cb980(eventobject);
}

function AS_Button_c4c212e2791a4a2db1143c15cf6cb980(eventobject) {
    return onCameraPhotoCapture.call(this);
}

function ActionOnClickAbout(eventobject) {
    return AS_FlexContainer_8ee945d5713b4ea2aa763cc0a5e4ccd4(eventobject);
}

function AS_FlexContainer_8ee945d5713b4ea2aa763cc0a5e4ccd4(eventobject) {
    return onClickAbout.call(this);
}

function ActionOnClickAllCheck(eventobject) {
    return AS_FlexContainer_97a7faded1f14832bdb9146672900824(eventobject);
}

function AS_FlexContainer_97a7faded1f14832bdb9146672900824(eventobject) {
    return onClickAllCheck.call(this);
}

function ActionOnClickBurgerMenu(eventobject) {
    return AS_Button_7cbd927421e2489a96d6f2dfbffb1397(eventobject);
}

function AS_Button_7cbd927421e2489a96d6f2dfbffb1397(eventobject) {
    return onClickOpenMenuButton.call(this);
}

function ActionOnClickEmailPopupCancel(eventobject) {
    return AS_Button_7f5d1ba38e514512a786ee6373e33ea7(eventobject);
}

function AS_Button_7f5d1ba38e514512a786ee6373e33ea7(eventobject) {
    return onClickEmailsPopupCancelButton.call(this);
}

function ActionOnClickLogout(eventobject) {
    return AS_FlexContainer_eee9853718e142ed919e016f9bec0453(eventobject);
}

function AS_FlexContainer_eee9853718e142ed919e016f9bec0453(eventobject) {
    return onClickLogout.call(this);
}

function ActionOnClickMsgPopup(eventobject) {
    return AS_Button_d2d34bf60a214e22b7fb01ef9fce7496(eventobject);
}

function AS_Button_d2d34bf60a214e22b7fb01ef9fce7496(eventobject) {
    return onClickMsgOverlayPopupOk.call(this);
}

function ActionOnClickSave(eventobject) {
    return AS_Button_7111904ae9264f2880c0137f7ea4e0c7(eventobject);
}

function AS_Button_7111904ae9264f2880c0137f7ea4e0c7(eventobject) {
    return onClickSaveBtn.call(this);
}

function ActionOnClickSettingsContainer(eventobject) {
    return AS_FlexContainer_e865e39c1cde4d28b9529c0e68d7578c(eventobject);
}

function AS_FlexContainer_e865e39c1cde4d28b9529c0e68d7578c(eventobject) {
    return closeSettingsMenu.call(this);
}

function ActionOnClickSync(eventobject) {
    return AS_FlexContainer_e98ab66c02224030908c7ab809fd67be(eventobject);
}

function AS_FlexContainer_e98ab66c02224030908c7ab809fd67be(eventobject) {
    return onClickSync.call(this);
}

function ActionOnHomeSegmentRowClick(eventobject, sectionNumber, rowNumber) {
    return AS_Segment_a24c5ef0c36c4768896bdd55499f543d(eventobject, sectionNumber, rowNumber);
}

function AS_Segment_a24c5ef0c36c4768896bdd55499f543d(eventobject, sectionNumber, rowNumber) {
    return onClickHomeSegRow.call(this, eventobject);
}

function ActionOverlayVoidCall(eventobject) {
    return AS_FlexContainer_c0d7710cb256423cac4dce3d6a0d0868(eventobject);
}

function AS_FlexContainer_c0d7710cb256423cac4dce3d6a0d0868(eventobject) {
    return onClickOverlayLayer.call(this);
}

function ActionPopupConfirmCancel(eventobject) {
    return AS_Button_4971bc3fbdb44fcc8d2d35b8d5198fcc(eventobject);
}

function AS_Button_4971bc3fbdb44fcc8d2d35b8d5198fcc(eventobject) {
    return onClickEmailsPopupCancelButton.call(this);
}

function ActionPopupConfirmOk(eventobject) {
    return AS_Button_118f150970c14159a9912c46974bf723(eventobject);
}

function AS_Button_118f150970c14159a9912c46974bf723(eventobject) {
    return onClickEmailsPopupOkButton.call(this);
}

function ActionPopupMessageOk(eventobject) {
    return AS_Button_6253f731806141048fc89f31f10ca6ac(eventobject);
}

function AS_Button_6253f731806141048fc89f31f10ca6ac(eventobject) {
    return onClickMessagePopupOkButton.call(this);
}

function ActionPostAppInit(eventobject) {
    return AS_AppEvents_c7f27d4c66c2405cb49d5be262d68762(eventobject);
}

function AS_AppEvents_c7f27d4c66c2405cb49d5be262d68762(eventobject) {
    return initializeGlobals.call(this);
}

function ActionPreAppInit(eventobject) {
    return AS_AppEvents_94d038b0047f47d4ad7fa4cbf55d9cf9(eventobject);
}

function AS_AppEvents_94d038b0047f47d4ad7fa4cbf55d9cf9(eventobject) {
    /* adapter = NfcAdapter.getDefaultAdapter(context1);
     	if (adapter === null){
     		alert("NFC Not Supported in this Device");
     	}
     	else{ 
     	if (!adapter.isEnabled()){
    		 alert("Please enable NFC");
     	var intent = new Intent(Settings.ACTION_NFC_SETTINGS);
     context1.startActivity(intent);
          
     }
        }*/
}

function ActionTohideWarnigmsgInLogin(eventobject, x, y) {
    return AS_FlexContainer_5f519dce9efe4ebc9d48b64033c0a8ea(eventobject, x, y);
}

function AS_FlexContainer_5f519dce9efe4ebc9d48b64033c0a8ea(eventobject, x, y) {
    return onClickBranchOverlayLayer.call(this);
}

function ActionToSelectBranch(eventobject) {
    return AS_ListBox_8179370764c54b9fbe98c6a78ce23217(eventobject);
}

function AS_ListBox_8179370764c54b9fbe98c6a78ce23217(eventobject) {
    return updateBranchObj.call(this, eventobject);
}

function AS_AppEvents_879f0f0b86be42c9972d45514b6496bf(eventobject) {}

function AS_Button_08ef175f59574e8ba91064bbfb36fd06(eventobject) {
    function SHOW_ALERT__5948e992d6094bd5a5d187462fca45aa_True() {
        onClickLogout.call(this);
    }

    function SHOW_ALERT__5948e992d6094bd5a5d187462fca45aa_False() {
        dismissMessagePopup.call(this);
    }

    function SHOW_ALERT__5948e992d6094bd5a5d187462fca45aa_Callback(response) {
        if (response == true) {
            SHOW_ALERT__5948e992d6094bd5a5d187462fca45aa_True()
        } else {
            SHOW_ALERT__5948e992d6094bd5a5d187462fca45aa_False()
        };
    }
    kony.ui.Alert({
        "alertType": constants.ALERT_TYPE_CONFIRMATION,
        "alertTitle": "",
        "yesLabel": "Yes",
        "noLabel": "No",
        "alertIcon": "logout_icon.png",
        "message": "Are you sure want to log out the application?",
        "alertHandler": SHOW_ALERT__5948e992d6094bd5a5d187462fca45aa_Callback
    }, {
        "iconPosition": constants.ALERT_ICON_POSITION_LEFT
    })
}

function AS_Button_8d4ec929f4974af29aaa73760d475c9b(eventobject) {
    function SHOW_ALERT_ide_onClick_b41e43c3aaf147ec912760580561ff10_True() {
        onClickLogout.call(this);
    }

    function SHOW_ALERT_ide_onClick_b41e43c3aaf147ec912760580561ff10_False() {
        dismissMessagePopup.call(this);
    }

    function SHOW_ALERT_ide_onClick_b41e43c3aaf147ec912760580561ff10_Callback(response) {
        if (response == true) {
            SHOW_ALERT_ide_onClick_b41e43c3aaf147ec912760580561ff10_True()
        } else {
            SHOW_ALERT_ide_onClick_b41e43c3aaf147ec912760580561ff10_False()
        };
    }
    kony.ui.Alert({
        "alertType": constants.ALERT_TYPE_CONFIRMATION,
        "alertTitle": "",
        "yesLabel": "Yes",
        "noLabel": "No",
        "alertIcon": "logout_icon.png",
        "message": "Are you sure want to log out the application?",
        "alertHandler": SHOW_ALERT_ide_onClick_b41e43c3aaf147ec912760580561ff10_Callback
    }, {
        "iconPosition": constants.ALERT_ICON_POSITION_LEFT
    })
}

function AS_Button_f428781056ff45f4a67f2d5344602855(eventobject) {
    return onClickOverlayLayer.call(this);
}

function AS_FlexContainer_37beab6ee81a4fbea5d8ac4236eeb4d7(eventobject, x, y) {
    return closeSettingsMenu.call(this);
}

function AS_FlexContainer_541dd236ddc3402b80e672fd46eba168(eventobject, x, y) {
    return onClickOverlayLayer.call(this);
}

function AS_FlexContainer_61e8e1e0f8aa4f729c904270f2213403(eventobject) {
    return onClickLogout.call(this);
}

function AS_FlexContainer_692d1b528c15446aaaafd8d135615f5e(eventobject) {
    return onClickSync.call(this);
}

function AS_FlexContainer_8e7ecb0203e94467a817c072fb21cba5(eventobject) {
    return onClickAbout.call(this);
}

function AS_FlexContainer_a17587b8ba4d4604ad2df87b514d432d(eventobject) {
    return onClickOverlayLayer.call(this);
}

function AS_FlexContainer_e749a4cb545f4364a476635699199636(eventobject, x, y) {
    return onClickOverlayLayer.call(this);
}