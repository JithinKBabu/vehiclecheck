function initializetplSegVCheckRow() {
    flxVCheckRowContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "flxVCheckRowContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "flxWhiteBgSkin"
    }, {}, {});
    flxVCheckRowContainer.setDefaultUnit(kony.flex.DP);
    var flxItemContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "id": "flxItemContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "62%",
        "zIndex": 1
    }, {}, {});
    flxItemContainer.setDefaultUnit(kony.flex.DP);
    var lblItem = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblItem",
        "isVisible": true,
        "left": "5%",
        "maxNumberOfLines": 2,
        "skin": "lblGrayNormalSkin",
        "text": "Label",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "top": "3dp",
        "width": "92%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxItemContainer.add(lblItem);
    var flxNoContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "id": "flxNoContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "onClick": AS_FlexContainer_9d8e0d7734dd4bbebdc196e43e4e9e78,
        "right": "22%",
        "skin": "slFbox",
        "top": "0dp",
        "width": "16%",
        "zIndex": 1
    }, {}, {});
    flxNoContainer.setDefaultUnit(kony.flex.DP);
    var imgNo = new kony.ui.Image2({
        "centerY": "50%",
        "height": "30dp",
        "id": "imgNo",
        "isVisible": true,
        "right": "25dp",
        "skin": "slImage",
        "src": "imagedrag.png",
        "width": "30dp"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblNo = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblNo",
        "isVisible": true,
        "right": "0dp",
        "skin": "lblGrayNormalSkin",
        "text": "No",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxNoContainer.add(imgNo, lblNo);
    var flxOkContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "id": "flxOkContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "onClick": AS_FlexContainer_49d56d5e141840f3bbd1827276055a26,
        "right": "4%",
        "skin": "slFbox",
        "top": "0dp",
        "width": "16%",
        "zIndex": 1
    }, {}, {});
    flxOkContainer.setDefaultUnit(kony.flex.DP);
    var imgOk = new kony.ui.Image2({
        "centerY": "50.00%",
        "height": "30dp",
        "id": "imgOk",
        "isVisible": true,
        "right": "25dp",
        "skin": "slImage",
        "src": "imagedrag.png",
        "width": "30dp"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblOk = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblOk",
        "isVisible": true,
        "right": "0dp",
        "skin": "lblGrayNormalSkin",
        "text": "Ok",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxOkContainer.add(imgOk, lblOk);
    flxVCheckRowContainer.add(flxItemContainer, flxNoContainer, flxOkContainer);
}