function initializetplTempSegRowEmp() {
    flxTempSegRowEmp = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "40dp",
        "id": "flxTempSegRowEmp",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "flxWhiteBgSkin"
    }, {}, {});
    flxTempSegRowEmp.setDefaultUnit(kony.flex.DP);
    var lblItem = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblItem",
        "isVisible": true,
        "left": "4%",
        "maxNumberOfLines": 2,
        "skin": "lblGrayTinySkin",
        "text": "Label",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "width": "50%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblColumn1 = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblColumn1",
        "isVisible": true,
        "left": "55.00%",
        "maxNumberOfLines": 2,
        "skin": "lblGrayTinySkin",
        "text": "Label",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "width": "29%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblColumn2 = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblColumn2",
        "isVisible": true,
        "left": "84%",
        "maxNumberOfLines": 2,
        "skin": "lblGrayTinySkin",
        "text": "Label",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "width": "8%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblColumn3 = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblColumn3",
        "isVisible": true,
        "left": "92%",
        "maxNumberOfLines": 2,
        "skin": "lblGrayTinySkin",
        "text": "Label",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "width": "8%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxTempSegRowEmp.add(lblItem, lblColumn1, lblColumn2, lblColumn3);
}