/**********************************************************************
 *	Author  : RI - CTS363601 
 *	Purpose : UI Screen - Content - Methods
 ***********************************************************************/
/** Show content screen **/
function showContentScreen() {
    Content.show();
}
/** Content screen - PreShow **/
function onPreShowContentScreen() {}
/** Content screen - PostShow **/
function onPostShowContentScreen() {
    showLoading('common.message.loading');
    Content.browserContent.htmlString = static_content;
    refreshSettingsUI();
    dismissLoading();
}