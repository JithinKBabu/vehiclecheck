/**********************************************************************
 *	Author  : RI - CTS363601 
 *	Purpose : Module to refer all the offline database methods
 ***********************************************************************/
/** Method - Create attendance record in database table **/
function saveVehicleChecks() {
    gblSaveActionFlags = {};
    gblSaveActionFlags.inCab = false;
    gblSaveActionFlags.uBonnet = false;
    gblSaveActionFlags.exterior = false;
    gblSaveActionFlags.vDetails = false;
    var vehicleRegNo = Home.txtVehicleRegNumber.text.trim();
    var dateValue = new Date();
    var mileage = "";
    if (!isEmpty(Home.txtMileage.text)) {
        mileage = Home.txtMileage.text.trim();
    } else {
        mileage = "NA";
    }
    regno = vehicleRegNo;
    email = gblEmployeeObject.email;
    branchnumber = gblEmployeeObject.branchNumber;
    location = gblGeoPosition;
    dateField = dateValue.toTimeString();
    latitude = gblLocationData.latitude;
    longitude = gblLocationData.longitude;
    accuracy = gblLocationData.accuracy;
    printMessage("#***# latitude#***#" + latitude);
    printMessage("#***# longitude #***#" + longitude);
    printMessage("#***# accuracy#***#" + accuracy);
    /*** var imgDataStr					= (!isEmpty(gblImageData))?kony.convertToBase64(gblImageData):""; ***/
    var imgDataStr = "";
    //	var vcdetaildata				= {"regno":regno, "email":email, "location":location, "dateField":dateField, "image":imgDataStr};
    var vcdetaildata = {
        "regno": regno,
        "email": email,
        "location": location,
        "latitude": latitude,
        "longitude": longitude,
        "accuracy": accuracy,
        "dateField": getFormatedDate(),
        "timestampField": getTimeInUTC(),
        "branchNumber": branchnumber,
        "mileage": mileage,
        "image": imgDataStr
    };
    VCScope.vcdetails.create(vcdetaildata, success_callbackVDetails, error_commonCallback, true);
    var categoriesCount = gblDataObjectsArray.length;
    var vCheckDataSet = {};
    var elementsCount = 0;
    var anItemObj = null;
    var anElementObject = null;
    var result = "";
    if (categoriesCount > 0) {
        for (var i = 0; i < categoriesCount; i++) {
            sqlStatement = "";
            anItemObj = gblDataObjectsArray[i];
            vCheckDataSet = {};
            vCheckDataSet.regno = vehicleRegNo;
            vCheckDataSet.email = gblEmployeeObject.email;
            elementsCount = anItemObj.checkItems.length;
            for (var j = 0; j < elementsCount; j++) {
                anElementObject = anItemObj.checkItems[j];
                if (anElementObject.isOkay === false) {
                    result = "No";
                } else if (anElementObject.isOkay === true) {
                    result = "Ok";
                } else {
                    result = "No Input";
                }
                vCheckDataSet[anElementObject.dbfield] = result;
            }
            if ("IN_CAB" == anItemObj.code) {
                VCScope.vcincab.create(vCheckDataSet, success_callbackInCab, error_commonCallback, true);
            } else if ("UNDER_BONNET" == anItemObj.code) {
                VCScope.vcunbonnet.create(vCheckDataSet, success_callbackUBonnet, error_commonCallback, true);
            } else if ("EXTERIOR" == anItemObj.code) {
                VCScope.vcexterior.create(vCheckDataSet, success_callbackExterior, error_commonCallback, true);
            }
        }
    }
}
/********************************************* Custom DB operations *************************************************/
/** To open DB handler **/
function openLocalDatabase() {
    var dbname = kony.sync.getDBName();
    var dbObjectId = kony.sync.getConnectionOnly(dbname, dbname);
    return dbObjectId;
}
/** Get details from employee details table - Transaction **/
function getEmployeeDetailsTransaction() {
    printMessage("#***# Inside Transaction - Get Employee Details #***#");
    var dbname = kony.sync.getDBName();
    var sqlStatement = "SELECT email,branchNumber,businessCode,countryCode FROM employeedetails LIMIT 0,1";
    kony.sync.single_select_execute(dbname, sqlStatement, null, success_getEmployeeDetails, error_getEmployeeDetails);
}
/** Insert details in employee details table - Transaction **/
function insertEmployeeDetailsTransaction() {
    var dbObjectId = openLocalDatabase();
    kony.db.transaction(dbObjectId, insertEmployeeDetails, error_commonCallback, success_commonCallback);
}
/** Delete all employee details from temp employee details table - Transaction **/
function deleteEmployeeDetailsTransaction() {
    var dbObjectId = openLocalDatabase();
    kony.db.transaction(dbObjectId, deleteEmployeeDetails, error_deleteEmployeeDetails, success_deleteEmployeeDetails);
}
/** Insert employee details in employeedetails Table **/
function insertEmployeeDetails(dbId) {
    var sqlStatement = "INSERT INTO employeedetails ( employeeCode, email, firstName, lastName, countryCode, businessCode, branchNumber) VALUES (?, ?, ?, ?, ?, ?, ?)";
    var inputData = [];
    inputData.push(gblEmployeeObject.employeeCode);
    inputData.push(gblEmployeeObject.email);
    inputData.push(gblEmployeeObject.firstName);
    inputData.push(gblEmployeeObject.lastName);
    inputData.push(gblEmployeeObject.countryCode);
    inputData.push(gblEmployeeObject.businessCode);
    inputData.push(gblEmployeeObject.branchNumber);
    kony.db.executeSql(dbId, sqlStatement, inputData, success_transCommonCallback, error_transCommonCallback);
}
/** Delete employee details from employeedetails Table **/
function deleteEmployeeDetails(dbId) {
    var sqlStatement = "DELETE FROM employeedetails";
    kony.db.executeSql(dbId, sqlStatement, null, success_transCommonCallback, error_transCommonCallback);
}
/** Common success callback - For sql statement execution **/
function success_commonCallback(result) {
    printMessage("The SQL statement has been executed successfully");
}
/** Common error callback - For sql statement execution **/
function error_commonCallback(error) {
    printMessage("The SQL statement execution failed. Error code: " + JSON.stringify(error));
}
/** Success callback - Table insertion - InCab **/
function success_callbackInCab(result) {
    gblSaveActionFlags.inCab = true;
    startDataSync();
}
/** Success callback - Table insertion - VehicleDetails **/
function success_callbackVDetails(result) {
    gblSaveActionFlags.vDetails = true;
    startDataSync();
}
/** Success callback - Table insertion - UnderBonnet **/
function success_callbackUBonnet(result) {
    gblSaveActionFlags.uBonnet = true;
    startDataSync();
}
/** Success callback - Table insertion - Exterior **/
function success_callbackExterior(result) {
    gblSaveActionFlags.exterior = true;
    startDataSync();
}
/** To start sync by checking all db transactions are complete **/
function startDataSync() {
    printMessage("#*** Inside  ***#");
    if (!isEmpty(gblSaveActionFlags) && gblSaveActionFlags.exterior === true && gblSaveActionFlags.uBonnet === true && gblSaveActionFlags.vDetails === true && gblSaveActionFlags.inCab === true) {
        initDataSync();
    }
}
/** Transaction common success callback - For sql statement execution **/
function success_transCommonCallback(transactionId, result) {}
/** Transaction common error callback - For sql statement execution **/
function error_transCommonCallback(error) {}