/**********************************************************************
 *	Author  : RI - CTS363601 
 *	Purpose : Module to refer all global variables and methods
 ***********************************************************************/
/** Initializing global variables **/
function initializeGlobals() {
    printMessage("#***# Inside Login - initializeGlobals #***#");
    getCurrentDevLocale();
    gblEmployeeObject = null;
    gblDataObjectsArray = [];
    gblSegEmailLastSelIndex = 0;
    gblMFInputs = {};
    gblIsSyncInProgress = false;
    gblGeoPosition = "";
    gblSaveActionFlags = null;
    gblShowCustomProgress = false;
    gblImageData = null;
    gblLocationData = [];
    selectedEmail = "";
}
/** Reset global variables **/
function resetGlobals() {
    gblEmployeeObject = null;
    gblDataObjectsArray = [];
    gblSegEmailLastSelIndex = 0;
    gblMFInputs = {};
    gblIsSyncInProgress = false;
    gblGeoPosition = "";
    gblSaveActionFlags = null;
    gblShowCustomProgress = false;
    gblImageData = null;
}
/** Device back button action **/
function onDeviceBackButtonCall() {
    var frmObject = kony.application.getCurrentForm();
    var prevFrmObject = kony.application.getPreviousForm();
    if (frmObject.id == VCConstant.FORM_LOGIN) {
        /* Nothing to do */
    } else if (frmObject.id == VCConstant.FORM_HOME) {
        /* Checking for any form value change modified for handling back button */
        if (checkUnsavedDataExistInDataObjects()) {
            Home.lblWarningMsg.text = "Unsaved data exists. Do you want to continue ?";
            Home.flxBottomMsgContainer.isVisible = true;
            Home.btnYes.isVisible = true;
            Home.btnNo.isVisible = true;
        }
    } else if (frmObject.id == VCConstant.FORM_CONTENT || frmObject.id == VCConstant.FORM_TEMP) {
        prevFrmObject.show();
    }
}
/** To fetch current device locale  **/
function getCurrentDevLocale() {
    var curDevLocale = kony.i18n.getCurrentDeviceLocale();
    var language = curDevLocale.language;
    var country = curDevLocale.country;
    var devLocale = (!isEmpty(language) && !isEmpty(country)) ? (language + "_" + toUpperCase(country)) : VCConstant.DEF_LOCALE;
}
/** To change current device locale  **/
function changeDeviceLocale(selLocale) {
    if (kony.i18n.isLocaleSupportedByDevice(selLocale) && kony.i18n.isResourceBundlePresent(selLocale)) {
        kony.i18n.setCurrentLocaleAsync(selLocale, success_setLocaleCallback, failure_setLocaleCallback, {});
    }
}
/** Success callback - change device locale  **/
function success_setLocaleCallback() {
    printMessage("Locale updated");
}
/** Failure callback - change device locale  **/
function failure_setLocaleCallback() {
    printMessage("Set locale action failed");
}
/** To set global employee object **/
function setGblUserObject(employeeInfo) {
    gblEmployeeObject = new Employee();
    gblEmployeeObject.email = (isEmpty(employeeInfo.email) || employeeInfo.email == "null") ? gblMFInputs.email : employeeInfo.email;
    gblEmployeeObject.firstName = (!isEmpty(employeeInfo.firstName)) ? employeeInfo.firstName : gblEmployeeObject.firstName;
    gblEmployeeObject.lastName = (!isEmpty(employeeInfo.lastName)) ? employeeInfo.lastName : gblEmployeeObject.lastName;
    gblEmployeeObject.countryCode = (!isEmpty(employeeInfo.countryCode)) ? employeeInfo.countryCode : gblEmployeeObject.countryCode;
    gblEmployeeObject.businessCode = (!isEmpty(employeeInfo.businessCode)) ? employeeInfo.businessCode : gblEmployeeObject.businessCode;
    gblEmployeeObject.branchNumber = (!isEmpty(employeeInfo.branchNumber)) ? employeeInfo.branchNumber : gblEmployeeObject.branchNumber;
    if (!isEmpty(employeeInfo.employeeCode)) {
        gblEmployeeObject.setEmployeeCode(employeeInfo.employeeCode);
    }
}
/** To configure data objects array for populating home segment **/
function configureDataObject() {
    gblDataObjectsArray = [];
    var categoriesCount = statCategories.length;
    if (categoriesCount > 0) {
        var anItemObj = null;
        var aCategoryObj = null;
        var elementsCount = 0;
        var anElementItem = null;
        var aCheckItemObject = null;
        for (var i = 0; i < categoriesCount; i++) {
            anItemObj = statCategories[i];
            aCategoryObj = new Category(i);
            aCategoryObj.code = anItemObj.code;
            aCategoryObj.i18n = anItemObj.i18n;
            elementsCount = anItemObj.items.length;
            for (var j = 0; j < elementsCount; j++) {
                anElementItem = anItemObj.items[j];
                aCheckItemObject = new CheckItem();
                aCheckItemObject.code = anElementItem.code;
                aCheckItemObject.i18n = anElementItem.i18n;
                aCheckItemObject.msg_i18n = anElementItem.msg_i18n;
                aCheckItemObject.isOkay = "";
                aCheckItemObject.dbfield = anElementItem.dbfield;
                aCategoryObj.checkItems.push(aCheckItemObject);
            }
            gblDataObjectsArray.push(aCategoryObj);
        }
    }
}