/**********************************************************************
 *	Author  : RI - CTS363601 
 *	Purpose : UI Screen - Home - Methods
 ***********************************************************************/
/** Show home screen **/
function showHomeScreen(userDataObj, isNewUser) {
    setGblUserObject(userDataObj);
    updateWelcomeLabel();
    if (isNewUser === true) {
        insertEmployeeDetailsTransaction();
    }
    configureDataObject();
    Home.imgAllCheck.info = {
        "isAllChecked": false
    };
    Home.imgAllCheck.src = VCConstant.ICON_CHECKBOX_OFF;
    loadHomeSegment();
    Home.show();
}
/** Home screen - PreShow **/
function onPreShowHomeScreen() {
    dismissLoading();
    Home.flxPopupOverlay.setVisibility(false);
    restoreVehicleReg();
    Home.lblAllChecksOk.text = getI18nString("common.label.allCheckOk");
    Home.lblPopupTitle.text = getI18nString("common.label.chooseUser");
    Home.btnOk.text = getI18nString("common.label.ok");
    Home.lblMessage.text = "";
    Home.lblWarningMsg.text = "";
    Home.btnYes.text = getI18nString("common.label.yes");
    Home.btnNo.text = getI18nString("common.label.no");
    Home.txtMileage.placeholder = getI18nString("common.placeholder.Mileage");
}
/** Home screen - PostShow **/
function onPostShowHomeScreen() {
    showLoading("common.message.loading");
    refreshSettingsUI();
    //ReadFromNFC();
    dismissLoading();
}
/** To load home segment with all vehicle check items **/
function loadHomeSegment(isAllCheck) {
    var categoriesCount = gblDataObjectsArray.length;
    var categoryDataSet = null; /** Includes row data sets **/
    if (categoriesCount > 0) {
        var anItemObj = null;
        var elementsCount = 0;
        var anElementObject = null;
        var anElement = null;
        for (var i = 0; i < categoriesCount; i++) {
            anItemObj = gblDataObjectsArray[i];
            categoryDataSet = [];
            elementsCount = anItemObj.checkItems.length;
            for (var j = 0; j < elementsCount; j++) {
                anElementObject = anItemObj.checkItems[j];
                anElement = {};
                anElement.lblItem = getI18nString(anElementObject.i18n);
                if (!isEmpty(isAllCheck)) {
                    anElement.imgNo = (isAllCheck === false) ? VCConstant.ICON_RADIO_OFF : VCConstant.ICON_RADIO_OFF;
                    anElement.imgOk = (isAllCheck === false) ? VCConstant.ICON_RADIO_OFF : VCConstant.ICON_RADIO_ON;
                } else if (anElementObject.isOkay === "") {
                    anElement.imgNo = VCConstant.ICON_RADIO_OFF;
                    anElement.imgOk = VCConstant.ICON_RADIO_OFF;
                } else {
                    anElement.imgNo = (anElementObject.isOkay === false) ? VCConstant.ICON_RADIO_ON : VCConstant.ICON_RADIO_OFF;
                    anElement.imgOk = (anElementObject.isOkay === false) ? VCConstant.ICON_RADIO_OFF : VCConstant.ICON_RADIO_ON;
                }
                anElement.lblNo = getI18nString("common.label.no");
                anElement.lblOk = getI18nString("common.label.ok");
                anElement.msg_i18n = anElementObject.msg_i18n;
                anElement.flxNoContainer = {
                    info: {
                        tabIndex: i
                    },
                    onClick: ActionHomeSegRowNoRadioBtn
                };
                anElement.flxOkContainer = {
                    info: {
                        tabIndex: i
                    },
                    onClick: ActionHomeSegRowOkRadioBtn
                };
                categoryDataSet.push(anElement);
            }
            if (i === 0) {
                Home.tabPaneOuter.segVehicleChecksOne.setData(categoryDataSet);
            } else if (i === 1) {
                Home.tabPaneOuter.segVehicleChecksTwo.setData(categoryDataSet);
            } else {
                Home.tabPaneOuter.segVehicleChecksThree.setData(categoryDataSet);
            }
        }
    }
}
/** Segment row click  **/
function onClickHomeSegRow(evntObject) {
    var segmentId = evntObject.id;
    var curSelectedRow = Home[segmentId].selectedItems[0];
    Home.lblPopupTitle.text = getI18nString("common.app.title");
    Home.lblMessage.text = getI18nString(curSelectedRow.msg_i18n);
    Home.btnOk.text = getI18nString("common.label.ok");
    Home.flxPopupOverlay.setVisibility(true);
}
/** Home segment onclick seg row Ok radio  **/
function onClickSegRowOkRadioBtn(evntObject) {
    var tabIndex = evntObject.info.tabIndex;
    var segmentId = "segVehicleChecksOne";
    if (parseInt(tabIndex) === 1) {
        segmentId = "segVehicleChecksTwo";
    } else if (parseInt(tabIndex) === 2) {
        segmentId = "segVehicleChecksThree";
    }
    var selectedRowIndex = Home[segmentId].selectedRowIndex[1];
    var selectedSectionIndex = Home[segmentId].selectedRowIndex[0];
    var curSelectedRow = Home[segmentId].selectedItems[0];
    curSelectedRow.imgNo = VCConstant.ICON_RADIO_OFF;
    curSelectedRow.imgOk = VCConstant.ICON_RADIO_ON;
    Home[segmentId].setDataAt(curSelectedRow, selectedRowIndex, selectedSectionIndex);
    updateSelectedItemInDataObjects(selectedRowIndex, parseInt(tabIndex), true);
    var isAllChecked = checkAllItemsAreOkInDataObjects();
    Home.imgAllCheck.info = {
        "isAllChecked": isAllChecked
    };
    Home.imgAllCheck.src = (isAllChecked === true) ? VCConstant.ICON_CHECKBOX_ON : VCConstant.ICON_CHECKBOX_OFF;
}
/** Home segment onclick seg row No radio  **/
function onClickSegRowNoRadioBtn(evntObject) {
    var tabIndex = evntObject.info.tabIndex;
    var segmentId = "segVehicleChecksOne";
    if (parseInt(tabIndex) === 1) {
        segmentId = "segVehicleChecksTwo";
    } else if (parseInt(tabIndex) === 2) {
        segmentId = "segVehicleChecksThree";
    }
    var selectedRowIndex = Home[segmentId].selectedRowIndex[1];
    var selectedSectionIndex = Home[segmentId].selectedRowIndex[0];
    var curSelectedRow = Home[segmentId].selectedItems[0];
    curSelectedRow.imgNo = VCConstant.ICON_RADIO_ON;
    curSelectedRow.imgOk = VCConstant.ICON_RADIO_OFF;
    Home[segmentId].setDataAt(curSelectedRow, selectedRowIndex, selectedSectionIndex);
    updateSelectedItemInDataObjects(selectedRowIndex, parseInt(tabIndex), false);
    Home.imgAllCheck.info = {
        "isAllChecked": false
    };
    Home.imgAllCheck.src = VCConstant.ICON_CHECKBOX_OFF;
}
/** Onclick all checkbox  **/
function onClickAllCheck() {
    var isAllCheckTrue = Home.imgAllCheck.info.isAllChecked;
    Home.imgAllCheck.src = (isAllCheckTrue === true) ? VCConstant.ICON_CHECKBOX_OFF : VCConstant.ICON_CHECKBOX_ON;
    Home.imgAllCheck.info = {
        "isAllChecked": !isAllCheckTrue
    };
    loadHomeSegment(!isAllCheckTrue);
    updateAllItemsInDataObjects(!isAllCheckTrue);
}
/** Overlay popup ok button click  **/
function onClickMsgOverlayPopupOk() {
    Home.flxPopupOverlay.setVisibility(false);
}
/** Update selected item in the data objects array **/
function updateSelectedItemInDataObjects(rowIndex, sectionIndex, isItemOkay) {
    var categoriesCount = gblDataObjectsArray.length;
    var anElementObject = null;
    if (categoriesCount > 0) {
        var anItemObj = null;
        var elementsCount = 0;
        for (var i = 0; i < categoriesCount; i++) {
            if (parseInt(sectionIndex) === i) {
                anItemObj = gblDataObjectsArray[i];
                elementsCount = anItemObj.checkItems.length;
                for (var j = 0; j < elementsCount; j++) {
                    if (parseInt(rowIndex) === j) {
                        anElementObject = anItemObj.checkItems[j];
                        anElementObject.isOkay = isItemOkay;
                        break;
                    }
                }
            }
        }
    }
}
/** Update all items in the data objects array **/
function updateAllItemsInDataObjects(isAllCheck) {
    var categoriesCount = gblDataObjectsArray.length;
    if (categoriesCount > 0) {
        var anItemObj = null;
        var elementsCount = 0;
        var anElementObject = null;
        for (var i = 0; i < categoriesCount; i++) {
            anItemObj = gblDataObjectsArray[i];
            elementsCount = anItemObj.checkItems.length;
            for (var j = 0; j < elementsCount; j++) {
                anElementObject = anItemObj.checkItems[j];
                anElementObject.isOkay = (isAllCheck === false) ? "" : true;
            }
        }
    }
}
/** Check all items are "Ok" in data objects **/
function checkAllItemsAreOkInDataObjects() {
    var categoriesCount = gblDataObjectsArray.length;
    var isAllAreOk = true;
    if (categoriesCount > 0) {
        var anItemObj = null;
        var elementsCount = 0;
        var anElementObject = null;
        for (var i = 0; i < categoriesCount; i++) {
            anItemObj = gblDataObjectsArray[i];
            elementsCount = anItemObj.checkItems.length;
            for (var j = 0; j < elementsCount; j++) {
                anElementObject = anItemObj.checkItems[j];
                if (anElementObject.isOkay === false || anElementObject.isOkay === "") {
                    isAllAreOk = false;
                    break;
                }
            }
        }
    }
    return isAllAreOk;
}
/** Check unsaved data exist in data objects **/
function checkUnsavedDataExistInDataObjects() {
    var categoriesCount = gblDataObjectsArray.length;
    var unsavedData = false;
    if (categoriesCount > 0) {
        var anItemObj = null;
        var elementsCount = 0;
        var anElementObject = null;
        for (var i = 0; i < categoriesCount; i++) {
            anItemObj = gblDataObjectsArray[i];
            elementsCount = anItemObj.checkItems.length;
            for (var j = 0; j < elementsCount; j++) {
                anElementObject = anItemObj.checkItems[j];
                if (anElementObject.isOkay === false || anElementObject.isOkay === true) {
                    unsavedData = true;
                    break;
                }
            }
        }
    }
    return unsavedData;
}
/** On click overlay layer **/
function onClickOverlayLayer() {
    Home.flxBottomMsgContainer.setVisibility(false);
}
/** This function will pick the Vehicle Reg number from local storage **/
function restoreVehicleReg() {
    if (kony.store.getItem(VCConstant.STORE_VEHICLE_NO) !== null) {
        var storedRegNo = kony.store.getItem(VCConstant.STORE_VEHICLE_NO);
        Home.txtVehicleRegNumber.text = storedRegNo;
    } else {
        Home.txtVehicleRegNumber.placeholder = "Reg Number";
    }
}
/** On click save button **/
function onClickSaveBtn() {
    showLoading("common.message.loading");
    if (!isEmpty(Home.txtVehicleRegNumber.text)) {
        gblShowCustomProgress = false;
        var vehicleRegNo = Home.txtVehicleRegNumber.text.trim();
        kony.store.setItem(VCConstant.STORE_VEHICLE_NO, vehicleRegNo);
        captureCurrentLocation(success_vCheckGeoLocCaptureCallback, failure_vCheckGeoLocCaptureCallback);
    } else {
        Home.lblWarningMsg.text = getI18nString("common.message.enterRegNo");
        Home.btnYes.setVisibility(false);
        Home.btnNo.setVisibility(false);
        Home.flxBottomMsgContainer.setVisibility(true);
        dismissLoading();
    }
}
/** On camera photo capture method **/
function onCameraPhotoCapture() {
    Home.tabPaneOuter.tabFour.imgPhoto.rawBytes = Home.camWidget.rawBytes;
    gblImageData = Home.tabPaneOuter.tabFour.imgPhoto.rawBytes;
    printMessage("gblImageData - 1 : " + gblImageData);
    Home.tabPaneOuter.activeTabs = [3];
}
/** Save vehicle check details final flow  **/
function finishActionSaveVehicleChecks() {
    var isAllCheckTrue = Home.imgAllCheck.info.isAllChecked;
    if (isAllCheckTrue !== true) {
        composeAndSendEmail();
    } else {
        Home.lblWarningMsg.text = getI18nString("common.message.dataSaved");
        Home.btnYes.setVisibility(false);
        Home.btnNo.setVisibility(false);
        Home.flxBottomMsgContainer.setVisibility(true);
        resetForm();
        dismissLoading();
    }
}
/** Compose and send mail **/
function composeAndSendEmail() {
    /* the variables defined here are used to generate the email message body text. The default value is "No input" this is then replaced by the input of the redio button in the app */
    var msgbody = "";
    var msgSubject = getI18nString("common.label.vehicleChkFail");
    var msgToRecipients = [];
    var msgCcRecipients = [];
    var msgBccRecipients = [];
    var msgAttachments = [];
    var categoriesCount = gblDataObjectsArray.length;
    var vehicleRegNo = Home.txtVehicleRegNumber.text.trim();
    if (categoriesCount > 0) {
        msgbody += "\n\n" + getI18nString("common.label.vehicleRegNo") + " - " + vehicleRegNo + "\n";
        var anItemObj = null;
        var elementsCount = 0;
        var anElementObject = null;
        var result = "";
        for (var i = 0; i < categoriesCount; i++) {
            anItemObj = gblDataObjectsArray[i];
            msgbody += "\n" + getI18nString(anItemObj.i18n).trim() + ":\n";
            elementsCount = anItemObj.checkItems.length;
            for (var j = 0; j < elementsCount; j++) {
                anElementObject = anItemObj.checkItems[j];
                if (anElementObject.isOkay === false) {
                    result = getI18nString("common.label.no");
                } else if (anElementObject.isOkay === true) {
                    result = getI18nString("common.label.ok");
                } else {
                    result = getI18nString("common.label.noInput");
                }
                msgbody += getI18nString(anElementObject.i18n) + " -\t";
                msgbody += result + "\n";
            }
        }
        msgbody = msgbody + "\n" + gblGeoPosition + "\n";
    }
    try {
        /* this checks if a picture has been taken, if not then the attachment variable is set to null */
        if (!isEmpty(Home.tabPaneOuter.tabFour.imgPhoto.rawBytes)) {
            msgAttachments.push({
                mimetype: "image/*",
                attachment: Home.tabPaneOuter.tabFour.imgPhoto.rawBytes
            });
        }
        kony.phone.openEmail(msgToRecipients, msgCcRecipients, msgBccRecipients, msgSubject, msgbody, false, msgAttachments);
    } catch (err) {
        printMessage("Error in opening Email:: " + err);
    }
    resetForm();
    dismissLoading();
}
/** To reset form **/
function resetForm() {
    Home.imgAllCheck.info = {
        "isAllChecked": false
    };
    Home.imgAllCheck.src = VCConstant.ICON_CHECKBOX_OFF;
    Home.tabPaneOuter.tabFour.imgPhoto.src = "icon_noimage.png";
    gblImageData = null;
    Home.tabPaneOuter.activeTabs = [0];
    updateAllItemsInDataObjects(false);
    Home.txtMileage.text = "";
    Home.txtMileage.placeholder = getI18nString("common.placeholder.Mileage");
    loadHomeSegment();
}