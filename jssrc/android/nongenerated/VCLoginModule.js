/**********************************************************************
 *	Author  : RI - CTS363601 
 *	Purpose : UI Screen - Content - Methods
 ***********************************************************************/
/** Login screen - PreShow **/
function onPreShowLoginScreen() {
    printMessage("#***# Inside Login - Pre Show #***#");
    gblEmployeeObject = null;
    Login.flxPopupOverlayContainer.setVisibility(false);
    Login.lblUsername.text = getI18nString("common.message.welcome");
    Login.lblPopupTitle.text = getI18nString("common.label.chooseUser");
    Login.btnCancel.text = getI18nString("common.label.cancel");
    Login.btnOk.text = getI18nString("common.label.ok");
    Login.segEmailList.setData([]);
    Login.listBusiness.masterData = businessCodes;
}
/** Login screen - PostShow **/
function onPostShowLoginScreen() {
    printMessage("#***# Inside Login - Post Show #***#");
    showLoading("common.message.loading");
    initSyncModule();
}
/** Sync initialization - Success callback  **/
function success_initSyncModule(response) {
    printMessage("#***# Inside Init Sync - Success #***#");
    getEmployeeDetailsTransaction();
}
/** Sync initialization - Failure callback  **/
function failure_initSyncModule(response) {
    printMessage("#***# Inside Init Sync - Failure #***#");
    dismissLoading();
}
/** Success callback - SQL query execution - get employee records **/
function success_getEmployeeDetails(result) {
    printMessage("#***# Inside success_getEmployeeDetails #***#");
    if (result !== null && result.length > 0) {
        var rowItem = null;
        for (var i = 0; i < result.length; i++) {
            rowItem = result[i];
            showHomeScreen(rowItem, false);
        }
    } else {
        getDeviceConfiguredEmails();
    }
}
/** Error callback - SQL query execution - get employee records **/
function error_getEmployeeDetails(error) {
    printMessage("#***# Inside error_getEmployeeDetails #***#");
    getDeviceConfiguredEmails();
}
/** Validate configured emails in device **/
function getDeviceConfiguredEmails() {
    Login.flexPopupBranch.setVisibility(false);
    var configuredEmails = EmailChecker.getEmail();
    var emailsCount = configuredEmails.length;
    var segMasterData = [];
    if (emailsCount > 0) {
        if (emailsCount > 1) {
            var aRecord = null;
            var anItem = null;
            for (var i = 0; i < emailsCount; i++) {
                aRecord = configuredEmails[i];
                anItem = {};
                anItem.lblItem = aRecord;
                anItem.imgSelection = (i === 0) ? VCConstant.ICON_RADIO_ON : VCConstant.ICON_RADIO_OFF;
                segMasterData.push(anItem);
            }
            Login.segEmailList.setData(segMasterData);
            gblSegEmailLastSelIndex = 0;
            Login.flxPopupOverlayContainer.setVisibility(true);
            Login.flxPopupContainer.setVisibility(true);
            Login.flexPopupBranch.setVisibility(false);
            dismissLoading();
        } else {
            //	if (isNetworkAvailable()){
            /*** callAuthorizeEmail(configuredEmails[0]); ***/
            var anInputObj = {};
            anInputObj.email = configuredEmails[0];
            //showHomeScreen(anInputObj, true);
            Login.flxPopupOverlayContainer.setVisibility(true);
            Login.flxPopupContainer.setVisibility(true);
            Login.flexPopupBranch.setVisibility(false);
            selectedEmail = configuredEmails[0];
            Login.flexPopupBranch.setVisibility(true);
            Login.listBusiness.selectedKey = VCConstant.SELECT;
            Login.listbranchnumbers.masterData = [
                [VCConstant.SELECT, VCConstant.SELECT]
            ];
            Login.listbranchnumbers.selectedKey = VCConstant.SELECT;
            Login.flxPopupContainer.setVisibility(false);
            Login.listbranchnumbers.setVisibility(false);
            dismissLoading();
        }
    } else {
        showMessagePopup("common.message.noAccountsConfigured", onClickMessagePopupOkButton);
        dismissLoading();
    }
}
/** On click message popup Ok button action general **/
function onClickMessagePopupOkButton() {
    showLoading("common.message.loading");
    dismissMessagePopup();
    dismissLoading();
    exitApplication();
}
/** To update welcome label text **/
function updateWelcomeLabel() {
    /*** Login.lblUsername.text				= getI18nString("common.message.welcome") + " " + gblEmployeeObject.getEmployeeName(); ***/
    Login.lblUsername.text = getI18nString("common.message.welcome");
}
/** Integration service call for user authentication  **/
function callAuthorizeEmail(selectedEmail) {
    var inputParams = {};
    inputParams.serviceName = "AuthorizeUserSIT";
    inputParams.operationName = "getEmployeeSIT";
    inputParams.inputs = {
        "countryCode": VCConstant.DEF_COUNTRY_CODE,
        "businessCode": VCConstant.DEF_BUSINESS_CODE,
        "email": selectedEmail,
        "languageCode": VCConstant.DEF_LANGUAGE_CODE
    };
    gblMFInputs.email = selectedEmail;
    callAuthenticateUserIntService(inputParams);
}
/** Success callback - Integration service - Autorize Email  **/
function success_authorizeEmailCallback(response) {
    if (response.httpStatusCode === VCConstant.SERVICE_HTTP_STATUS) {
        if (response.opstatus === VCConstant.SERVICE_OPSTATUS) {
            if (response.message === VCConstant.SERVICE_MSG_SUCCESS) {
                showHomeScreen(response, true);
            } else {
                showMessagePopup("common.message.invalidUser", onClickMessagePopupOkButton);
                dismissLoading();
            }
        } else {
            showMessagePopup("common.message.serviceDown", onClickMessagePopupOkButton);
        }
        dismissLoading();
    } else {
        showMessagePopup("common.message.serviceUnavailable", ActionOnMessagePopupOkBtn);
        dismissLoading();
    }
}
/** Failure callback - Integration service - Autorize Email  **/
function failure_authorizeEmailCallback(error) {
    printMessage("Kony SDK - Integration service failed - Authorize email");
    showMessagePopup("common.message.serviceDown", onClickMessagePopupOkButton);
    dismissLoading();
}
/** On click Emails popup Ok button action **/
function onClickEmailsPopupOkButton() {
    //showLoading("common.message.loading");
    var selectedItem = Login.segEmailList.data[gblSegEmailLastSelIndex];
    selectedEmail = selectedItem.lblItem;
    printMessage("selectedEmail111----------->>>>" + selectedEmail);
    //  	if (isNetworkAvailable()){
    /*** callAuthorizeEmail(selectedEmail); ***/
    // 	var anInputObj					= {};
    // 	anInputObj.email				= selectedEmail;
    //showHomeScreen(anInputObj, true);
    //calling the popup
    // showBranchPopup(selectedEmail);
    Login.flexPopupBranch.setVisibility(true);
    Login.listBusiness.selectedKey = VCConstant.SELECT;
    Login.listbranchnumbers.masterData = [
        [VCConstant.SELECT, VCConstant.SELECT]
    ];
    Login.listbranchnumbers.selectedKey = VCConstant.SELECT;
    Login.flxPopupContainer.setVisibility(false);
    Login.listbranchnumbers.setVisibility(false);
    /*  } else {
        	showMessagePopup("common.message.noNetwork", onClickMessagePopupOkButton);
        	dismissLoading();
      }*/
}
/** On click Emails popup Cancel button action **/
function onClickEmailsPopupCancelButton() {
    showLoading("common.message.loading");
    Login.flxPopupOverlayContainer.setVisibility(false);
    dismissLoading();
    exitApplication();
}
/** Selecting an email from segment list **/
function onClickSegEmailRow() {
    showLoading("common.message.loading");
    var selectedRowIndex = Login.segEmailList.selectedRowIndex[1];
    var selectedSectionIndex = Login.segEmailList.selectedRowIndex[0];
    if (selectedRowIndex !== gblSegEmailLastSelIndex) {
        var preSelectedRow = Login.segEmailList.data[gblSegEmailLastSelIndex];
        var curSelectedRow = Login.segEmailList.data[selectedRowIndex];
        preSelectedRow.imgSelection = "radio_off.png";
        curSelectedRow.imgSelection = "radio_on.png";
        Login.segEmailList.setDataAt(preSelectedRow, gblSegEmailLastSelIndex, selectedSectionIndex);
        Login.segEmailList.setDataAt(curSelectedRow, selectedRowIndex, selectedSectionIndex);
        gblSegEmailLastSelIndex = selectedRowIndex;
    }
    dismissLoading();
}

function onClickBranchPopupOkButton() {
    showLoading("common.message.loading");
    printMessage("selectedEmail----------->>>>" + selectedEmail);
    //validate business and brnach entered
    if ((Login.listbranchnumbers.selectedKey != VCConstant.SELECT) && (Login.listBusiness.selectedKey != VCConstant.SELECT)) {
        Login.flxPopupContainer.setVisibility(false);
        Login.flexPopupBranch.setVisibility(false);
        //var selectedItem	= Login.segEmailList.data[gblSegEmailLastSelIndex];
        //	var selectedEmail	= selectedItem.lblItem;
        if (isNetworkAvailable()) {
            /*** callAuthorizeEmail(selectedEmail); ***/
            var anInputObj = {};
            anInputObj.email = selectedEmail;
            anInputObj.businessCode = Login.listBusiness.selectedKey;
            anInputObj.branchNumber = Login.listbranchnumbers.selectedKey;
            printMessage("anInputObj  : " + JSON.stringify(anInputObj));
            showHomeScreen(anInputObj, true);
        } else {
            showMessagePopup("common.message.noNetwork", onClickMessagePopupOkButton);
            dismissLoading();
        }
    } else {
        Login.lblWarningMsg.text = "Business and Branch are mandatory";
        Login.btnYes.setVisibility(false);
        Login.flxBottomMsgContainer.setVisibility(true);
        dismissLoading();
    }
}

function onClickBranchOverlayLayer() {
    Login.flxBottomMsgContainer.setVisibility(false);
}

function updateBranchObj(widgetRef) {
    printMessage("updateBranchObj --->>>" + JSON.stringify(widgetRef));
    //  var comboRef											= widgetRef.id.slice(-1);
    Login.listbranchnumbers.masterData = [];
    var selectedKey = Login.listBusiness.selectedKey;
    if (selectedKey == VCConstant.SELECT) {
        Login.listbranchnumbers.setVisibility(false);
        Login.listbranchnumbers.selectedKey = VCConstant.SELECT;
    }
    if (selectedKey == VCConstant.PEST) {
        Login.listbranchnumbers.setVisibility(true);
        printMessage("updateBranchObj --->>>selectedKey" + selectedKey);
        Login.listbranchnumbers.masterData = pestbranches;
        Login.listbranchnumbers.selectedKey = VCConstant.SELECT;
    } else if (selectedKey == VCConstant.HYGIENE) {
        Login.listbranchnumbers.setVisibility(true);
        printMessage("updateBranchObj --->>>selectedKey" + selectedKey);
        Login.listbranchnumbers.masterData = hygienebranches;
        Login.listbranchnumbers.selectedKey = VCConstant.SELECT;
    }
}