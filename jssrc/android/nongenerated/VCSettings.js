/**********************************************************************
 *	Author  : RI - CTS363601 
 *	Purpose : UI Screen - Settings(Burger Menu) - Methods
 ***********************************************************************/
/** Settings Menu (Burger Menu)- UI refreshing like i18n updates, default visibility and all **/
function refreshSettingsUI() {
    var frmObj = kony.application.getCurrentForm();
    var usernameStr = (!isEmpty(gblEmployeeObject)) ? (gblEmployeeObject.getEmployeeName()) : "";
    frmObj.flxSettingsContainer.centerX = "-50%";
    frmObj.lblVersion.text = getI18nString("common.label.version") + " " + VCConstant.APP_VERSION;
    /*** frmObj.lblUsername.text					= getI18nString("common.message.welcome") + " " + usernameStr; ***/
    frmObj.lblUsername.text = usernameStr;
    frmObj.lblAbout.text = getI18nString("common.label.about");
    frmObj.lblLogout.text = getI18nString("common.label.logout");
    frmObj.lblSync.text = getI18nString("common.label.sync");
    displayLastSyncDate();
    //Settings screen Gesture configuration.
    var setupTblSwipe = {
        fingers: 1,
        swipedistance: 50,
        swipevelocity: 75
    }; //Swipe gesture
    frmObj.flxSettingsContainer.setGestureRecognizer(2, setupTblSwipe, handleSwipeGestureOnSettings);
}
/** Settings Menu (Burger Menu)- Open menu button action **/
function onClickOpenMenuButton() {
    var frmObj = kony.application.getCurrentForm();
    displayLastSyncDate();

    function openMenu_Callback() {}
    frmObj.flxSettingsContainer.animate(kony.ui.createAnimation({
        "100": {
            "centerX": "50%",
            "stepConfig": {
                "timingFunction": kony.anim.LINEAR
            }
        }
    }), {
        "delay": 0,
        "iterationCount": 1,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 0.25
    }, {
        "animationEnd": openMenu_Callback
    });
}
/** Settings Menu (Burger Menu)- Close menu button action **/
function closeSettingsMenu() {
    var frmObj = kony.application.getCurrentForm();

    function closeMenu_Callback() {}
    frmObj.flxSettingsContainer.animate(kony.ui.createAnimation({
        "100": {
            "centerX": "-50%",
            "stepConfig": {
                "timingFunction": kony.anim.LINEAR
            }
        }
    }), {
        "delay": 0,
        "iterationCount": 1,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 0.25
    }, {
        "animationEnd": closeMenu_Callback
    });
}
/** Settings Menu (Burger Menu)- About button action **/
function onClickAbout() {
    showContentScreen();
}
/** Settings Menu (Burger Menu)- Sync button action **/
function onClickSync() {
    closeSettingsMenu();
    gblShowCustomProgress = true;
    initDataSync();
}
/** Settings Menu (Burger Menu)- Logout button action **/
function onClickLogout() {
    showLoading("common.message.loading");
    resetGlobals();
    deleteEmployeeDetailsTransaction();
}
/** Success callback - SQL query execution - delete employee record **/
function success_deleteEmployeeDetails() {
    dismissLoading();
    exitApplication();
}
/** Error callback - SQL query execution - delete employee record **/
function error_deleteEmployeeDetails(error) {
    dismissLoading();
    exitApplication();
}
/** Success callback - Delete records query on Employee table  **/
function success_deleteEmployee(transactionId, resultset) {
    printMessage("Delete employee table transaction success");
    resetGlobals();
    dismissLoading();
    exitApplication();
}
/** Failure callback - Delete records query on Employee table  **/
function failure_deleteEmployee(error) {
    printMessage("Delete employee table transaction failed: " + JSON.stringify(error));
}
/** The below function will get invoked when a swipe gesture is recognized **/
function handleSwipeGestureOnSettings(widgetRef, gestureInfo) {
    if (!isEmpty(widgetRef) && widgetRef.id == "flxSettingsContainer" && !isEmpty(gestureInfo) && gestureInfo[gestureType] == 2) {
        if (gestureInfo.swipeDirection == 1) {
            // Swipe to left
        } else if (gestureInfo.swipeDirection == 2) {
            // Swipe to right
        } else {
            // Nothing to do with top and bottom direction
        }
        var frmObj = kony.application.getCurrentForm();
        closeSettingsMenu();
    }
}
/** Last sync date displya method **/
function displayLastSyncDate() {
    var frmObj = kony.application.getCurrentForm();
    var lastSyncDate = kony.store.getItem(VCConstant.LAST_SYNC_DATE);
    if (!isEmpty(lastSyncDate)) {
        frmObj.lblLastSync.text = getI18nString("common.label.lastSyncDate");
        frmObj.lblLastSyncDate.text = (!isEmpty(lastSyncDate)) ? lastSyncDate : "";
    } else {
        frmObj.lblLastSync.text = "";
        frmObj.lblLastSyncDate.text = "";
    }
}