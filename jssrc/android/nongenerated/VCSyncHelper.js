/**********************************************************************
 *	Author  : RI - CTS363601 
 *	Purpose : Module to refer sync based methods
 ***********************************************************************/
/** Start sync session **/
function initDataSync() {
    printMessage("sync started successfully------> syncing starts");
    if (gblShowCustomProgress === true) {
        showProgressPopup("common.message.syncStarting", "common.label.dataSync");
    }
    gblIsSyncInProgress = true;
    var config = {};
    config.userid = VCConstant.SYNC_USER_ID;
    config.password = VCConstant.SYNC_PASSWORD;
    config.appid = VCConstant.SYNC_APP_ID;
    config.serverhost = VCConstant.SYNC_SERVER_HOST;
    config.serverport = VCConstant.SYNC_SERVER_PORT;
    config.issecure = VCConstant.SYNC_IS_SECURE;
    config.sessiontasks = {
        "VCScope": {
            doupload: VCConstant.SYNC_DO_UPLOAD,
            dodownload: VCConstant.SYNC_DO_DOWNLOAD,
            uploaderrorpolicy: VCConstant.SYNC_UPLOAD_ERROR_POLICY
        },
        "EmployeeScope": {
            doupload: false,
            dodownload: false,
            uploaderrorpolicy: VCConstant.SYNC_UPLOAD_ERROR_POLICY
        }
    };
    config.removeafterupload = {
        "VCScope": []
    };
    //sync life cycle callbacks
    config.onsyncstart = onSyncStartCallback;
    config.onscopestart = onScopeStartCallback;
    config.onscopecerror = onScopeErrorCallback;
    config.onscopesuccess = onScopeSuccessCallback;
    config.onuploadstart = onUploadStartCallback;
    config.onuploadsuccess = onUploadSuccessCallback;
    config.onsyncsuccess = onSyncSuccessCallback;
    config.onsyncerror = onSyncErrorCallBack;
    sync.startSession(config);
}
/** Stop sync session **/
function stopSyncSession() {
    sync.stopSession(onSyncStopSessionCallback);
}
/** Reset sync **/
function resetSync() {
    showLoading("common.message.loading");
    sync.reset(onSyncResetSuccessCallback, onSyncResetFailureCallback);
}
/** Sync start callback **/
function onSyncStartCallback(response) {
    printMessage("onSyncStartCallback: " + JSON.stringify(response));
}
/** Sync scope start callback **/
function onScopeStartCallback(response) {
    printMessage("onScopeStartCallback: " + JSON.stringify(response));
    if (gblShowCustomProgress === true) {
        updateProgressMessage("common.message.syncInProgress");
    }
}
/** Sync scope error callback **/
function onScopeErrorCallback(response) {
    printMessage("onScopeErrorCallback: " + JSON.stringify(response));
}
/** Sync scope success callback **/
function onScopeSuccessCallback(response) {
    printMessage("onScopeSuccessCallback: " + JSON.stringify(response));
    if (gblShowCustomProgress === true) {
        updateProgressMessage("common.message.syncDone");
    }
}
/** Sync upload start callback **/
function onUploadStartCallback(response) {
    printMessage("onUploadStartCallback: " + JSON.stringify(response));
}
/** Sync upload success callback **/
function onUploadSuccessCallback(response) {
    printMessage("onUploadSuccessCallback: " + JSON.stringify(response));
}
/** Sync success callback **/
function onSyncSuccessCallback(response) {
    printMessage("######################onSyncSuccessCallback: " + JSON.stringify(response));
    var today = new Date();
    var dateStr = today.toLocaleDateString();
    var timeStr = today.toLocaleTimeString();
    var syncDateStr = dateStr + " " + timeStr;
    kony.store.setItem(VCConstant.LAST_SYNC_DATE, syncDateStr);
    if (gblShowCustomProgress === true) {
        dismissProgressMessage();
    }
    gblIsSyncInProgress = false;
    resetSync();
    dismissLoading();
    if (gblShowCustomProgress === false) {
        finishActionSaveVehicleChecks();
    }
}
/** Sync error callback **/
function onSyncErrorCallBack(response) {
    printMessage("onSyncErrorCallBack: " + JSON.stringify(response));
    if (gblShowCustomProgress === true) {
        dismissProgressMessage();
    }
    gblIsSyncInProgress = false;
    dismissLoading();
    if (gblShowCustomProgress === false) {
        finishActionSaveVehicleChecks();
    }
}
/** Sync stop session callback **/
function onSyncStopSessionCallback(response) {
    printMessage("onSyncStopSessionCallback: " + JSON.stringify(response));
    gblIsSyncInProgress = false;
    dismissLoading();
}
/** Sync reset success callback **/
function onSyncResetSuccessCallback(response) {
    printMessage("onSyncResetSuccessCallback: " + JSON.stringify(response));
    dismissLoading();
}
/** Sync reset failure callback **/
function onSyncResetFailureCallback(response) {
    printMessage("onSyncResetFailureCallback: " + JSON.stringify(response));
    dismissLoading();
}