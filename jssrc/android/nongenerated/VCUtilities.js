/**********************************************************************
 *	Author  : RI - CTS363601 
 *	Purpose : Module to refer all utility methods
 ***********************************************************************/
/** To show loading indicator **/
function showLoading(messageKey) {
    var lodingText = getI18nString(messageKey);
    try {
        kony.application.showLoadingScreen("sknLoading", lodingText, "center", true, true, null);
    } catch (e) {
        kony.print("Error while showing loading indicator :" + e);
    }
}
/** To dismiss loading indicator **/
function dismissLoading() {
    kony.application.dismissLoadingScreen();
}
/** To get i18n localized string for given key **/
function getI18nString(key) {
    return key ? kony.i18n.getLocalizedString(key) : "";
}
/** To remove whitespaces  **/
function trim(inputString) {
    var outputString = "";
    if (inputString !== null) {
        outputString = kony.string.trim(inputString);
    }
    return outputString;
}
/** To check a string is valid or not  **/
function isEmpty(val) {
    return (val === undefined || val === null || val === '' || val.length <= 0) ? true : false;
}
/** To make given string to uppercase  **/
function toUpperCase(inputString) {
    var outputVal = "";
    if (!isEmpty(inputString)) {
        outputVal = inputString.toUpperCase();
    }
    return outputVal;
}
/** To make given string to uppercase  **/
function toLowerCase(inputString) {
    var outputVal = "";
    if (!isEmpty(inputString)) {
        outputVal = inputString.toLowerCase();
    }
    return outputVal;
}
/** To add a leading zero on given string  **/
function appendLeadingZero(data) {
    return (parseInt(data) < 10) ? '0' + data : data;
}
/** To validate string length limit  **/
function isValidCharsCountInString(textStr, charLimit) {
    var boolResult = true;
    if (!isEmpty(textStr)) {
        if (textStr.length > charLimit) {
            boolResult = false;
        }
    }
    return boolResult;
}
/** To get Current locale  **/
function getCurrentLocale() {
    var currentLocale = "";
    try {
        currentLocale = kony.i18n.getCurrentLocale();
    } catch (i18nError) {
        printMessage("Exception While getting currentLocale  : " + i18nError);
    }
    return currentLocale;
}
/** To check network availability  **/
function isNetworkAvailable() {
    return kony.net.isNetworkAvailable(constants.NETWORK_TYPE_ANY);
}
/** To exit application  **/
function exitApplication() {
    kony.application.exit();
}
/** To print a message on console  **/
function printMessage(message) {
    kony.print(message);
}
/** To get a matching record from given array of elements  **/
function getMatchingItemFromArray(recordsArray, matchingKey, matchingValue) {
    var matchingItem = null;
    if (!isEmpty(recordsArray) && recordsArray.length > 0) {
        var arrayLength = recordsArray.length;
        var aRecord = null;
        for (var i = 0; i < arrayLength; i++) {
            aRecord = recordsArray[i];
            if (aRecord[matchingKey] === matchingValue) {
                matchingItem = aRecord;
                break;
            }
        }
    }
    return matchingItem;
}
/** To find index of a record from an array  **/
function getItemIndexFromArray(recordsArray, record) {
    var index = (!isEmpty(recordsArray) && !isEmpty(record)) ? recordsArray.indexOf(record) : -1;
    return index;
}
/** To remove a record from an array  **/
function removeItemFromArray(recordsArray, record) {
    var index = getItemIndexFromArray(recordsArray, record);
    if (index !== -1) {
        recordsArray.splice(index, 1);
    }
}
/** To search a record in given array  **/
function isRecordExist(record, recordsArray, matchingKey, matchingKey2) {
    var result = false;
    if (!isEmpty(recordsArray) && recordsArray.length > 0) {
        var arrayLength = recordsArray.length;
        var aRecord = null;
        for (var i = 0; i < arrayLength; i++) {
            aRecord = recordsArray[i];
            if (aRecord[matchingKey] === record[matchingKey] || (!isEmpty(matchingKey2) && aRecord[matchingKey2] === record[matchingKey2])) {
                result = true;
                break;
            }
        }
    }
    return result;
}
/** To identify matched record from an array  **/
function getMatchedRecordFromArray(recordsArray, matchingKey, matchingValue) {
    var resultRecord = null;
    if (!isEmpty(recordsArray) && recordsArray.length > 0) {
        var arrayLength = recordsArray.length;
        var aRecord = null;
        for (var i = 0; i < arrayLength; i++) {
            aRecord = recordsArray[i];
            if (aRecord[matchingKey] === matchingValue) {
                resultRecord = aRecord;
                break;
            }
        }
    }
    return resultRecord;
}
/**
 Format the Date: DD/MM/YYYY
*/
function getFormatedDate() {
    var dateObj = new Date();
    var formateDateStr = dateObj.getDate() + "/" + dateObj.getMonth() + "/" + dateObj.getFullYear();
    return formateDateStr;
}
/**
Format the Timestamp: HH:MM:SS GMT
*/
function getTimeInUTC() {
    var mDateObj = new Date();
    // var timeStr 	= mDateObj.getUTCHours()+":"+mDateObj.getUTCMinutes()+":"+mDateObj.getUTCSeconds();
    var gmthrs = (mDateObj.getUTCHours() < 10) ? ("0" + mDateObj.getUTCHours().toString()) : mDateObj.getUTCHours();
    var gmtmnts = (mDateObj.getUTCMinutes() < 10) ? ("0" + mDateObj.getUTCMinutes()) : mDateObj.getUTCMinutes();
    var gmtscnds = (mDateObj.getUTCSeconds() < 10) ? ("0" + mDateObj.getUTCSeconds().toString()) : mDateObj.getUTCSeconds();
    var timeStr = gmthrs + ":" + gmtmnts + ":" + gmtscnds;
    return timeStr;
}