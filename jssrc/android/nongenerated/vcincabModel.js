//****************Sync Version:MobileFabricInstaller-QA-7.2.0_v201610191639_r117*******************
// ****************Generated On Fri Nov 25 12:14:24 UTC 2016vcincab*******************
// **********************************Start vcincab's helper methods************************
if (typeof(kony) === "undefined") {
    kony = {};
}
if (typeof(kony.sync) === "undefined") {
    kony.sync = {};
}
if (typeof(kony.sync.log) === "undefined") {
    kony.sync.log = {};
}
if (typeof(sync) === "undefined") {
    sync = {};
}
if (typeof(sync.log) === "undefined") {
    sync.log = {};
}
if (typeof(VCScope) === "undefined") {
    VCScope = {};
}
/************************************************************************************
 * Creates new vcincab
 *************************************************************************************/
VCScope.vcincab = function() {
    this.cabid = null;
    this.regno = null;
    this.email = null;
    this.fuel = null;
    this.Mirrors = null;
    this.Horn = null;
    this.Indicators = null;
    this.GroupLightsA = null;
    this.GroupLightsB = null;
    this.GroupLightsC = null;
    this.Windscreen = null;
    this.Tachograph = null;
    this.Brakes = null;
    this.Wipers = null;
    this.Steering = null;
    this.GeneralSafety = null;
    this.isDeleted = null;
    this.lastModifiedDate = null;
    this.markForUpload = true;
};
VCScope.vcincab.prototype = {
    get cabid() {
        return this._cabid;
    },
    set cabid(val) {
        if (!kony.sync.isEmptyString(val) && !kony.sync.isNull(val) && !kony.sync.isValidNumberType(val)) {
            sync.log.error("Invalid data type for the attribute cabid in vcincab.\nExpected:\"integer\"\nActual:\"" + kony.type(val) + "\"");
        }
        this._cabid = val;
    },
    get regno() {
        return this._regno;
    },
    set regno(val) {
        this._regno = val;
    },
    get email() {
        return this._email;
    },
    set email(val) {
        this._email = val;
    },
    get fuel() {
        return this._fuel;
    },
    set fuel(val) {
        this._fuel = val;
    },
    get Mirrors() {
        return this._Mirrors;
    },
    set Mirrors(val) {
        this._Mirrors = val;
    },
    get Horn() {
        return this._Horn;
    },
    set Horn(val) {
        this._Horn = val;
    },
    get Indicators() {
        return this._Indicators;
    },
    set Indicators(val) {
        this._Indicators = val;
    },
    get GroupLightsA() {
        return this._GroupLightsA;
    },
    set GroupLightsA(val) {
        this._GroupLightsA = val;
    },
    get GroupLightsB() {
        return this._GroupLightsB;
    },
    set GroupLightsB(val) {
        this._GroupLightsB = val;
    },
    get GroupLightsC() {
        return this._GroupLightsC;
    },
    set GroupLightsC(val) {
        this._GroupLightsC = val;
    },
    get Windscreen() {
        return this._Windscreen;
    },
    set Windscreen(val) {
        this._Windscreen = val;
    },
    get Tachograph() {
        return this._Tachograph;
    },
    set Tachograph(val) {
        this._Tachograph = val;
    },
    get Brakes() {
        return this._Brakes;
    },
    set Brakes(val) {
        this._Brakes = val;
    },
    get Wipers() {
        return this._Wipers;
    },
    set Wipers(val) {
        this._Wipers = val;
    },
    get Steering() {
        return this._Steering;
    },
    set Steering(val) {
        this._Steering = val;
    },
    get GeneralSafety() {
        return this._GeneralSafety;
    },
    set GeneralSafety(val) {
        this._GeneralSafety = val;
    },
    get isDeleted() {
        return kony.sync.getBoolean(this._isDeleted) + "";
    },
    set isDeleted(val) {
        if (!kony.sync.isEmptyString(val) && !kony.sync.isNull(val) && !kony.sync.isValidBooleanType(val)) {
            sync.log.error("Invalid data type for the attribute isDeleted in vcincab.\nExpected:\"boolean\"\nActual:\"" + kony.type(val) + "\"");
        }
        this._isDeleted = val;
    },
    get lastModifiedDate() {
        return this._lastModifiedDate;
    },
    set lastModifiedDate(val) {
        this._lastModifiedDate = val;
    },
};
/************************************************************************************
 * Retrieves all instances of vcincab SyncObject present in local database with
 * given limit and offset where limit indicates the number of records to be retrieved
 * and offset indicates number of rows to be ignored before returning the records.
 * e.g. var orderByMap = []
 * orderByMap[0] = {};
 * orderByMap[0].key = "cabid";
 * orderByMap[0].sortType ="desc";
 * orderByMap[1] = {};
 * orderByMap[1].key = "regno";
 * orderByMap[1].sortType ="asc";
 * var limit = 20;
 * var offset = 5;
 * VCScope.vcincab.getAll(successcallback,errorcallback, orderByMap, limit, offset)
 *************************************************************************************/
VCScope.vcincab.getAll = function(successcallback, errorcallback, orderByMap, limit, offset) {
    sync.log.trace("Entering VCScope.vcincab.getAll->main function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = VCScope.vcincab.getTableName();
    orderByMap = kony.sync.formOrderByClause("vcincab", orderByMap);
    var query = kony.sync.qb_createQuery();
    kony.sync.qb_select(query, null);
    kony.sync.qb_from(query, tbname);
    kony.sync.qb_orderBy(query, orderByMap);
    kony.sync.qb_limitOffset(query, limit, offset);
    var query_compile = kony.sync.qb_compile(query);
    var sql = query_compile[0];
    var params = query_compile[1];

    function mySuccCallback(res) {
        sync.log.trace("Entering VCScope.vcincab.getAll->successcallback");
        successcallback(VCScope.vcincab.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
    }
    kony.sync.single_select_execute(dbname, sql, params, mySuccCallback, errorcallback);
};
/************************************************************************************
 * Returns number of vcincab present in local database.
 *************************************************************************************/
VCScope.vcincab.getAllCount = function(successcallback, errorcallback) {
    sync.log.trace("Entering VCScope.vcincab.getAllCount function");
    VCScope.vcincab.getCount("", successcallback, errorcallback);
};
/************************************************************************************
 * Returns number of vcincab using where clause in the local Database
 *************************************************************************************/
VCScope.vcincab.getCount = function(wcs, successcallback, errorcallback) {
    sync.log.trace("Entering VCScope.vcincab.getCount->main function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    if (!kony.sync.validateInput(arguments, "VCScope.vcincab.getCount", "getCount", errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = VCScope.vcincab.getTableName();
    wcs = kony.sync.validateWhereClause(wcs);
    var sql = "select count(*) from \"" + tbname + "\" " + wcs;
    kony.sync.single_execute_sql(dbname, sql, null, mySuccCallback, errorcallback);

    function mySuccCallback(res) {
        sync.log.trace("Entering VCScope.vcincab.getCount->successcallback");
        if (null !== res) {
            var count = null;
            count = res["count(*)"];
            kony.sync.verifyAndCallClosure(successcallback, {
                count: count
            });
        } else {
            sync.log.error("Some error occured while getting the count");
        }
    }
};
/************************************************************************************
 * Creates a new instance of vcincab in the local Database. The new record will 
 * be merged with the enterprise datasource in the next Sync.
 *************************************************************************************/
VCScope.vcincab.prototype.create = function(successcallback, errorcallback) {
    sync.log.trace("Entering  VCScope.vcincab.prototype.create function");
    var valuestable = this.getValuesTable(true);
    VCScope.vcincab.create(valuestable, successcallback, errorcallback, this.markForUpload);
};
VCScope.vcincab.create = function(valuestable, successcallback, errorcallback, markForUpload) {
    sync.log.trace("Entering  VCScope.vcincab.create->main function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    if (!kony.sync.validateInput(arguments, "VCScope.vcincab.create", "create", errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = VCScope.vcincab.getTableName();
    markForUpload = kony.sync.getUploadStatus(markForUpload);
    if (kony.sync.attributeValidation(valuestable, "vcincab", errorcallback, true) === false) {
        return;
    }

    function executeSuccess() {
        sync.log.trace("Entering  VCScope.vcincab.create->success callback");
        kony.sync.single_insert_execute(dbname, tbname, valuestable, successcallback, errorcallback, markForUpload);
    }
    if (kony.sync.enableORMValidations) {
        var relationshipMap = {};
        relationshipMap = VCScope.vcincab.getRelationshipMap(relationshipMap, valuestable);
        kony.sync.checkIntegrity(dbname, relationshipMap, executeSuccess, errorcallback);
    } else {
        kony.sync.single_insert_execute(dbname, tbname, valuestable, successcallback, errorcallback, markForUpload);
    }
};
/************************************************************************************
 * Creates number of new instances of vcincab in the local Database. These new 
 * records will be merged with the enterprise datasource in the next Sync. Based upon 
 * kony.sync.enableORMValidations flag, validations will be enabled/disabled.
 * e.g.	var valuesArray = [];
 *		valuesArray[0] = {};
 *		valuesArray[0].regno = "regno_0";
 *		valuesArray[0].email = "email_0";
 *		valuesArray[0].fuel = "fuel_0";
 *		valuesArray[0].Mirrors = "Mirrors_0";
 *		valuesArray[0].Horn = "Horn_0";
 *		valuesArray[0].Indicators = "Indicators_0";
 *		valuesArray[0].GroupLightsA = "GroupLightsA_0";
 *		valuesArray[0].GroupLightsB = "GroupLightsB_0";
 *		valuesArray[0].GroupLightsC = "GroupLightsC_0";
 *		valuesArray[0].Windscreen = "Windscreen_0";
 *		valuesArray[0].Tachograph = "Tachograph_0";
 *		valuesArray[0].Brakes = "Brakes_0";
 *		valuesArray[0].Wipers = "Wipers_0";
 *		valuesArray[0].Steering = "Steering_0";
 *		valuesArray[0].GeneralSafety = "GeneralSafety_0";
 *		valuesArray[1] = {};
 *		valuesArray[1].regno = "regno_1";
 *		valuesArray[1].email = "email_1";
 *		valuesArray[1].fuel = "fuel_1";
 *		valuesArray[1].Mirrors = "Mirrors_1";
 *		valuesArray[1].Horn = "Horn_1";
 *		valuesArray[1].Indicators = "Indicators_1";
 *		valuesArray[1].GroupLightsA = "GroupLightsA_1";
 *		valuesArray[1].GroupLightsB = "GroupLightsB_1";
 *		valuesArray[1].GroupLightsC = "GroupLightsC_1";
 *		valuesArray[1].Windscreen = "Windscreen_1";
 *		valuesArray[1].Tachograph = "Tachograph_1";
 *		valuesArray[1].Brakes = "Brakes_1";
 *		valuesArray[1].Wipers = "Wipers_1";
 *		valuesArray[1].Steering = "Steering_1";
 *		valuesArray[1].GeneralSafety = "GeneralSafety_1";
 *		valuesArray[2] = {};
 *		valuesArray[2].regno = "regno_2";
 *		valuesArray[2].email = "email_2";
 *		valuesArray[2].fuel = "fuel_2";
 *		valuesArray[2].Mirrors = "Mirrors_2";
 *		valuesArray[2].Horn = "Horn_2";
 *		valuesArray[2].Indicators = "Indicators_2";
 *		valuesArray[2].GroupLightsA = "GroupLightsA_2";
 *		valuesArray[2].GroupLightsB = "GroupLightsB_2";
 *		valuesArray[2].GroupLightsC = "GroupLightsC_2";
 *		valuesArray[2].Windscreen = "Windscreen_2";
 *		valuesArray[2].Tachograph = "Tachograph_2";
 *		valuesArray[2].Brakes = "Brakes_2";
 *		valuesArray[2].Wipers = "Wipers_2";
 *		valuesArray[2].Steering = "Steering_2";
 *		valuesArray[2].GeneralSafety = "GeneralSafety_2";
 *		VCScope.vcincab.createAll(valuesArray, successcallback, errorcallback, true);
 *************************************************************************************/
VCScope.vcincab.createAll = function(valuesArray, successcallback, errorcallback, markForUpload) {
    sync.log.trace("Entering VCScope.vcincab.createAll function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    if (!kony.sync.validateInput(arguments, "VCScope.vcincab.createAll", "createAll", errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = VCScope.vcincab.getTableName();
    markForUpload = kony.sync.getUploadStatus(markForUpload);
    var isProperData = true;
    var arrayLen = 0;
    var errorInfo = [];
    var arrayLength = valuesArray.length;
    var errObject = null;
    var isReferentialIntegrityFailure = false;
    var errMsg = null;
    if (kony.sync.enableORMValidations) {
        var newValuesArray = [];
        //column level validations
        for (var i = 0; valuesArray != null && i < arrayLength; i++) {
            var valuestable = valuesArray[i];
            if (kony.sync.attributeValidation(valuestable, "vcincab", errorcallback, true) === false) {
                return;
            }
            newValuesArray[i] = valuestable;
        }
        valuesArray = newValuesArray;
        var connection = kony.sync.getConnectionOnly(dbname, dbname);
        kony.sync.startTransaction(connection, checkIntegrity, transactionSuccessCallback, transactionErrorCallback);
        var isError = false;
    } else {
        //copying by value
        var newValuesArray = [];
        arrayLength = valuesArray.length;
        for (var i = 0; valuesArray != null && i < arrayLength; i++) {
            newValuesArray[i] = kony.sync.CreateCopy(valuesArray[i]);
        }
        valuesArray = newValuesArray;
        kony.sync.massInsert(dbname, tbname, valuesArray, successcallback, errorcallback, markForUpload);
    }

    function transactionErrorCallback() {
        if (isError == true) {
            //Statement error has occurred
            kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
        } else {
            //Transaction error has occurred
            kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodeTransaction, kony.sync.getErrorMessage(kony.sync.errorCodeTransaction), null));
        }
    }

    function transactionSuccessCallback() {
        sync.log.trace("Entering  VCScope.vcincab.createAll->transactionSuccessCallback");
        if (!isError) {
            kony.sync.massInsert(dbname, tbname, valuesArray, successcallback, errorcallback, markForUpload);
        } else {
            if (isReferentialIntegrityFailure) {
                kony.sync.verifyAndCallClosure(errorcallback, errObject);
            }
        }
    }
    //foreign key constraints validations
    function checkIntegrity(tx) {
        sync.log.trace("Entering  VCScope.vcincab.createAll->checkIntegrity");
        arrayLength = valuesArray.length;
        for (var i = 0; valuesArray != null && i < arrayLength; i++) {
            var relationshipMap = {};
            relationshipMap = VCScope.vcincab.getRelationshipMap(relationshipMap, valuesArray[i]);
            errObject = kony.sync.checkIntegrityinTransaction(tx, relationshipMap, null);
            if (errObject === false) {
                isError = true;
                return;
            }
            if (errObject !== true) {
                isError = true;
                isReferentialIntegrityFailure = true;
                return;
            }
        }
    }
};
/************************************************************************************
 * Updates vcincab using primary key in the local Database. The update will be
 * merged with the enterprise datasource in the next Sync.
 *************************************************************************************/
VCScope.vcincab.prototype.updateByPK = function(successcallback, errorcallback) {
    sync.log.trace("Entering  VCScope.vcincab.prototype.updateByPK function");
    var pks = this.getPKTable();
    var valuestable = this.getValuesTable(false);
    VCScope.vcincab.updateByPK(pks, valuestable, successcallback, errorcallback, this.markForUpload);
};
VCScope.vcincab.updateByPK = function(pks, valuestable, successcallback, errorcallback, markForUpload) {
    sync.log.trace("Entering  VCScope.vcincab.updateByPK-> main function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    if (!kony.sync.validateInput(arguments, "VCScope.vcincab.updateByPK", "updateByPk", errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = VCScope.vcincab.getTableName();
    markForUpload = kony.sync.getUploadStatus(markForUpload);
    var wcs = [];
    if (VCScope.vcincab.pkCheck(pks, wcs, errorcallback, "updating") === false) {
        return;
    }
    if (kony.sync.attributeValidation(valuestable, "vcincab", errorcallback, false) === false) {
        return;
    }
    var relationshipMap = {};
    relationshipMap = VCScope.vcincab.getRelationshipMap(relationshipMap, valuestable);
    kony.sync.updateByPK(tbname, dbname, relationshipMap, pks, valuestable, successcallback, errorcallback, markForUpload, wcs);
};
/************************************************************************************
 * Updates vcincab(s) using where clause in the local Database. The update(s)
 * will be merged with the enterprise datasource in the next Sync.
 *************************************************************************************/
VCScope.vcincab.update = function(wcs, valuestable, successcallback, errorcallback, markForUpload) {
    sync.log.trace("Entering VCScope.vcincab.update function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    if (!kony.sync.validateInput(arguments, "VCScope.vcincab.update", "update", errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = VCScope.vcincab.getTableName();
    markForUpload = kony.sync.getUploadStatus(markForUpload);
    wcs = kony.sync.validateWhereClause(wcs);
    if (kony.sync.attributeValidation(valuestable, "vcincab", errorcallback, false) === false) {
        return;
    }

    function executeSuccess() {
        sync.log.trace("Entering  VCScope.vcincab.update-> success callback of Integrity Check");
        kony.sync.single_update_execute(dbname, tbname, valuestable, wcs, successcallback, errorcallback, true, markForUpload, VCScope.vcincab.getPKTable());
    }
    if (kony.sync.enableORMValidations) {
        var relationshipMap = {};
        relationshipMap = VCScope.vcincab.getRelationshipMap(relationshipMap, valuestable);
        kony.sync.checkIntegrity(dbname, relationshipMap, executeSuccess, errorcallback);
    } else {
        kony.sync.single_update_execute(dbname, tbname, valuestable, wcs, successcallback, errorcallback, true, markForUpload, VCScope.vcincab.getPKTable());
    }
};
/************************************************************************************
 * Updates vcincab(s) satisfying one or more where clauses in the local Database. 
 * The update(s) will be merged with the enterprise datasource in the next Sync.
 * Based upon kony.sync.enableORMValidations flag, validations will be enabled/disabled.
 * e.g.	var inputArray = [];
 *		inputArray[0] = {};
 *		inputArray[0].changeSet = {};
 *		inputArray[0].changeSet.regno = "regno_updated0";
 *		inputArray[0].changeSet.email = "email_updated0";
 *		inputArray[0].changeSet.fuel = "fuel_updated0";
 *		inputArray[0].changeSet.Mirrors = "Mirrors_updated0";
 *		inputArray[0].whereClause = "where cabid = 0";
 *		inputArray[1] = {};
 *		inputArray[1].changeSet = {};
 *		inputArray[1].changeSet.regno = "regno_updated1";
 *		inputArray[1].changeSet.email = "email_updated1";
 *		inputArray[1].changeSet.fuel = "fuel_updated1";
 *		inputArray[1].changeSet.Mirrors = "Mirrors_updated1";
 *		inputArray[1].whereClause = "where cabid = 1";
 *		inputArray[2] = {};
 *		inputArray[2].changeSet = {};
 *		inputArray[2].changeSet.regno = "regno_updated2";
 *		inputArray[2].changeSet.email = "email_updated2";
 *		inputArray[2].changeSet.fuel = "fuel_updated2";
 *		inputArray[2].changeSet.Mirrors = "Mirrors_updated2";
 *		inputArray[2].whereClause = "where cabid = 2";
 *		VCScope.vcincab.updateAll(inputArray,successcallback,errorcallback);
 *************************************************************************************/
VCScope.vcincab.updateAll = function(inputArray, successcallback, errorcallback, markForUpload) {
        sync.log.trace("Entering VCScope.vcincab.updateAll function");
        if (!kony.sync.isSyncInitialized(errorcallback)) {
            return;
        }
        if (!kony.sync.validateInput(arguments, "VCScope.vcincab.updateAll", "updateAll", errorcallback)) {
            return;
        }
        var dbname = "100004898461ca65a";
        var tbname = "vcincab";
        var isError = false;
        var errObject = null;
        if (markForUpload == false || markForUpload == "false") {
            markForUpload = "false"
        } else {
            markForUpload = "true"
        }
        if ((kony.sync.enableORMValidations)) {
            var newInputArray = [];
            for (var i = 0;
                ((inputArray) != null) && i < inputArray.length; i++) {
                var v = inputArray[i];
                var valuestable = v.changeSet;
                var isEmpty = true;
                for (var key in valuestable) {
                    isEmpty = false;
                    break;
                }
                if (isEmpty) {
                    errorcallback(kony.sync.getErrorTable(kony.sync.errorCodeNullValue, kony.sync.getErrorMessage(kony.sync.errorCodeNullValue)));
                    return;
                }
                var wcs = v.whereClause;
                var twcs = wcs;
                if (kony.sync.attributeValidation(valuestable, "vcincab", errorcallback, false) === false) {
                    return;
                }
                newInputArray[i] = [];
                newInputArray[i].changeSet = valuestable;
                newInputArray[i].whereClause = wcs;
            }
            inputArray = newInputArray;
            var connection = kony.sync.getConnectionOnly(dbname, dbname);
            kony.sync.startTransaction(connection, checkIntegrity, transactionSuccessCallback, transactionErrorCallback);
        } else {
            //copying by value
            var newInputArray = [];
            for (var i = 0;
                ((inputArray) != null) && i < inputArray.length; i++) {
                var v = inputArray[i];
                newInputArray[i] = kony.sync.CreateCopy(v);
            }
            inputArray = newInputArray;
            kony.sync.massUpdate(dbname, tbname, inputArray, successcallback, errorcallback, markForUpload, VCScope.vcincab.getPKTable());
        }

        function transactionSuccessCallback() {
            sync.log.trace("Entering  VCScope.vcincab.updateAll->transactionSuccessCallback");
            if (!isError) {
                kony.sync.massUpdate(dbname, tbname, inputArray, successcallback, transactionErrorCallback, markForUpload, VCScope.vcincab.getPKTable());
            }
        }

        function transactionErrorCallback() {
            if (errObject === false) {
                //Sql statement error has occcurred
                kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
                kony.sync.errorObject = null;
            } else if (errObject !== null) {
                // Referential integrity error has occurred
                kony.sync.verifyAndCallClosure(errorcallback, errObject);
            } else {
                //Transaction error has occurred
                kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodeTransaction, kony.sync.getErrorMessage(kony.sync.errorCodeTransaction), null));
            }
        }
        //foreign key constraints validations
        function checkIntegrity(tx) {
            sync.log.trace("Entering  VCScope.vcincab.updateAll->checkIntegrity");
            for (var i = 0;
                ((inputArray) != null) && i < inputArray.length; i++) {
                var relationshipMap = {};
                relationshipMap = VCScope.vcincab.getRelationshipMap(relationshipMap, inputArray[i].changeSet);
                sync.log.debug("Relationship Map for Integrity check created:", relationshipMap);
                errObject = kony.sync.checkIntegrityinTransaction(tx, relationshipMap, null);
                if (errObject === false) {
                    isError = true;
                    return;
                }
                if (errObject !== true) {
                    isError = true;
                    kony.sync.rollbackTransaction(tx);
                    return;
                }
            }
        }
    }
    /************************************************************************************
     * Deletes vcincab using primary key from the local Database. The record will be
     * deleted from the enterprise datasource in the next Sync.
     *************************************************************************************/
VCScope.vcincab.prototype.deleteByPK = function(successcallback, errorcallback) {
    sync.log.trace("Entering VCScope.vcincab.prototype.deleteByPK function");
    var pks = this.getPKTable();
    VCScope.vcincab.deleteByPK(pks, successcallback, errorcallback, this.markForUpload);
};
VCScope.vcincab.deleteByPK = function(pks, successcallback, errorcallback, markForUpload) {
    sync.log.trace("Entering VCScope.vcincab.deleteByPK-> main function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    if (!kony.sync.validateInput(arguments, "VCScope.vcincab.deleteByPK", "deleteByPK", errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = VCScope.vcincab.getTableName();
    markForUpload = kony.sync.getUploadStatus(markForUpload);
    var wcs = [];
    var isError = false;
    var pkNotFound = false;
    var twcs = [];
    var deletedRows;
    var record = "";
    if (VCScope.vcincab.pkCheck(pks, wcs, errorcallback, "deleting") === false) {
        return;
    }
    twcs = kony.sync.CreateCopy(wcs);

    function vcincabTransactionCallback(tx) {
        sync.log.trace("Entering VCScope.vcincab.deleteByPK->vcincab_PKPresent successcallback");
        record = kony.sync.getOriginalRow(tx, tbname, wcs, errorcallback);
        if (record === false) {
            isError = true;
            return;
        }
        if (null !== record) {} else {
            pkNotFound = true;
        }
        var deletedRows = kony.sync.remove(tx, tbname, wcs, false, markForUpload, null);
        if (deletedRows === false) {
            isError = true;
        }
    }

    function vcincabErrorCallback() {
        sync.log.error("Entering VCScope.vcincab.deleteByPK->relationship failure callback");
        if (isError === false) {
            kony.sync.verifyAndCallClosure(errorcallback);
        }
        if (kony.sync.errorObject != null) {
            kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
            kony.sync.errorObject = null;
        }
    }

    function vcincabSuccessCallback() {
        sync.log.trace("Entering VCScope.vcincab.deleteByPK->relationship success callback");
        if (pkNotFound === true) {
            kony.sync.verifyAndCallClosure(pkNotFoundErrCallback);
            return;
        }
        if (!isError) {
            kony.sync.verifyAndCallClosure(successcallback, {
                rowsdeleted: 1
            });
        }
    }

    function pkNotFoundErrCallback() {
        sync.log.error("Entering VCScope.vcincab.deleteByPK->PK not found callback");
        kony.sync.pkNotFoundErrCallback(errorcallback, tbname);
    }
    var dbconnection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
    if (dbconnection != null) {
        kony.sync.startTransaction(dbconnection, vcincabTransactionCallback, vcincabSuccessCallback, vcincabErrorCallback, "Single Execute");
    }
};
/************************************************************************************
 * Deletes vcincab(s) using where clause from the local Database. The record(s)
 * will be deleted from the enterprise datasource in the next Sync.
 * e.g. VCScope.vcincab.remove("where regno like 'A%'", successcallback,errorcallback, true);
 *************************************************************************************/
VCScope.vcincab.remove = function(wcs, successcallback, errorcallback, markForUpload) {
    sync.log.trace("Entering VCScope.vcincab.remove->main function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    if (!kony.sync.validateInput(arguments, "VCScope.vcincab.remove", "remove", errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = VCScope.vcincab.getTableName();
    markForUpload = kony.sync.getUploadStatus(markForUpload);
    wcs = kony.sync.validateWhereClause(wcs);
    var twcs = wcs;
    var isError = false;
    var rowsDeleted;

    function vcincab_removeTransactioncallback(tx) {
        wcs = " " + wcs;
        rowsDeleted = kony.sync.deleteBatch(tx, tbname, wcs, false, markForUpload, errorcallback)
        if (rowsDeleted === false) {
            isError = true;
        }
    }

    function vcincab_removeSuccess() {
        sync.log.trace("Entering VCScope.vcincab.remove->vcincab_removeSuccess function");
        if (!isError) {
            kony.sync.verifyAndCallClosure(successcallback, rowsDeleted);
        }
    }

    function errorcallbackWrapper() {
        sync.log.trace("Entering VCScope.vcincab.remove->error callback function");
        if (!isError) {
            kony.sync.showTransactionError(errorcallback);
        }
        if (kony.sync.errorObject != null) {
            kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
            kony.sync.errorObject = null;
        }
    }

    function deleteEntity() {
        sync.log.trace("Entering VCScope.vcincab.remove->delete Entity function");
        var connection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
        if (connection != null) {
            kony.sync.startTransaction(connection, vcincab_removeTransactioncallback, vcincab_removeSuccess, errorcallbackWrapper);
        }
    }
    deleteEntity();
};
/************************************************************************************
 * Deletes vcincab using primary key from the local Database. This will
 * not have any effect in enterprise datasource in subsequent sync cycles
 *************************************************************************************/
VCScope.vcincab.prototype.removeDeviceInstanceByPK = function(successcallback, errorcallback) {
    sync.log.trace("Entering VCScope.vcincab.prototype.removeDeviceInstanceByPK function");
    var pks = this.getPKTable();
    VCScope.vcincab.removeDeviceInstanceByPK(pks, successcallback, errorcallback);
};
VCScope.vcincab.removeDeviceInstanceByPK = function(pks, successcallback, errorcallback) {
    sync.log.trace("Entering VCScope.vcincab.removeDeviceInstanceByPK function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    if (!kony.sync.validateInput(arguments, "VCScope.vcincab.removeDeviceInstanceByPK", "removeDeviceInstanceByPK", errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = VCScope.vcincab.getTableName();
    var wcs = [];
    var isError = false;
    var pkNotFound = false;
    var deletedRows;
    if (VCScope.vcincab.pkCheck(pks, wcs, errorcallback, "deleting") === false) {
        return;
    }

    function vcincabTransactionCallback(tx) {
        sync.log.trace("Entering VCScope.vcincab.removeDeviceInstanceByPK -> vcincabTransactionCallback");
        var record = kony.sync.getOriginalRow(tx, tbname, wcs, errorcallback);
        if (null !== record && false != record) {
            deletedRows = kony.sync.remove(tx, tbname, wcs, true, null, null);
            if (deletedRows === false) {
                isError = true;
            }
        } else {
            pkNotFound = true;
        }
    }

    function vcincabErrorCallback() {
        sync.log.error("Entering VCScope.vcincab.removeDeviceInstanceByPK -> vcincabErrorCallback");
        if (isError === false) {
            kony.sync.verifyAndCallClosure(errorcallback);
        }
        if (kony.sync.errorObject != null) {
            kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
            kony.sync.errorObject = null;
        }
    }

    function vcincabSuccessCallback() {
        sync.log.trace("Entering VCScope.vcincab.removeDeviceInstanceByPK -> vcincabSuccessCallback");
        if (pkNotFound === true) {
            kony.sync.verifyAndCallClosure(pkNotFoundErrCallback);
            return;
        }
        if (!isError) {
            kony.sync.verifyAndCallClosure(successcallback, {
                rowsdeleted: 1
            });
        }
    }

    function pkNotFoundErrCallback() {
        sync.log.error("Entering VCScope.vcincab.removeDeviceInstanceByPK -> PK not found callback");
        kony.sync.pkNotFoundErrCallback(errorcallback, tbname);
    }
    var dbconnection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
    if (dbconnection != null) {
        kony.sync.startTransaction(dbconnection, vcincabTransactionCallback, vcincabSuccessCallback, vcincabErrorCallback, "Single Execute");
    }
};
/************************************************************************************
 * Deletes vcincab(s) using where clause from the local Database. This will
 * not have any effect in enterprise datasource in subsequent sync cycles
 *************************************************************************************/
VCScope.vcincab.removeDeviceInstance = function(wcs, successcallback, errorcallback) {
    sync.log.trace("Entering VCScope.vcincab.removeDeviceInstance->main function");
    var dbname = kony.sync.getDBName();
    var tbname = VCScope.vcincab.getTableName();
    wcs = kony.sync.validateWhereClause(wcs);
    var twcs = wcs;
    var isError = false;
    var rowsDeleted;

    function vcincab_removeTransactioncallback(tx) {
        wcs = " " + wcs;
        rowsDeleted = kony.sync.deleteBatch(tx, tbname, wcs, true, null, errorcallback)
        if (rowsDeleted === false) {
            isError = true;
        }
    }

    function vcincab_removeSuccess() {
        sync.log.trace("Entering VCScope.vcincab.remove->vcincab_removeSuccess function");
        if (!isError) {
            kony.sync.verifyAndCallClosure(successcallback, rowsDeleted);
        }
    }

    function errorcallbackWrapper() {
        sync.log.trace("Entering VCScope.vcincab.remove->error callback function");
        if (!isError) {
            kony.sync.showTransactionError(errorcallback);
        }
        if (kony.sync.errorObject != null) {
            kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
            kony.sync.errorObject = null;
        }
    }

    function deleteEntity() {
        sync.log.trace("Entering VCScope.vcincab.remove->delete Entity function");
        var connection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
        if (connection != null) {
            kony.sync.startTransaction(connection, vcincab_removeTransactioncallback, vcincab_removeSuccess, errorcallbackWrapper);
        }
    }
    deleteEntity();
};
/************************************************************************************
 * Retrieves vcincab using primary key from the local Database. 
 *************************************************************************************/
VCScope.vcincab.prototype.getAllDetailsByPK = function(successcallback, errorcallback) {
    sync.log.trace("Entering VCScope.vcincab.prototype.getAllDetailsByPK function");
    var pks = this.getPKTable();
    VCScope.vcincab.getAllDetailsByPK(pks, successcallback, errorcallback);
};
VCScope.vcincab.getAllDetailsByPK = function(pks, successcallback, errorcallback) {
    sync.log.trace("Entering VCScope.vcincab.getAllDetailsByPK-> main function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    if (!kony.sync.validateInput(arguments, "VCScope.vcincab.getAllDetailsByPK", "getAllDetailsByPK", errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = VCScope.vcincab.getTableName();
    var wcs = [];
    if (VCScope.vcincab.pkCheck(pks, wcs, errorcallback, "searching") === false) {
        return;
    }
    twcs = kony.sync.CreateCopy(wcs);
    var query = kony.sync.qb_createQuery();
    kony.sync.qb_select(query, null);
    kony.sync.qb_from(query, tbname);
    kony.sync.qb_where(query, wcs);
    var query_compile = kony.sync.qb_compile(query);
    var sql = query_compile[0];
    var params = query_compile[1];

    function mySuccCallback(res) {
        sync.log.trace("Entering VCScope.vcincab.getAllDetailsByPK-> success callback function");
        successcallback(VCScope.vcincab.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
    }
    kony.sync.single_select_execute(dbname, sql, params, mySuccCallback, errorcallback);
};
/************************************************************************************
 * Retrieves vcincab(s) using where clause from the local Database. 
 * e.g. VCScope.vcincab.find("where regno like 'A%'", successcallback,errorcallback);
 *************************************************************************************/
VCScope.vcincab.find = function(wcs, successcallback, errorcallback) {
    sync.log.trace("Entering VCScope.vcincab.find function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    if (!kony.sync.validateInput(arguments, "VCScope.vcincab.find", "find", errorcallback)) {
        return;
    }
    //wcs will be a string formed by the user.
    var dbname = kony.sync.getDBName();
    var tbname = VCScope.vcincab.getTableName();
    wcs = kony.sync.validateWhereClause(wcs);
    var sql = "select * from \"" + tbname + "\" " + wcs;

    function mySuccCallback(res) {
        kony.sync.verifyAndCallClosure(successcallback, VCScope.vcincab.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
    }
    kony.sync.single_select_execute(dbname, sql, null, mySuccCallback, errorcallback);
};
/************************************************************************************
 * Marks instance of vcincab with given primary key for upload. This will 
 * enable deferred records to merge with the enterprise datasource in the next Sync.
 *************************************************************************************/
VCScope.vcincab.prototype.markForUploadbyPK = function(successcallback, errorcallback) {
    sync.log.trace("Entering VCScope.vcincab.prototype.markForUploadbyPK function");
    var pks = this.getPKTable();
    VCScope.vcincab.markForUploadbyPK(pks, successcallback, errorcallback);
};
VCScope.vcincab.markForUploadbyPK = function(pks, successcallback, errorcallback) {
    sync.log.trace("Entering VCScope.vcincab.markForUploadbyPK function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    if (!kony.sync.validateInput(arguments, "VCScope.vcincab.markForUploadbyPK", "markForUploadbyPK", errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = VCScope.vcincab.getTableName();
    var isError = false;
    var recordsFound = false;
    var recordsMarkedForUpload = 0;
    var wcs = [];
    if (VCScope.vcincab.pkCheck(pks, wcs, errorcallback, "marking for upload by PK") === false) {
        return;
    }

    function markRecordForUpload(tx, record) {
        var versionMapMain = [];
        versionMapMain[kony.sync.mainTableChangeTypeColumn] = kony.sync.getChangeTypeForUploadTrue(record[kony.sync.historyTableChangeTypeColumn]);
        var scopename = kony.sync.scopes.syncTableScopeDic[tbname];
        var versionNo = kony.sync.getseqnumber(tx, scopename);
        if (versionNo === false) {
            return false;
        }
        versionMapMain[kony.sync.historyTableSyncVersionColumn] = versionNo.versionnumber;
        var query = kony.sync.qb_createQuery();
        kony.sync.qb_update(query, tbname);
        kony.sync.qb_set(query, versionMapMain);
        kony.sync.qb_where(query, wcs);
        var query_compile = kony.sync.qb_compile(query);
        var sql = query_compile[0];
        var params = query_compile[1];
        return kony.sync.executeSql(tx, sql, params);
    }

    function markRecordForUploadHistory(tx, record) {
        var versionMap = [];
        versionMap[kony.sync.historyTableChangeTypeColumn] = kony.sync.getChangeTypeForUploadTrue(record[kony.sync.historyTableChangeTypeColumn]);
        var scopename = kony.sync.scopes.syncTableScopeDic[tbname];
        var versionNo = kony.sync.getseqnumber(tx, scopename);
        if (versionNo === false) {
            return false;
        }
        var twcs = [];
        twcs = wcs;
        kony.table.insert(twcs, {
            key: kony.sync.historyTableChangeTypeColumn,
            value: record[kony.sync.historyTableChangeTypeColumn],
            optype: "EQ",
            comptype: "AND"
        });
        versionMap[kony.sync.historyTableSyncVersionColumn] = versionNo.versionnumber;
        var query = kony.sync.qb_createQuery();
        kony.sync.qb_update(query, tbname + kony.sync.historyTableName);
        kony.sync.qb_set(query, versionMap);
        kony.sync.qb_where(query, twcs);
        kony.table.remove(twcs);
        var query_compile = kony.sync.qb_compile(query);
        var sql = query_compile[0];
        var params = query_compile[1];
        return kony.sync.executeSql(tx, sql, params);
    }

    function single_transaction_callback(tx) {
        var query = kony.sync.qb_createQuery();
        kony.sync.qb_select(query, [kony.sync.historyTableChangeTypeColumn]);
        kony.sync.qb_from(query, tbname);
        kony.sync.qb_where(query, wcs);
        var query_compile = kony.sync.qb_compile(query);
        var sql = query_compile[0];
        var params = query_compile[1];
        var resultSet = kony.sync.executeSql(tx, sql, params);
        if (resultSet === false) {
            isError = true;
            return;
        }
        var num_records = resultSet.rows.length;
        if (num_records > 0) {
            recordsFound = true;
            var record = kony.db.sqlResultsetRowItem(tx, resultSet, 0);
            var changeType = record[kony.sync.mainTableChangeTypeColumn];
            if (!kony.sync.isNullOrUndefined(changeType) && kony.string.startsWith("" + changeType, "9")) {
                recordsMarkedForUpload = 1;
                if (markRecordForUpload(tx, record) === false) {
                    isError = true;
                    return;
                }
            }
        }
        var query1 = kony.sync.qb_createQuery();
        kony.sync.qb_select(query1, [kony.sync.historyTableChangeTypeColumn]);
        kony.sync.qb_from(query1, tbname + kony.sync.historyTableName);
        kony.sync.qb_where(query1, wcs);
        var query1_compile = kony.sync.qb_compile(query1);
        var sql1 = query1_compile[0];
        var params1 = query1_compile[1];
        var resultSet1 = kony.sync.executeSql(tx, sql1, params1);
        if (resultSet1 !== false) {
            var num_records = resultSet1.rows.length;
            for (var i = 0; i <= num_records - 1; i++) {
                var record = kony.db.sqlResultsetRowItem(tx, resultSet1, i);
                if (markRecordForUploadHistory(tx, record) === false) {
                    isError = true;
                    return;
                }
                recordsFound = true;
            }
        } else {
            isError = true;
        }
    }

    function single_transaction_success_callback() {
        if (recordsFound === true) {
            kony.sync.verifyAndCallClosure(successcallback, {
                count: recordsMarkedForUpload
            });
        } else {
            kony.sync.pkNotFoundErrCallback(errorcallback, tbname);
        }
    }

    function single_transaction_error_callback(res) {
        if (!isError) {
            kony.sync.showTransactionError(errorcallback);
        } else {
            kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
            kony.sync.errorObject = null;
        }
    }
    var connection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
    if (connection != null) {
        kony.sync.startTransaction(connection, single_transaction_callback, single_transaction_success_callback, single_transaction_error_callback);
    }
};
/************************************************************************************
 * Marks instance(s) of vcincab matching given where clause for upload. This will 
 * enable deferred records to merge with the enterprise datasource in the next Sync.
 *************************************************************************************/
VCScope.vcincab.markForUpload = function(wcs, successcallback, errorcallback) {
    sync.log.trace("Entering VCScope.vcincab.markForUpload->main function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    if (!kony.sync.validateInput(arguments, "VCScope.vcincab.markForUpload", "markForUpload", errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = VCScope.vcincab.getTableName();
    var isError = false;
    var num_records_main = 0;
    wcs = kony.sync.validateWhereClause(wcs);
    if (!kony.sync.isNull(wcs) && !kony.sync.isEmptyString(wcs)) {
        wcs = wcs + " and " + kony.sync.historyTableChangeTypeColumn + " like '9%'";
    } else {
        wcs = "where " + kony.sync.historyTableChangeTypeColumn + " like '9%'";
    }

    function markRecordForUpload(tx, record) {
        var versionMapMain = [];
        versionMapMain[kony.sync.mainTableChangeTypeColumn] = kony.sync.getChangeTypeForUploadTrue(record[kony.sync.historyTableChangeTypeColumn]);
        var scopename = kony.sync.scopes.syncTableScopeDic[tbname];
        var versionNo = kony.sync.getseqnumber(tx, scopename);
        if (versionNo === false) {
            return false;
        }
        versionMapMain[kony.sync.historyTableSyncVersionColumn] = versionNo.versionnumber;
        var query = kony.sync.qb_createQuery();
        kony.sync.qb_update(query, tbname);
        kony.sync.qb_set(query, versionMapMain);
        var query_compile = kony.sync.qb_compile(query);
        var sql = query_compile[0] + " " + wcs;
        var params = query_compile[1];
        if (kony.sync.executeSql(tx, sql, params) === false) {
            return false;
        }
    }

    function markRecordForUploadHistory(tx, record) {
        var versionMap = [];
        versionMap[kony.sync.historyTableChangeTypeColumn] = kony.sync.getChangeTypeForUploadTrue(record[kony.sync.historyTableChangeTypeColumn]);
        var scopename = kony.sync.scopes.syncTableScopeDic[tbname];
        var versionNo = kony.sync.getseqnumber(tx, scopename);
        if (versionNo === false) {
            return false;
        }
        var twcs = "";
        twcs = wcs;
        twcs = twcs + " AND " + kony.sync.historyTableChangeTypeColumn + " = " + record[kony.sync.historyTableChangeTypeColumn];
        versionMap[kony.sync.historyTableSyncVersionColumn] = versionNo.versionnumber;
        var query = kony.sync.qb_createQuery();
        kony.sync.qb_update(query, tbname + kony.sync.historyTableName);
        kony.sync.qb_set(query, versionMap);
        var query_compile = kony.sync.qb_compile(query);
        var sql = query_compile[0] + " " + twcs;
        var params = query_compile[1];
        if (kony.sync.executeSql(tx, sql, params) === false) {
            return false;
        }
    }

    function single_transaction_callback(tx) {
        sync.log.trace("Entering VCScope.vcincab.markForUpload->single_transaction_callback");
        //updating main table
        var sql = "select " + kony.sync.historyTableChangeTypeColumn + " from \"" + tbname + "\" " + wcs;
        var resultSet = kony.sync.executeSql(tx, sql, null);
        if (resultSet === false) {
            isError = true;
            return;
        }
        num_records_main = resultSet.rows.length;
        for (var i = 0; i < num_records_main; i++) {
            var record = kony.db.sqlResultsetRowItem(tx, resultSet, i);
            if (markRecordForUpload(tx, record) === false) {
                isError = true;
                return;
            }
        }
        //updating history table
        var sql = "select " + kony.sync.historyTableChangeTypeColumn + " from " + tbname + kony.sync.historyTableName + " " + wcs;
        var resultSet = kony.sync.executeSql(tx, sql, null);
        if (resultSet === false) {
            isError = true;
            return;
        }
        var num_records = resultSet.rows.length;
        for (var i = 0; i <= num_records - 1; i++) {
            var record = kony.db.sqlResultsetRowItem(tx, resultSet, i);
            if (markRecordForUploadHistory(tx, record) === false) {
                isError = true;
                return;
            }
        }
    }

    function single_transaction_success_callback() {
        sync.log.trace("Entering VCScope.vcincab.markForUpload->single_transaction_success_callback");
        kony.sync.verifyAndCallClosure(successcallback, {
            count: num_records_main
        });
    }

    function single_transaction_error_callback() {
        sync.log.error("Entering VCScope.vcincab.markForUpload->single_transaction_error_callback");
        if (!isError) {
            kony.sync.showTransactionError(errorcallback);
        } else {
            kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
            kony.sync.errorObject = null;
        }
    }
    var connection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
    if (connection != null) {
        kony.sync.startTransaction(connection, single_transaction_callback, single_transaction_success_callback, single_transaction_error_callback);
    }
};
/************************************************************************************
 * Retrieves instance(s) of vcincab pending for upload. Records are marked for
 * pending upload if they have been updated or created locally and the changes have
 * not been merged with enterprise datasource.
 *************************************************************************************/
VCScope.vcincab.getPendingUpload = function(wcs, successcallback, errorcallback) {
    sync.log.trace("Entering VCScope.vcincab.getPendingUpload->main function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = VCScope.vcincab.getTableName();
    var currentversion = kony.sync.getCurrentVersionNumber(tbname);
    var sql;
    if (typeof(wcs) === "string" && wcs != null) {
        wcs = kony.sync.validateWhereClause(wcs);
        sql = "select * from \"" + tbname + "\" " + wcs + " and " + kony.sync.mainTableChangeTypeColumn + " is not null AND " + kony.sync.mainTableChangeTypeColumn + " <> -1 AND " + kony.sync.mainTableSyncVersionColumn + " = " + currentversion + " AND " + kony.sync.mainTableChangeTypeColumn + " NOT LIKE '9%'";
    } else {
        errorcallback = successcallback;
        successcallback = wcs;
        sql = "select * from \"" + tbname + "\" WHERE " + kony.sync.mainTableChangeTypeColumn + " is not null AND " + kony.sync.mainTableChangeTypeColumn + " <> -1 AND " + kony.sync.mainTableSyncVersionColumn + " = " + currentversion + " AND " + kony.sync.mainTableChangeTypeColumn + " NOT LIKE '9%'";
    }
    kony.sync.single_select_execute(dbname, sql, null, mySuccesscallback, errorcallback);

    function mySuccesscallback(res) {
        sync.log.trace("Entering VCScope.vcincab.getPendingUpload->successcallback function");
        kony.sync.verifyAndCallClosure(successcallback, VCScope.vcincab.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
    }
};
/************************************************************************************
 * Retrieves instance(s) of vcincab pending for acknowledgement. This is relevant
 * when the SyncObject is part of the SyncScope whose SyncStrategy is PersistentSync.
 * In persistent Sync the  records in the local database are put into a pending 
 * acknowledgement state after an upload.
 *************************************************************************************/
VCScope.vcincab.getPendingAcknowledgement = function(successcallback, errorcallback) {
    sync.log.trace("Entering VCScope.vcincab.getPendingAcknowledgement->main function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = VCScope.vcincab.getTableName();
    var currentversion = kony.sync.getCurrentVersionNumber(tbname);
    var mysql = "select * from \"" + tbname + "\" WHERE " + kony.sync.mainTableChangeTypeColumn + " is not null AND " + kony.sync.mainTableChangeTypeColumn + " <> -1 AND " + kony.sync.mainTableSyncVersionColumn + " <> " + currentversion + " AND " + kony.sync.mainTableChangeTypeColumn + " NOT LIKE '9%'";
    kony.sync.single_select_execute(dbname, mysql, null, mySuccesscallback, errorcallback);

    function mySuccesscallback(res) {
        sync.log.trace("Entering VCScope.vcincab.getPendingAcknowledgement success callback function");
        kony.sync.verifyAndCallClosure(successcallback, VCScope.vcincab.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
    }
};
/************************************************************************************
 * Retrieves instance(s) of vcincab deferred for upload.
 *************************************************************************************/
VCScope.vcincab.getDeferredUpload = function(wcs, successcallback, errorcallback) {
    sync.log.trace("Entering VCScope.vcincab.getDeferredUpload->main function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = VCScope.vcincab.getTableName();
    var sql;
    if (typeof(wcs) === "string" && wcs != null) {
        wcs = kony.sync.validateWhereClause(wcs);
        sql = "select * from \"" + tbname + "\" " + wcs + " and " + kony.sync.mainTableChangeTypeColumn + " is not null AND " + kony.sync.mainTableChangeTypeColumn + " <> -1 AND " + kony.sync.mainTableChangeTypeColumn + " LIKE '9%'";
    } else {
        errorcallback = successcallback;
        successcallback = wcs;
        sql = "select * from \"" + tbname + "\" WHERE " + kony.sync.mainTableChangeTypeColumn + " is not null AND " + kony.sync.mainTableChangeTypeColumn + " <> -1 AND " + kony.sync.mainTableChangeTypeColumn + " LIKE '9%'";
    }
    kony.sync.single_select_execute(dbname, sql, null, mySuccesscallback, errorcallback);

    function mySuccesscallback(res) {
        sync.log.trace("Entering VCScope.vcincab.getDeferredUpload->success callback function");
        kony.sync.verifyAndCallClosure(successcallback, VCScope.vcincab.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
    }
};
/************************************************************************************
 * Rollbacks all changes to vcincab in local database to last synced state
 *************************************************************************************/
VCScope.vcincab.rollbackPendingLocalChanges = function(successcallback, errorcallback) {
    sync.log.trace("Entering VCScope.vcincab.rollbackPendingLocalChanges->main function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = VCScope.vcincab.getTableName();
    kony.sync.konySyncRollBackPendingChanges(tbname, dbname, null, mySuccesscallback, errorcallback);

    function mySuccesscallback(res) {
        sync.log.trace("Entering VCScope.vcincab.rollbackPendingLocalChanges->main function");
        kony.sync.verifyAndCallClosure(successcallback, res);
    }
};
/************************************************************************************
 * Rollbacks changes to vcincab's record with given primary key in local 
 * database to last synced state
 *************************************************************************************/
VCScope.vcincab.prototype.rollbackPendingLocalChangesByPK = function(successcallback, errorcallback) {
    sync.log.trace("Entering VCScope.vcincab.prototype.rollbackPendingLocalChangesByPK function");
    var pks = this.getPKTable();
    VCScope.vcincab.rollbackPendingLocalChangesByPK(pks, successcallback, errorcallback);
};
VCScope.vcincab.rollbackPendingLocalChangesByPK = function(pks, successcallback, errorcallback) {
    sync.log.trace("Entering VCScope.vcincab.rollbackPendingLocalChangesByPK->main function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    if (!kony.sync.validateInput(arguments, "VCScope.vcincab.rollbackPendingLocalChangesByPK", "rollbackPendingLocalChangesByPK", errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = VCScope.vcincab.getTableName();
    var wcs = [];
    if (VCScope.vcincab.pkCheck(pks, wcs, errorcallback, "rollbacking") === false) {
        return;
    }
    kony.sync.konySyncRollBackPendingChanges(tbname, dbname, wcs, mySuccesscallback, pkNotFoundErrCallback);

    function mySuccesscallback(res) {
        sync.log.trace("Entering VCScope.vcincab.rollbackPendingLocalChangesByPK->success callback function");
        kony.sync.verifyAndCallClosure(successcallback, res);
    }

    function pkNotFoundErrCallback() {
        sync.log.error("Entering VCScope.vcincab.rollbackPendingLocalChangesByPK->PK not found callback");
        kony.sync.pkNotFoundErrCallback(errorcallback, tbname);
    }
};
/************************************************************************************
 * isRecordDeferredForUpload returns true or false depending on whether vcincab's record  
 * with given primary key got deferred in last sync
 *************************************************************************************/
VCScope.vcincab.prototype.isRecordDeferredForUpload = function(successcallback, errorcallback) {
    sync.log.trace("Entering  VCScope.vcincab.prototype.isRecordDeferredForUpload function");
    var pks = this.getPKTable();
    VCScope.vcincab.isRecordDeferredForUpload(pks, successcallback, errorcallback);
};
VCScope.vcincab.isRecordDeferredForUpload = function(pks, successcallback, errorcallback) {
    sync.log.trace("Entering VCScope.vcincab.isRecordDeferredForUpload->main function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    if (!kony.sync.validateInput(arguments, "VCScope.vcincab.isRecordDeferredForUpload", "isRecordDeferredForUpload", errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = VCScope.vcincab.getTableName();
    var wcs = [];
    var flag;
    if (VCScope.vcincab.pkCheck(pks, wcs, errorcallback, "selecting") === false) {
        return;
    }
    var twcs = [];
    twcs = kony.sync.CreateCopy(wcs);
    kony.table.insert(twcs, {
        key: kony.sync.mainTableChangeTypeColumn,
        value: "9%",
        optype: "LIKE",
        comptype: "AND"
    });
    var query = kony.sync.qb_createQuery();
    kony.sync.qb_select(query, null);
    kony.sync.qb_from(query, tbname);
    kony.sync.qb_where(query, twcs);
    var query_compile = kony.sync.qb_compile(query);
    var sql = query_compile[0];
    var params = query_compile[1];
    kony.sync.single_select_execute(dbname, sql, params, mySuccesscallback, errorcallback);

    function mySuccesscallback(res) {
        sync.log.trace("Entering VCScope.vcincab.isRecordDeferredForUpload->successcallback function");
        if (res.length === 1) {
            flag = true;
        } else {
            flag = false;
        }
        kony.sync.verifyAndCallClosure(successcallback, {
            deferred: flag
        });
    }
};
/************************************************************************************
 * isRecordPendingForUpload returns true or false depending on whether vcincab's record  
 * with given primary key is pending for upload
 *************************************************************************************/
VCScope.vcincab.prototype.isRecordPendingForUpload = function(successcallback, errorcallback) {
    sync.log.trace("Entering  VCScope.vcincab.prototype.isRecordPendingForUpload function");
    var pks = this.getPKTable();
    VCScope.vcincab.isRecordPendingForUpload(pks, successcallback, errorcallback);
};
VCScope.vcincab.isRecordPendingForUpload = function(pks, successcallback, errorcallback) {
    sync.log.trace("Entering VCScope.vcincab.isRecordPendingForUpload->main function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    if (!kony.sync.validateInput(arguments, "VCScope.vcincab.isRecordPendingForUpload", "isRecordPendingForUpload", errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = VCScope.vcincab.getTableName();
    var wcs = [];
    var flag;
    if (VCScope.vcincab.pkCheck(pks, wcs, errorcallback, "selecting") === false) {
        return;
    }
    var twcs = [];
    twcs = kony.sync.CreateCopy(wcs);
    kony.table.insert(twcs, {
        key: kony.sync.mainTableChangeTypeColumn,
        value: "9%",
        optype: "NOT LIKE",
        comptype: "AND"
    });
    var query = kony.sync.qb_createQuery();
    kony.sync.qb_select(query, null);
    kony.sync.qb_from(query, tbname);
    kony.sync.qb_where(query, twcs);
    var query_compile = kony.sync.qb_compile(query);
    var sql = query_compile[0];
    var params = query_compile[1];
    kony.sync.single_select_execute(dbname, sql, params, mySuccesscallback, errorcallback);

    function mySuccesscallback(res) {
        sync.log.trace("Entering VCScope.vcincab.isRecordPendingForUpload->successcallback function");
        if (res.length === 1) {
            flag = true;
        } else {
            flag = false;
        }
        kony.sync.verifyAndCallClosure(successcallback, {
            pending: flag
        });
    }
};
/************************************************************************************
 * Start of helper functions used internally, not to be used as ORMs
 *************************************************************************************/
//Deletes all the dependant tables in the relationship tables.Need to pass transaction handler as input
VCScope.vcincab.removeCascade = function(tx, wcs, errorcallback, markForUpload, isCascade, parentTable, isLocal) {
    sync.log.trace("Entering VCScope.vcincab.removeCascade function");
    var tbname = VCScope.vcincab.getTableName();
    markForUpload = kony.sync.getUploadStatus(markForUpload);

    function removeCascadeChildren() {}
    if (isCascade) {
        if (removeCascadeChildren() === false) {
            return false;
        }
        if (kony.sync.deleteBatch(tx, tbname, wcs, isLocal, markForUpload, null) === false) {
            return false;
        }
        return true;
    } else {
        var sql = "select * from \"" + tbname + "\" " + wcs;
        var resultSet = kony.sync.executeSql(tx, sql, null);
        if (resultSet === false) {
            return false;
        }
        var num_records = resultSet.rows.length;
        if (num_records === 0) {
            return true;
        } else {
            sync.log.error(kony.sync.getReferetialIntegrityDeleteErrMessg(tbname, tbname, tbname, parentTable));
            errorcallback(kony.sync.getErrorTable(kony.sync.errorCodeReferentialIntegrity, kony.sync.getReferetialIntegrityDeleteErrMessg(tbname, tbname, tbname, parentTable)));
            return false;
        }
    }
};
VCScope.vcincab.convertTableToObject = function(res) {
    sync.log.trace("Entering VCScope.vcincab.convertTableToObject function");
    objMap = [];
    if (res !== null) {
        for (var i in res) {
            var obj = new VCScope.vcincab();
            obj.cabid = res[i].cabid;
            obj.regno = res[i].regno;
            obj.email = res[i].email;
            obj.fuel = res[i].fuel;
            obj.Mirrors = res[i].Mirrors;
            obj.Horn = res[i].Horn;
            obj.Indicators = res[i].Indicators;
            obj.GroupLightsA = res[i].GroupLightsA;
            obj.GroupLightsB = res[i].GroupLightsB;
            obj.GroupLightsC = res[i].GroupLightsC;
            obj.Windscreen = res[i].Windscreen;
            obj.Tachograph = res[i].Tachograph;
            obj.Brakes = res[i].Brakes;
            obj.Wipers = res[i].Wipers;
            obj.Steering = res[i].Steering;
            obj.GeneralSafety = res[i].GeneralSafety;
            obj.isDeleted = res[i].isDeleted;
            obj.lastModifiedDate = res[i].lastModifiedDate;
            obj.markForUpload = (Math.floor(res[i].konysyncchangetype / 10) == 9) ? false : true;
            objMap[i] = obj;
        }
    }
    return objMap;
};
VCScope.vcincab.filterAttributes = function(valuestable, insert) {
    sync.log.trace("Entering VCScope.vcincab.filterAttributes function");
    var attributeTable = {};
    attributeTable.cabid = "cabid";
    attributeTable.regno = "regno";
    attributeTable.email = "email";
    attributeTable.fuel = "fuel";
    attributeTable.Mirrors = "Mirrors";
    attributeTable.Horn = "Horn";
    attributeTable.Indicators = "Indicators";
    attributeTable.GroupLightsA = "GroupLightsA";
    attributeTable.GroupLightsB = "GroupLightsB";
    attributeTable.GroupLightsC = "GroupLightsC";
    attributeTable.Windscreen = "Windscreen";
    attributeTable.Tachograph = "Tachograph";
    attributeTable.Brakes = "Brakes";
    attributeTable.Wipers = "Wipers";
    attributeTable.Steering = "Steering";
    attributeTable.GeneralSafety = "GeneralSafety";
    var PKTable = {};
    PKTable.cabid = {}
    PKTable.cabid.name = "cabid";
    PKTable.cabid.isAutoGen = true;
    var newvaluestable = {};
    for (var k in valuestable) {
        var v = valuestable[k];
        if (kony.sync.isNull(attributeTable[k])) {
            sync.log.warn("Ignoring the attribute " + k + " for the SyncObject vcincab. " + k + " not defined as an attribute in SyncConfiguration.");
        } else if (!kony.sync.isNull(PKTable[k])) {
            if (insert === false) {
                sync.log.warn("Ignoring the primary key " + k + " for the SyncObject vcincab. Primary Key should not be the part of the attributes to be updated in the local device database.");
            } else if (PKTable[k].isAutoGen) {
                sync.log.warn("Ignoring the auto-generated primary key " + k + " for the SyncObject vcincab. Auto-generated Primary Key should not be the part of the attributes to be inserted in the local device database.");
            } else {
                newvaluestable[k] = v;
            }
        } else {
            newvaluestable[k] = v;
        }
    }
    return newvaluestable;
};
VCScope.vcincab.formOrderByClause = function(orderByMap) {
    sync.log.trace("Entering VCScope.vcincab.formOrderByClause function");
    if (!kony.sync.isNull(orderByMap)) {
        var valuestable = kony.sync.convertOrderByMapToValuesTable(orderByMap);
        //var filteredValuestable = VCScope.vcincab.filterAttributes(valuestable, true);
        return kony.sync.convertToValuesTableOrderByMap(orderByMap, valuestable);
    }
    return null;
};
VCScope.vcincab.prototype.getValuesTable = function(isInsert) {
    sync.log.trace("Entering VCScope.vcincab.prototype.getValuesTable function");
    var valuesTable = {};
    if (isInsert === true) {
        valuesTable.cabid = this.cabid;
    }
    valuesTable.regno = this.regno;
    valuesTable.email = this.email;
    valuesTable.fuel = this.fuel;
    valuesTable.Mirrors = this.Mirrors;
    valuesTable.Horn = this.Horn;
    valuesTable.Indicators = this.Indicators;
    valuesTable.GroupLightsA = this.GroupLightsA;
    valuesTable.GroupLightsB = this.GroupLightsB;
    valuesTable.GroupLightsC = this.GroupLightsC;
    valuesTable.Windscreen = this.Windscreen;
    valuesTable.Tachograph = this.Tachograph;
    valuesTable.Brakes = this.Brakes;
    valuesTable.Wipers = this.Wipers;
    valuesTable.Steering = this.Steering;
    valuesTable.GeneralSafety = this.GeneralSafety;
    return valuesTable;
};
VCScope.vcincab.prototype.getPKTable = function() {
    sync.log.trace("Entering VCScope.vcincab.prototype.getPKTable function");
    var pkTable = {};
    pkTable.cabid = {
        key: "cabid",
        value: this.cabid
    };
    return pkTable;
};
VCScope.vcincab.getPKTable = function() {
    sync.log.trace("Entering VCScope.vcincab.getPKTable function");
    var pkTable = [];
    pkTable.push("cabid");
    return pkTable;
};
VCScope.vcincab.pkCheck = function(pks, wcs, errorcallback, opName) {
    sync.log.trace("Entering VCScope.vcincab.pkCheck function");
    var wc = [];
    if (kony.sync.isNull(pks)) {
        sync.log.error("Primary Key cabid not specified in  " + opName + "  an item in vcincab");
        kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodePrimaryKeyNotSpecified, kony.sync.getPrimaryKeyNotSpecifiedMsg("cabid", opName, "vcincab")));
        return false;
    } else if (kony.sync.isValidJSTable(pks)) {
        if (!kony.sync.isNull(pks.cabid)) {
            if (!kony.sync.isNull(pks.cabid.value)) {
                wc.key = "cabid";
                wc.value = pks.cabid.value;
            } else {
                wc.key = "cabid";
                wc.value = pks.cabid;
            }
        } else {
            sync.log.error("Primary Key cabid not specified in  " + opName + "  an item in vcincab");
            kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodePrimaryKeyNotSpecified, kony.sync.getPrimaryKeyNotSpecifiedMsg("cabid", opName, "vcincab")));
            return false;
        }
    } else {
        wc.key = "cabid";
        wc.value = pks;
    }
    kony.table.insert(wcs, wc);
    return true;
};
VCScope.vcincab.validateNull = function(valuestable, errorcallback) {
    sync.log.trace("Entering VCScope.vcincab.validateNull function");
    return true;
};
VCScope.vcincab.validateNullInsert = function(valuestable, errorcallback) {
    sync.log.trace("Entering VCScope.vcincab.validateNullInsert function");
    return true;
};
VCScope.vcincab.getRelationshipMap = function(relationshipMap, valuestable) {
    sync.log.trace("Entering VCScope.vcincab.getRelationshipMap function");
    var r1 = {};
    return relationshipMap;
};
VCScope.vcincab.checkPKValueTables = function(valuetables) {
    var checkPksNotNullFlag = true;
    for (var i = 0; i < valuetables.length; i++) {
        if (kony.sync.isNull(valuetables[i])) {
            checkPksNotNullFlag = false;
            break;
        }
    }
    return checkPksNotNullFlag;
};
VCScope.vcincab.getTableName = function() {
    return "vcincab";
};
// **********************************End vcincab's helper methods************************