//****************Sync Version:MobileFabricInstaller-QA-7.2.0_v201610191639_r117*******************
// ****************Generated On Fri Nov 25 12:14:24 UTC 2016vcunbonnet*******************
// **********************************Start vcunbonnet's helper methods************************
if (typeof(kony) === "undefined") {
    kony = {};
}
if (typeof(kony.sync) === "undefined") {
    kony.sync = {};
}
if (typeof(kony.sync.log) === "undefined") {
    kony.sync.log = {};
}
if (typeof(sync) === "undefined") {
    sync = {};
}
if (typeof(sync.log) === "undefined") {
    sync.log = {};
}
if (typeof(VCScope) === "undefined") {
    VCScope = {};
}
/************************************************************************************
 * Creates new vcunbonnet
 *************************************************************************************/
VCScope.vcunbonnet = function() {
    this.unbid = null;
    this.regno = null;
    this.email = null;
    this.OilCheck = null;
    this.CoolantLevel = null;
    this.WasherBottle = null;
    this.BrakeFluid = null;
    this.isDeleted = null;
    this.lastModifiedDate = null;
    this.markForUpload = true;
};
VCScope.vcunbonnet.prototype = {
    get unbid() {
        return this._unbid;
    },
    set unbid(val) {
        if (!kony.sync.isEmptyString(val) && !kony.sync.isNull(val) && !kony.sync.isValidNumberType(val)) {
            sync.log.error("Invalid data type for the attribute unbid in vcunbonnet.\nExpected:\"integer\"\nActual:\"" + kony.type(val) + "\"");
        }
        this._unbid = val;
    },
    get regno() {
        return this._regno;
    },
    set regno(val) {
        this._regno = val;
    },
    get email() {
        return this._email;
    },
    set email(val) {
        this._email = val;
    },
    get OilCheck() {
        return this._OilCheck;
    },
    set OilCheck(val) {
        this._OilCheck = val;
    },
    get CoolantLevel() {
        return this._CoolantLevel;
    },
    set CoolantLevel(val) {
        this._CoolantLevel = val;
    },
    get WasherBottle() {
        return this._WasherBottle;
    },
    set WasherBottle(val) {
        this._WasherBottle = val;
    },
    get BrakeFluid() {
        return this._BrakeFluid;
    },
    set BrakeFluid(val) {
        this._BrakeFluid = val;
    },
    get isDeleted() {
        return kony.sync.getBoolean(this._isDeleted) + "";
    },
    set isDeleted(val) {
        if (!kony.sync.isEmptyString(val) && !kony.sync.isNull(val) && !kony.sync.isValidBooleanType(val)) {
            sync.log.error("Invalid data type for the attribute isDeleted in vcunbonnet.\nExpected:\"boolean\"\nActual:\"" + kony.type(val) + "\"");
        }
        this._isDeleted = val;
    },
    get lastModifiedDate() {
        return this._lastModifiedDate;
    },
    set lastModifiedDate(val) {
        this._lastModifiedDate = val;
    },
};
/************************************************************************************
 * Retrieves all instances of vcunbonnet SyncObject present in local database with
 * given limit and offset where limit indicates the number of records to be retrieved
 * and offset indicates number of rows to be ignored before returning the records.
 * e.g. var orderByMap = []
 * orderByMap[0] = {};
 * orderByMap[0].key = "unbid";
 * orderByMap[0].sortType ="desc";
 * orderByMap[1] = {};
 * orderByMap[1].key = "regno";
 * orderByMap[1].sortType ="asc";
 * var limit = 20;
 * var offset = 5;
 * VCScope.vcunbonnet.getAll(successcallback,errorcallback, orderByMap, limit, offset)
 *************************************************************************************/
VCScope.vcunbonnet.getAll = function(successcallback, errorcallback, orderByMap, limit, offset) {
    sync.log.trace("Entering VCScope.vcunbonnet.getAll->main function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = VCScope.vcunbonnet.getTableName();
    orderByMap = kony.sync.formOrderByClause("vcunbonnet", orderByMap);
    var query = kony.sync.qb_createQuery();
    kony.sync.qb_select(query, null);
    kony.sync.qb_from(query, tbname);
    kony.sync.qb_orderBy(query, orderByMap);
    kony.sync.qb_limitOffset(query, limit, offset);
    var query_compile = kony.sync.qb_compile(query);
    var sql = query_compile[0];
    var params = query_compile[1];

    function mySuccCallback(res) {
        sync.log.trace("Entering VCScope.vcunbonnet.getAll->successcallback");
        successcallback(VCScope.vcunbonnet.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
    }
    kony.sync.single_select_execute(dbname, sql, params, mySuccCallback, errorcallback);
};
/************************************************************************************
 * Returns number of vcunbonnet present in local database.
 *************************************************************************************/
VCScope.vcunbonnet.getAllCount = function(successcallback, errorcallback) {
    sync.log.trace("Entering VCScope.vcunbonnet.getAllCount function");
    VCScope.vcunbonnet.getCount("", successcallback, errorcallback);
};
/************************************************************************************
 * Returns number of vcunbonnet using where clause in the local Database
 *************************************************************************************/
VCScope.vcunbonnet.getCount = function(wcs, successcallback, errorcallback) {
    sync.log.trace("Entering VCScope.vcunbonnet.getCount->main function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    if (!kony.sync.validateInput(arguments, "VCScope.vcunbonnet.getCount", "getCount", errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = VCScope.vcunbonnet.getTableName();
    wcs = kony.sync.validateWhereClause(wcs);
    var sql = "select count(*) from \"" + tbname + "\" " + wcs;
    kony.sync.single_execute_sql(dbname, sql, null, mySuccCallback, errorcallback);

    function mySuccCallback(res) {
        sync.log.trace("Entering VCScope.vcunbonnet.getCount->successcallback");
        if (null !== res) {
            var count = null;
            count = res["count(*)"];
            kony.sync.verifyAndCallClosure(successcallback, {
                count: count
            });
        } else {
            sync.log.error("Some error occured while getting the count");
        }
    }
};
/************************************************************************************
 * Creates a new instance of vcunbonnet in the local Database. The new record will 
 * be merged with the enterprise datasource in the next Sync.
 *************************************************************************************/
VCScope.vcunbonnet.prototype.create = function(successcallback, errorcallback) {
    sync.log.trace("Entering  VCScope.vcunbonnet.prototype.create function");
    var valuestable = this.getValuesTable(true);
    VCScope.vcunbonnet.create(valuestable, successcallback, errorcallback, this.markForUpload);
};
VCScope.vcunbonnet.create = function(valuestable, successcallback, errorcallback, markForUpload) {
    sync.log.trace("Entering  VCScope.vcunbonnet.create->main function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    if (!kony.sync.validateInput(arguments, "VCScope.vcunbonnet.create", "create", errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = VCScope.vcunbonnet.getTableName();
    markForUpload = kony.sync.getUploadStatus(markForUpload);
    if (kony.sync.attributeValidation(valuestable, "vcunbonnet", errorcallback, true) === false) {
        return;
    }

    function executeSuccess() {
        sync.log.trace("Entering  VCScope.vcunbonnet.create->success callback");
        kony.sync.single_insert_execute(dbname, tbname, valuestable, successcallback, errorcallback, markForUpload);
    }
    if (kony.sync.enableORMValidations) {
        var relationshipMap = {};
        relationshipMap = VCScope.vcunbonnet.getRelationshipMap(relationshipMap, valuestable);
        kony.sync.checkIntegrity(dbname, relationshipMap, executeSuccess, errorcallback);
    } else {
        kony.sync.single_insert_execute(dbname, tbname, valuestable, successcallback, errorcallback, markForUpload);
    }
};
/************************************************************************************
 * Creates number of new instances of vcunbonnet in the local Database. These new 
 * records will be merged with the enterprise datasource in the next Sync. Based upon 
 * kony.sync.enableORMValidations flag, validations will be enabled/disabled.
 * e.g.	var valuesArray = [];
 *		valuesArray[0] = {};
 *		valuesArray[0].regno = "regno_0";
 *		valuesArray[0].email = "email_0";
 *		valuesArray[0].OilCheck = "OilCheck_0";
 *		valuesArray[0].CoolantLevel = "CoolantLevel_0";
 *		valuesArray[0].WasherBottle = "WasherBottle_0";
 *		valuesArray[0].BrakeFluid = "BrakeFluid_0";
 *		valuesArray[1] = {};
 *		valuesArray[1].regno = "regno_1";
 *		valuesArray[1].email = "email_1";
 *		valuesArray[1].OilCheck = "OilCheck_1";
 *		valuesArray[1].CoolantLevel = "CoolantLevel_1";
 *		valuesArray[1].WasherBottle = "WasherBottle_1";
 *		valuesArray[1].BrakeFluid = "BrakeFluid_1";
 *		valuesArray[2] = {};
 *		valuesArray[2].regno = "regno_2";
 *		valuesArray[2].email = "email_2";
 *		valuesArray[2].OilCheck = "OilCheck_2";
 *		valuesArray[2].CoolantLevel = "CoolantLevel_2";
 *		valuesArray[2].WasherBottle = "WasherBottle_2";
 *		valuesArray[2].BrakeFluid = "BrakeFluid_2";
 *		VCScope.vcunbonnet.createAll(valuesArray, successcallback, errorcallback, true);
 *************************************************************************************/
VCScope.vcunbonnet.createAll = function(valuesArray, successcallback, errorcallback, markForUpload) {
    sync.log.trace("Entering VCScope.vcunbonnet.createAll function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    if (!kony.sync.validateInput(arguments, "VCScope.vcunbonnet.createAll", "createAll", errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = VCScope.vcunbonnet.getTableName();
    markForUpload = kony.sync.getUploadStatus(markForUpload);
    var isProperData = true;
    var arrayLen = 0;
    var errorInfo = [];
    var arrayLength = valuesArray.length;
    var errObject = null;
    var isReferentialIntegrityFailure = false;
    var errMsg = null;
    if (kony.sync.enableORMValidations) {
        var newValuesArray = [];
        //column level validations
        for (var i = 0; valuesArray != null && i < arrayLength; i++) {
            var valuestable = valuesArray[i];
            if (kony.sync.attributeValidation(valuestable, "vcunbonnet", errorcallback, true) === false) {
                return;
            }
            newValuesArray[i] = valuestable;
        }
        valuesArray = newValuesArray;
        var connection = kony.sync.getConnectionOnly(dbname, dbname);
        kony.sync.startTransaction(connection, checkIntegrity, transactionSuccessCallback, transactionErrorCallback);
        var isError = false;
    } else {
        //copying by value
        var newValuesArray = [];
        arrayLength = valuesArray.length;
        for (var i = 0; valuesArray != null && i < arrayLength; i++) {
            newValuesArray[i] = kony.sync.CreateCopy(valuesArray[i]);
        }
        valuesArray = newValuesArray;
        kony.sync.massInsert(dbname, tbname, valuesArray, successcallback, errorcallback, markForUpload);
    }

    function transactionErrorCallback() {
        if (isError == true) {
            //Statement error has occurred
            kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
        } else {
            //Transaction error has occurred
            kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodeTransaction, kony.sync.getErrorMessage(kony.sync.errorCodeTransaction), null));
        }
    }

    function transactionSuccessCallback() {
        sync.log.trace("Entering  VCScope.vcunbonnet.createAll->transactionSuccessCallback");
        if (!isError) {
            kony.sync.massInsert(dbname, tbname, valuesArray, successcallback, errorcallback, markForUpload);
        } else {
            if (isReferentialIntegrityFailure) {
                kony.sync.verifyAndCallClosure(errorcallback, errObject);
            }
        }
    }
    //foreign key constraints validations
    function checkIntegrity(tx) {
        sync.log.trace("Entering  VCScope.vcunbonnet.createAll->checkIntegrity");
        arrayLength = valuesArray.length;
        for (var i = 0; valuesArray != null && i < arrayLength; i++) {
            var relationshipMap = {};
            relationshipMap = VCScope.vcunbonnet.getRelationshipMap(relationshipMap, valuesArray[i]);
            errObject = kony.sync.checkIntegrityinTransaction(tx, relationshipMap, null);
            if (errObject === false) {
                isError = true;
                return;
            }
            if (errObject !== true) {
                isError = true;
                isReferentialIntegrityFailure = true;
                return;
            }
        }
    }
};
/************************************************************************************
 * Updates vcunbonnet using primary key in the local Database. The update will be
 * merged with the enterprise datasource in the next Sync.
 *************************************************************************************/
VCScope.vcunbonnet.prototype.updateByPK = function(successcallback, errorcallback) {
    sync.log.trace("Entering  VCScope.vcunbonnet.prototype.updateByPK function");
    var pks = this.getPKTable();
    var valuestable = this.getValuesTable(false);
    VCScope.vcunbonnet.updateByPK(pks, valuestable, successcallback, errorcallback, this.markForUpload);
};
VCScope.vcunbonnet.updateByPK = function(pks, valuestable, successcallback, errorcallback, markForUpload) {
    sync.log.trace("Entering  VCScope.vcunbonnet.updateByPK-> main function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    if (!kony.sync.validateInput(arguments, "VCScope.vcunbonnet.updateByPK", "updateByPk", errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = VCScope.vcunbonnet.getTableName();
    markForUpload = kony.sync.getUploadStatus(markForUpload);
    var wcs = [];
    if (VCScope.vcunbonnet.pkCheck(pks, wcs, errorcallback, "updating") === false) {
        return;
    }
    if (kony.sync.attributeValidation(valuestable, "vcunbonnet", errorcallback, false) === false) {
        return;
    }
    var relationshipMap = {};
    relationshipMap = VCScope.vcunbonnet.getRelationshipMap(relationshipMap, valuestable);
    kony.sync.updateByPK(tbname, dbname, relationshipMap, pks, valuestable, successcallback, errorcallback, markForUpload, wcs);
};
/************************************************************************************
 * Updates vcunbonnet(s) using where clause in the local Database. The update(s)
 * will be merged with the enterprise datasource in the next Sync.
 *************************************************************************************/
VCScope.vcunbonnet.update = function(wcs, valuestable, successcallback, errorcallback, markForUpload) {
    sync.log.trace("Entering VCScope.vcunbonnet.update function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    if (!kony.sync.validateInput(arguments, "VCScope.vcunbonnet.update", "update", errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = VCScope.vcunbonnet.getTableName();
    markForUpload = kony.sync.getUploadStatus(markForUpload);
    wcs = kony.sync.validateWhereClause(wcs);
    if (kony.sync.attributeValidation(valuestable, "vcunbonnet", errorcallback, false) === false) {
        return;
    }

    function executeSuccess() {
        sync.log.trace("Entering  VCScope.vcunbonnet.update-> success callback of Integrity Check");
        kony.sync.single_update_execute(dbname, tbname, valuestable, wcs, successcallback, errorcallback, true, markForUpload, VCScope.vcunbonnet.getPKTable());
    }
    if (kony.sync.enableORMValidations) {
        var relationshipMap = {};
        relationshipMap = VCScope.vcunbonnet.getRelationshipMap(relationshipMap, valuestable);
        kony.sync.checkIntegrity(dbname, relationshipMap, executeSuccess, errorcallback);
    } else {
        kony.sync.single_update_execute(dbname, tbname, valuestable, wcs, successcallback, errorcallback, true, markForUpload, VCScope.vcunbonnet.getPKTable());
    }
};
/************************************************************************************
 * Updates vcunbonnet(s) satisfying one or more where clauses in the local Database. 
 * The update(s) will be merged with the enterprise datasource in the next Sync.
 * Based upon kony.sync.enableORMValidations flag, validations will be enabled/disabled.
 * e.g.	var inputArray = [];
 *		inputArray[0] = {};
 *		inputArray[0].changeSet = {};
 *		inputArray[0].changeSet.regno = "regno_updated0";
 *		inputArray[0].changeSet.email = "email_updated0";
 *		inputArray[0].changeSet.OilCheck = "OilCheck_updated0";
 *		inputArray[0].changeSet.CoolantLevel = "CoolantLevel_updated0";
 *		inputArray[0].whereClause = "where unbid = 0";
 *		inputArray[1] = {};
 *		inputArray[1].changeSet = {};
 *		inputArray[1].changeSet.regno = "regno_updated1";
 *		inputArray[1].changeSet.email = "email_updated1";
 *		inputArray[1].changeSet.OilCheck = "OilCheck_updated1";
 *		inputArray[1].changeSet.CoolantLevel = "CoolantLevel_updated1";
 *		inputArray[1].whereClause = "where unbid = 1";
 *		inputArray[2] = {};
 *		inputArray[2].changeSet = {};
 *		inputArray[2].changeSet.regno = "regno_updated2";
 *		inputArray[2].changeSet.email = "email_updated2";
 *		inputArray[2].changeSet.OilCheck = "OilCheck_updated2";
 *		inputArray[2].changeSet.CoolantLevel = "CoolantLevel_updated2";
 *		inputArray[2].whereClause = "where unbid = 2";
 *		VCScope.vcunbonnet.updateAll(inputArray,successcallback,errorcallback);
 *************************************************************************************/
VCScope.vcunbonnet.updateAll = function(inputArray, successcallback, errorcallback, markForUpload) {
        sync.log.trace("Entering VCScope.vcunbonnet.updateAll function");
        if (!kony.sync.isSyncInitialized(errorcallback)) {
            return;
        }
        if (!kony.sync.validateInput(arguments, "VCScope.vcunbonnet.updateAll", "updateAll", errorcallback)) {
            return;
        }
        var dbname = "100004898461ca65a";
        var tbname = "vcunbonnet";
        var isError = false;
        var errObject = null;
        if (markForUpload == false || markForUpload == "false") {
            markForUpload = "false"
        } else {
            markForUpload = "true"
        }
        if ((kony.sync.enableORMValidations)) {
            var newInputArray = [];
            for (var i = 0;
                ((inputArray) != null) && i < inputArray.length; i++) {
                var v = inputArray[i];
                var valuestable = v.changeSet;
                var isEmpty = true;
                for (var key in valuestable) {
                    isEmpty = false;
                    break;
                }
                if (isEmpty) {
                    errorcallback(kony.sync.getErrorTable(kony.sync.errorCodeNullValue, kony.sync.getErrorMessage(kony.sync.errorCodeNullValue)));
                    return;
                }
                var wcs = v.whereClause;
                var twcs = wcs;
                if (kony.sync.attributeValidation(valuestable, "vcunbonnet", errorcallback, false) === false) {
                    return;
                }
                newInputArray[i] = [];
                newInputArray[i].changeSet = valuestable;
                newInputArray[i].whereClause = wcs;
            }
            inputArray = newInputArray;
            var connection = kony.sync.getConnectionOnly(dbname, dbname);
            kony.sync.startTransaction(connection, checkIntegrity, transactionSuccessCallback, transactionErrorCallback);
        } else {
            //copying by value
            var newInputArray = [];
            for (var i = 0;
                ((inputArray) != null) && i < inputArray.length; i++) {
                var v = inputArray[i];
                newInputArray[i] = kony.sync.CreateCopy(v);
            }
            inputArray = newInputArray;
            kony.sync.massUpdate(dbname, tbname, inputArray, successcallback, errorcallback, markForUpload, VCScope.vcunbonnet.getPKTable());
        }

        function transactionSuccessCallback() {
            sync.log.trace("Entering  VCScope.vcunbonnet.updateAll->transactionSuccessCallback");
            if (!isError) {
                kony.sync.massUpdate(dbname, tbname, inputArray, successcallback, transactionErrorCallback, markForUpload, VCScope.vcunbonnet.getPKTable());
            }
        }

        function transactionErrorCallback() {
            if (errObject === false) {
                //Sql statement error has occcurred
                kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
                kony.sync.errorObject = null;
            } else if (errObject !== null) {
                // Referential integrity error has occurred
                kony.sync.verifyAndCallClosure(errorcallback, errObject);
            } else {
                //Transaction error has occurred
                kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodeTransaction, kony.sync.getErrorMessage(kony.sync.errorCodeTransaction), null));
            }
        }
        //foreign key constraints validations
        function checkIntegrity(tx) {
            sync.log.trace("Entering  VCScope.vcunbonnet.updateAll->checkIntegrity");
            for (var i = 0;
                ((inputArray) != null) && i < inputArray.length; i++) {
                var relationshipMap = {};
                relationshipMap = VCScope.vcunbonnet.getRelationshipMap(relationshipMap, inputArray[i].changeSet);
                sync.log.debug("Relationship Map for Integrity check created:", relationshipMap);
                errObject = kony.sync.checkIntegrityinTransaction(tx, relationshipMap, null);
                if (errObject === false) {
                    isError = true;
                    return;
                }
                if (errObject !== true) {
                    isError = true;
                    kony.sync.rollbackTransaction(tx);
                    return;
                }
            }
        }
    }
    /************************************************************************************
     * Deletes vcunbonnet using primary key from the local Database. The record will be
     * deleted from the enterprise datasource in the next Sync.
     *************************************************************************************/
VCScope.vcunbonnet.prototype.deleteByPK = function(successcallback, errorcallback) {
    sync.log.trace("Entering VCScope.vcunbonnet.prototype.deleteByPK function");
    var pks = this.getPKTable();
    VCScope.vcunbonnet.deleteByPK(pks, successcallback, errorcallback, this.markForUpload);
};
VCScope.vcunbonnet.deleteByPK = function(pks, successcallback, errorcallback, markForUpload) {
    sync.log.trace("Entering VCScope.vcunbonnet.deleteByPK-> main function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    if (!kony.sync.validateInput(arguments, "VCScope.vcunbonnet.deleteByPK", "deleteByPK", errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = VCScope.vcunbonnet.getTableName();
    markForUpload = kony.sync.getUploadStatus(markForUpload);
    var wcs = [];
    var isError = false;
    var pkNotFound = false;
    var twcs = [];
    var deletedRows;
    var record = "";
    if (VCScope.vcunbonnet.pkCheck(pks, wcs, errorcallback, "deleting") === false) {
        return;
    }
    twcs = kony.sync.CreateCopy(wcs);

    function vcunbonnetTransactionCallback(tx) {
        sync.log.trace("Entering VCScope.vcunbonnet.deleteByPK->vcunbonnet_PKPresent successcallback");
        record = kony.sync.getOriginalRow(tx, tbname, wcs, errorcallback);
        if (record === false) {
            isError = true;
            return;
        }
        if (null !== record) {} else {
            pkNotFound = true;
        }
        var deletedRows = kony.sync.remove(tx, tbname, wcs, false, markForUpload, null);
        if (deletedRows === false) {
            isError = true;
        }
    }

    function vcunbonnetErrorCallback() {
        sync.log.error("Entering VCScope.vcunbonnet.deleteByPK->relationship failure callback");
        if (isError === false) {
            kony.sync.verifyAndCallClosure(errorcallback);
        }
        if (kony.sync.errorObject != null) {
            kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
            kony.sync.errorObject = null;
        }
    }

    function vcunbonnetSuccessCallback() {
        sync.log.trace("Entering VCScope.vcunbonnet.deleteByPK->relationship success callback");
        if (pkNotFound === true) {
            kony.sync.verifyAndCallClosure(pkNotFoundErrCallback);
            return;
        }
        if (!isError) {
            kony.sync.verifyAndCallClosure(successcallback, {
                rowsdeleted: 1
            });
        }
    }

    function pkNotFoundErrCallback() {
        sync.log.error("Entering VCScope.vcunbonnet.deleteByPK->PK not found callback");
        kony.sync.pkNotFoundErrCallback(errorcallback, tbname);
    }
    var dbconnection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
    if (dbconnection != null) {
        kony.sync.startTransaction(dbconnection, vcunbonnetTransactionCallback, vcunbonnetSuccessCallback, vcunbonnetErrorCallback, "Single Execute");
    }
};
/************************************************************************************
 * Deletes vcunbonnet(s) using where clause from the local Database. The record(s)
 * will be deleted from the enterprise datasource in the next Sync.
 * e.g. VCScope.vcunbonnet.remove("where regno like 'A%'", successcallback,errorcallback, true);
 *************************************************************************************/
VCScope.vcunbonnet.remove = function(wcs, successcallback, errorcallback, markForUpload) {
    sync.log.trace("Entering VCScope.vcunbonnet.remove->main function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    if (!kony.sync.validateInput(arguments, "VCScope.vcunbonnet.remove", "remove", errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = VCScope.vcunbonnet.getTableName();
    markForUpload = kony.sync.getUploadStatus(markForUpload);
    wcs = kony.sync.validateWhereClause(wcs);
    var twcs = wcs;
    var isError = false;
    var rowsDeleted;

    function vcunbonnet_removeTransactioncallback(tx) {
        wcs = " " + wcs;
        rowsDeleted = kony.sync.deleteBatch(tx, tbname, wcs, false, markForUpload, errorcallback)
        if (rowsDeleted === false) {
            isError = true;
        }
    }

    function vcunbonnet_removeSuccess() {
        sync.log.trace("Entering VCScope.vcunbonnet.remove->vcunbonnet_removeSuccess function");
        if (!isError) {
            kony.sync.verifyAndCallClosure(successcallback, rowsDeleted);
        }
    }

    function errorcallbackWrapper() {
        sync.log.trace("Entering VCScope.vcunbonnet.remove->error callback function");
        if (!isError) {
            kony.sync.showTransactionError(errorcallback);
        }
        if (kony.sync.errorObject != null) {
            kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
            kony.sync.errorObject = null;
        }
    }

    function deleteEntity() {
        sync.log.trace("Entering VCScope.vcunbonnet.remove->delete Entity function");
        var connection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
        if (connection != null) {
            kony.sync.startTransaction(connection, vcunbonnet_removeTransactioncallback, vcunbonnet_removeSuccess, errorcallbackWrapper);
        }
    }
    deleteEntity();
};
/************************************************************************************
 * Deletes vcunbonnet using primary key from the local Database. This will
 * not have any effect in enterprise datasource in subsequent sync cycles
 *************************************************************************************/
VCScope.vcunbonnet.prototype.removeDeviceInstanceByPK = function(successcallback, errorcallback) {
    sync.log.trace("Entering VCScope.vcunbonnet.prototype.removeDeviceInstanceByPK function");
    var pks = this.getPKTable();
    VCScope.vcunbonnet.removeDeviceInstanceByPK(pks, successcallback, errorcallback);
};
VCScope.vcunbonnet.removeDeviceInstanceByPK = function(pks, successcallback, errorcallback) {
    sync.log.trace("Entering VCScope.vcunbonnet.removeDeviceInstanceByPK function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    if (!kony.sync.validateInput(arguments, "VCScope.vcunbonnet.removeDeviceInstanceByPK", "removeDeviceInstanceByPK", errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = VCScope.vcunbonnet.getTableName();
    var wcs = [];
    var isError = false;
    var pkNotFound = false;
    var deletedRows;
    if (VCScope.vcunbonnet.pkCheck(pks, wcs, errorcallback, "deleting") === false) {
        return;
    }

    function vcunbonnetTransactionCallback(tx) {
        sync.log.trace("Entering VCScope.vcunbonnet.removeDeviceInstanceByPK -> vcunbonnetTransactionCallback");
        var record = kony.sync.getOriginalRow(tx, tbname, wcs, errorcallback);
        if (null !== record && false != record) {
            deletedRows = kony.sync.remove(tx, tbname, wcs, true, null, null);
            if (deletedRows === false) {
                isError = true;
            }
        } else {
            pkNotFound = true;
        }
    }

    function vcunbonnetErrorCallback() {
        sync.log.error("Entering VCScope.vcunbonnet.removeDeviceInstanceByPK -> vcunbonnetErrorCallback");
        if (isError === false) {
            kony.sync.verifyAndCallClosure(errorcallback);
        }
        if (kony.sync.errorObject != null) {
            kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
            kony.sync.errorObject = null;
        }
    }

    function vcunbonnetSuccessCallback() {
        sync.log.trace("Entering VCScope.vcunbonnet.removeDeviceInstanceByPK -> vcunbonnetSuccessCallback");
        if (pkNotFound === true) {
            kony.sync.verifyAndCallClosure(pkNotFoundErrCallback);
            return;
        }
        if (!isError) {
            kony.sync.verifyAndCallClosure(successcallback, {
                rowsdeleted: 1
            });
        }
    }

    function pkNotFoundErrCallback() {
        sync.log.error("Entering VCScope.vcunbonnet.removeDeviceInstanceByPK -> PK not found callback");
        kony.sync.pkNotFoundErrCallback(errorcallback, tbname);
    }
    var dbconnection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
    if (dbconnection != null) {
        kony.sync.startTransaction(dbconnection, vcunbonnetTransactionCallback, vcunbonnetSuccessCallback, vcunbonnetErrorCallback, "Single Execute");
    }
};
/************************************************************************************
 * Deletes vcunbonnet(s) using where clause from the local Database. This will
 * not have any effect in enterprise datasource in subsequent sync cycles
 *************************************************************************************/
VCScope.vcunbonnet.removeDeviceInstance = function(wcs, successcallback, errorcallback) {
    sync.log.trace("Entering VCScope.vcunbonnet.removeDeviceInstance->main function");
    var dbname = kony.sync.getDBName();
    var tbname = VCScope.vcunbonnet.getTableName();
    wcs = kony.sync.validateWhereClause(wcs);
    var twcs = wcs;
    var isError = false;
    var rowsDeleted;

    function vcunbonnet_removeTransactioncallback(tx) {
        wcs = " " + wcs;
        rowsDeleted = kony.sync.deleteBatch(tx, tbname, wcs, true, null, errorcallback)
        if (rowsDeleted === false) {
            isError = true;
        }
    }

    function vcunbonnet_removeSuccess() {
        sync.log.trace("Entering VCScope.vcunbonnet.remove->vcunbonnet_removeSuccess function");
        if (!isError) {
            kony.sync.verifyAndCallClosure(successcallback, rowsDeleted);
        }
    }

    function errorcallbackWrapper() {
        sync.log.trace("Entering VCScope.vcunbonnet.remove->error callback function");
        if (!isError) {
            kony.sync.showTransactionError(errorcallback);
        }
        if (kony.sync.errorObject != null) {
            kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
            kony.sync.errorObject = null;
        }
    }

    function deleteEntity() {
        sync.log.trace("Entering VCScope.vcunbonnet.remove->delete Entity function");
        var connection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
        if (connection != null) {
            kony.sync.startTransaction(connection, vcunbonnet_removeTransactioncallback, vcunbonnet_removeSuccess, errorcallbackWrapper);
        }
    }
    deleteEntity();
};
/************************************************************************************
 * Retrieves vcunbonnet using primary key from the local Database. 
 *************************************************************************************/
VCScope.vcunbonnet.prototype.getAllDetailsByPK = function(successcallback, errorcallback) {
    sync.log.trace("Entering VCScope.vcunbonnet.prototype.getAllDetailsByPK function");
    var pks = this.getPKTable();
    VCScope.vcunbonnet.getAllDetailsByPK(pks, successcallback, errorcallback);
};
VCScope.vcunbonnet.getAllDetailsByPK = function(pks, successcallback, errorcallback) {
    sync.log.trace("Entering VCScope.vcunbonnet.getAllDetailsByPK-> main function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    if (!kony.sync.validateInput(arguments, "VCScope.vcunbonnet.getAllDetailsByPK", "getAllDetailsByPK", errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = VCScope.vcunbonnet.getTableName();
    var wcs = [];
    if (VCScope.vcunbonnet.pkCheck(pks, wcs, errorcallback, "searching") === false) {
        return;
    }
    twcs = kony.sync.CreateCopy(wcs);
    var query = kony.sync.qb_createQuery();
    kony.sync.qb_select(query, null);
    kony.sync.qb_from(query, tbname);
    kony.sync.qb_where(query, wcs);
    var query_compile = kony.sync.qb_compile(query);
    var sql = query_compile[0];
    var params = query_compile[1];

    function mySuccCallback(res) {
        sync.log.trace("Entering VCScope.vcunbonnet.getAllDetailsByPK-> success callback function");
        successcallback(VCScope.vcunbonnet.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
    }
    kony.sync.single_select_execute(dbname, sql, params, mySuccCallback, errorcallback);
};
/************************************************************************************
 * Retrieves vcunbonnet(s) using where clause from the local Database. 
 * e.g. VCScope.vcunbonnet.find("where regno like 'A%'", successcallback,errorcallback);
 *************************************************************************************/
VCScope.vcunbonnet.find = function(wcs, successcallback, errorcallback) {
    sync.log.trace("Entering VCScope.vcunbonnet.find function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    if (!kony.sync.validateInput(arguments, "VCScope.vcunbonnet.find", "find", errorcallback)) {
        return;
    }
    //wcs will be a string formed by the user.
    var dbname = kony.sync.getDBName();
    var tbname = VCScope.vcunbonnet.getTableName();
    wcs = kony.sync.validateWhereClause(wcs);
    var sql = "select * from \"" + tbname + "\" " + wcs;

    function mySuccCallback(res) {
        kony.sync.verifyAndCallClosure(successcallback, VCScope.vcunbonnet.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
    }
    kony.sync.single_select_execute(dbname, sql, null, mySuccCallback, errorcallback);
};
/************************************************************************************
 * Marks instance of vcunbonnet with given primary key for upload. This will 
 * enable deferred records to merge with the enterprise datasource in the next Sync.
 *************************************************************************************/
VCScope.vcunbonnet.prototype.markForUploadbyPK = function(successcallback, errorcallback) {
    sync.log.trace("Entering VCScope.vcunbonnet.prototype.markForUploadbyPK function");
    var pks = this.getPKTable();
    VCScope.vcunbonnet.markForUploadbyPK(pks, successcallback, errorcallback);
};
VCScope.vcunbonnet.markForUploadbyPK = function(pks, successcallback, errorcallback) {
    sync.log.trace("Entering VCScope.vcunbonnet.markForUploadbyPK function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    if (!kony.sync.validateInput(arguments, "VCScope.vcunbonnet.markForUploadbyPK", "markForUploadbyPK", errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = VCScope.vcunbonnet.getTableName();
    var isError = false;
    var recordsFound = false;
    var recordsMarkedForUpload = 0;
    var wcs = [];
    if (VCScope.vcunbonnet.pkCheck(pks, wcs, errorcallback, "marking for upload by PK") === false) {
        return;
    }

    function markRecordForUpload(tx, record) {
        var versionMapMain = [];
        versionMapMain[kony.sync.mainTableChangeTypeColumn] = kony.sync.getChangeTypeForUploadTrue(record[kony.sync.historyTableChangeTypeColumn]);
        var scopename = kony.sync.scopes.syncTableScopeDic[tbname];
        var versionNo = kony.sync.getseqnumber(tx, scopename);
        if (versionNo === false) {
            return false;
        }
        versionMapMain[kony.sync.historyTableSyncVersionColumn] = versionNo.versionnumber;
        var query = kony.sync.qb_createQuery();
        kony.sync.qb_update(query, tbname);
        kony.sync.qb_set(query, versionMapMain);
        kony.sync.qb_where(query, wcs);
        var query_compile = kony.sync.qb_compile(query);
        var sql = query_compile[0];
        var params = query_compile[1];
        return kony.sync.executeSql(tx, sql, params);
    }

    function markRecordForUploadHistory(tx, record) {
        var versionMap = [];
        versionMap[kony.sync.historyTableChangeTypeColumn] = kony.sync.getChangeTypeForUploadTrue(record[kony.sync.historyTableChangeTypeColumn]);
        var scopename = kony.sync.scopes.syncTableScopeDic[tbname];
        var versionNo = kony.sync.getseqnumber(tx, scopename);
        if (versionNo === false) {
            return false;
        }
        var twcs = [];
        twcs = wcs;
        kony.table.insert(twcs, {
            key: kony.sync.historyTableChangeTypeColumn,
            value: record[kony.sync.historyTableChangeTypeColumn],
            optype: "EQ",
            comptype: "AND"
        });
        versionMap[kony.sync.historyTableSyncVersionColumn] = versionNo.versionnumber;
        var query = kony.sync.qb_createQuery();
        kony.sync.qb_update(query, tbname + kony.sync.historyTableName);
        kony.sync.qb_set(query, versionMap);
        kony.sync.qb_where(query, twcs);
        kony.table.remove(twcs);
        var query_compile = kony.sync.qb_compile(query);
        var sql = query_compile[0];
        var params = query_compile[1];
        return kony.sync.executeSql(tx, sql, params);
    }

    function single_transaction_callback(tx) {
        var query = kony.sync.qb_createQuery();
        kony.sync.qb_select(query, [kony.sync.historyTableChangeTypeColumn]);
        kony.sync.qb_from(query, tbname);
        kony.sync.qb_where(query, wcs);
        var query_compile = kony.sync.qb_compile(query);
        var sql = query_compile[0];
        var params = query_compile[1];
        var resultSet = kony.sync.executeSql(tx, sql, params);
        if (resultSet === false) {
            isError = true;
            return;
        }
        var num_records = resultSet.rows.length;
        if (num_records > 0) {
            recordsFound = true;
            var record = kony.db.sqlResultsetRowItem(tx, resultSet, 0);
            var changeType = record[kony.sync.mainTableChangeTypeColumn];
            if (!kony.sync.isNullOrUndefined(changeType) && kony.string.startsWith("" + changeType, "9")) {
                recordsMarkedForUpload = 1;
                if (markRecordForUpload(tx, record) === false) {
                    isError = true;
                    return;
                }
            }
        }
        var query1 = kony.sync.qb_createQuery();
        kony.sync.qb_select(query1, [kony.sync.historyTableChangeTypeColumn]);
        kony.sync.qb_from(query1, tbname + kony.sync.historyTableName);
        kony.sync.qb_where(query1, wcs);
        var query1_compile = kony.sync.qb_compile(query1);
        var sql1 = query1_compile[0];
        var params1 = query1_compile[1];
        var resultSet1 = kony.sync.executeSql(tx, sql1, params1);
        if (resultSet1 !== false) {
            var num_records = resultSet1.rows.length;
            for (var i = 0; i <= num_records - 1; i++) {
                var record = kony.db.sqlResultsetRowItem(tx, resultSet1, i);
                if (markRecordForUploadHistory(tx, record) === false) {
                    isError = true;
                    return;
                }
                recordsFound = true;
            }
        } else {
            isError = true;
        }
    }

    function single_transaction_success_callback() {
        if (recordsFound === true) {
            kony.sync.verifyAndCallClosure(successcallback, {
                count: recordsMarkedForUpload
            });
        } else {
            kony.sync.pkNotFoundErrCallback(errorcallback, tbname);
        }
    }

    function single_transaction_error_callback(res) {
        if (!isError) {
            kony.sync.showTransactionError(errorcallback);
        } else {
            kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
            kony.sync.errorObject = null;
        }
    }
    var connection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
    if (connection != null) {
        kony.sync.startTransaction(connection, single_transaction_callback, single_transaction_success_callback, single_transaction_error_callback);
    }
};
/************************************************************************************
 * Marks instance(s) of vcunbonnet matching given where clause for upload. This will 
 * enable deferred records to merge with the enterprise datasource in the next Sync.
 *************************************************************************************/
VCScope.vcunbonnet.markForUpload = function(wcs, successcallback, errorcallback) {
    sync.log.trace("Entering VCScope.vcunbonnet.markForUpload->main function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    if (!kony.sync.validateInput(arguments, "VCScope.vcunbonnet.markForUpload", "markForUpload", errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = VCScope.vcunbonnet.getTableName();
    var isError = false;
    var num_records_main = 0;
    wcs = kony.sync.validateWhereClause(wcs);
    if (!kony.sync.isNull(wcs) && !kony.sync.isEmptyString(wcs)) {
        wcs = wcs + " and " + kony.sync.historyTableChangeTypeColumn + " like '9%'";
    } else {
        wcs = "where " + kony.sync.historyTableChangeTypeColumn + " like '9%'";
    }

    function markRecordForUpload(tx, record) {
        var versionMapMain = [];
        versionMapMain[kony.sync.mainTableChangeTypeColumn] = kony.sync.getChangeTypeForUploadTrue(record[kony.sync.historyTableChangeTypeColumn]);
        var scopename = kony.sync.scopes.syncTableScopeDic[tbname];
        var versionNo = kony.sync.getseqnumber(tx, scopename);
        if (versionNo === false) {
            return false;
        }
        versionMapMain[kony.sync.historyTableSyncVersionColumn] = versionNo.versionnumber;
        var query = kony.sync.qb_createQuery();
        kony.sync.qb_update(query, tbname);
        kony.sync.qb_set(query, versionMapMain);
        var query_compile = kony.sync.qb_compile(query);
        var sql = query_compile[0] + " " + wcs;
        var params = query_compile[1];
        if (kony.sync.executeSql(tx, sql, params) === false) {
            return false;
        }
    }

    function markRecordForUploadHistory(tx, record) {
        var versionMap = [];
        versionMap[kony.sync.historyTableChangeTypeColumn] = kony.sync.getChangeTypeForUploadTrue(record[kony.sync.historyTableChangeTypeColumn]);
        var scopename = kony.sync.scopes.syncTableScopeDic[tbname];
        var versionNo = kony.sync.getseqnumber(tx, scopename);
        if (versionNo === false) {
            return false;
        }
        var twcs = "";
        twcs = wcs;
        twcs = twcs + " AND " + kony.sync.historyTableChangeTypeColumn + " = " + record[kony.sync.historyTableChangeTypeColumn];
        versionMap[kony.sync.historyTableSyncVersionColumn] = versionNo.versionnumber;
        var query = kony.sync.qb_createQuery();
        kony.sync.qb_update(query, tbname + kony.sync.historyTableName);
        kony.sync.qb_set(query, versionMap);
        var query_compile = kony.sync.qb_compile(query);
        var sql = query_compile[0] + " " + twcs;
        var params = query_compile[1];
        if (kony.sync.executeSql(tx, sql, params) === false) {
            return false;
        }
    }

    function single_transaction_callback(tx) {
        sync.log.trace("Entering VCScope.vcunbonnet.markForUpload->single_transaction_callback");
        //updating main table
        var sql = "select " + kony.sync.historyTableChangeTypeColumn + " from \"" + tbname + "\" " + wcs;
        var resultSet = kony.sync.executeSql(tx, sql, null);
        if (resultSet === false) {
            isError = true;
            return;
        }
        num_records_main = resultSet.rows.length;
        for (var i = 0; i < num_records_main; i++) {
            var record = kony.db.sqlResultsetRowItem(tx, resultSet, i);
            if (markRecordForUpload(tx, record) === false) {
                isError = true;
                return;
            }
        }
        //updating history table
        var sql = "select " + kony.sync.historyTableChangeTypeColumn + " from " + tbname + kony.sync.historyTableName + " " + wcs;
        var resultSet = kony.sync.executeSql(tx, sql, null);
        if (resultSet === false) {
            isError = true;
            return;
        }
        var num_records = resultSet.rows.length;
        for (var i = 0; i <= num_records - 1; i++) {
            var record = kony.db.sqlResultsetRowItem(tx, resultSet, i);
            if (markRecordForUploadHistory(tx, record) === false) {
                isError = true;
                return;
            }
        }
    }

    function single_transaction_success_callback() {
        sync.log.trace("Entering VCScope.vcunbonnet.markForUpload->single_transaction_success_callback");
        kony.sync.verifyAndCallClosure(successcallback, {
            count: num_records_main
        });
    }

    function single_transaction_error_callback() {
        sync.log.error("Entering VCScope.vcunbonnet.markForUpload->single_transaction_error_callback");
        if (!isError) {
            kony.sync.showTransactionError(errorcallback);
        } else {
            kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
            kony.sync.errorObject = null;
        }
    }
    var connection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
    if (connection != null) {
        kony.sync.startTransaction(connection, single_transaction_callback, single_transaction_success_callback, single_transaction_error_callback);
    }
};
/************************************************************************************
 * Retrieves instance(s) of vcunbonnet pending for upload. Records are marked for
 * pending upload if they have been updated or created locally and the changes have
 * not been merged with enterprise datasource.
 *************************************************************************************/
VCScope.vcunbonnet.getPendingUpload = function(wcs, successcallback, errorcallback) {
    sync.log.trace("Entering VCScope.vcunbonnet.getPendingUpload->main function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = VCScope.vcunbonnet.getTableName();
    var currentversion = kony.sync.getCurrentVersionNumber(tbname);
    var sql;
    if (typeof(wcs) === "string" && wcs != null) {
        wcs = kony.sync.validateWhereClause(wcs);
        sql = "select * from \"" + tbname + "\" " + wcs + " and " + kony.sync.mainTableChangeTypeColumn + " is not null AND " + kony.sync.mainTableChangeTypeColumn + " <> -1 AND " + kony.sync.mainTableSyncVersionColumn + " = " + currentversion + " AND " + kony.sync.mainTableChangeTypeColumn + " NOT LIKE '9%'";
    } else {
        errorcallback = successcallback;
        successcallback = wcs;
        sql = "select * from \"" + tbname + "\" WHERE " + kony.sync.mainTableChangeTypeColumn + " is not null AND " + kony.sync.mainTableChangeTypeColumn + " <> -1 AND " + kony.sync.mainTableSyncVersionColumn + " = " + currentversion + " AND " + kony.sync.mainTableChangeTypeColumn + " NOT LIKE '9%'";
    }
    kony.sync.single_select_execute(dbname, sql, null, mySuccesscallback, errorcallback);

    function mySuccesscallback(res) {
        sync.log.trace("Entering VCScope.vcunbonnet.getPendingUpload->successcallback function");
        kony.sync.verifyAndCallClosure(successcallback, VCScope.vcunbonnet.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
    }
};
/************************************************************************************
 * Retrieves instance(s) of vcunbonnet pending for acknowledgement. This is relevant
 * when the SyncObject is part of the SyncScope whose SyncStrategy is PersistentSync.
 * In persistent Sync the  records in the local database are put into a pending 
 * acknowledgement state after an upload.
 *************************************************************************************/
VCScope.vcunbonnet.getPendingAcknowledgement = function(successcallback, errorcallback) {
    sync.log.trace("Entering VCScope.vcunbonnet.getPendingAcknowledgement->main function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = VCScope.vcunbonnet.getTableName();
    var currentversion = kony.sync.getCurrentVersionNumber(tbname);
    var mysql = "select * from \"" + tbname + "\" WHERE " + kony.sync.mainTableChangeTypeColumn + " is not null AND " + kony.sync.mainTableChangeTypeColumn + " <> -1 AND " + kony.sync.mainTableSyncVersionColumn + " <> " + currentversion + " AND " + kony.sync.mainTableChangeTypeColumn + " NOT LIKE '9%'";
    kony.sync.single_select_execute(dbname, mysql, null, mySuccesscallback, errorcallback);

    function mySuccesscallback(res) {
        sync.log.trace("Entering VCScope.vcunbonnet.getPendingAcknowledgement success callback function");
        kony.sync.verifyAndCallClosure(successcallback, VCScope.vcunbonnet.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
    }
};
/************************************************************************************
 * Retrieves instance(s) of vcunbonnet deferred for upload.
 *************************************************************************************/
VCScope.vcunbonnet.getDeferredUpload = function(wcs, successcallback, errorcallback) {
    sync.log.trace("Entering VCScope.vcunbonnet.getDeferredUpload->main function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = VCScope.vcunbonnet.getTableName();
    var sql;
    if (typeof(wcs) === "string" && wcs != null) {
        wcs = kony.sync.validateWhereClause(wcs);
        sql = "select * from \"" + tbname + "\" " + wcs + " and " + kony.sync.mainTableChangeTypeColumn + " is not null AND " + kony.sync.mainTableChangeTypeColumn + " <> -1 AND " + kony.sync.mainTableChangeTypeColumn + " LIKE '9%'";
    } else {
        errorcallback = successcallback;
        successcallback = wcs;
        sql = "select * from \"" + tbname + "\" WHERE " + kony.sync.mainTableChangeTypeColumn + " is not null AND " + kony.sync.mainTableChangeTypeColumn + " <> -1 AND " + kony.sync.mainTableChangeTypeColumn + " LIKE '9%'";
    }
    kony.sync.single_select_execute(dbname, sql, null, mySuccesscallback, errorcallback);

    function mySuccesscallback(res) {
        sync.log.trace("Entering VCScope.vcunbonnet.getDeferredUpload->success callback function");
        kony.sync.verifyAndCallClosure(successcallback, VCScope.vcunbonnet.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
    }
};
/************************************************************************************
 * Rollbacks all changes to vcunbonnet in local database to last synced state
 *************************************************************************************/
VCScope.vcunbonnet.rollbackPendingLocalChanges = function(successcallback, errorcallback) {
    sync.log.trace("Entering VCScope.vcunbonnet.rollbackPendingLocalChanges->main function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = VCScope.vcunbonnet.getTableName();
    kony.sync.konySyncRollBackPendingChanges(tbname, dbname, null, mySuccesscallback, errorcallback);

    function mySuccesscallback(res) {
        sync.log.trace("Entering VCScope.vcunbonnet.rollbackPendingLocalChanges->main function");
        kony.sync.verifyAndCallClosure(successcallback, res);
    }
};
/************************************************************************************
 * Rollbacks changes to vcunbonnet's record with given primary key in local 
 * database to last synced state
 *************************************************************************************/
VCScope.vcunbonnet.prototype.rollbackPendingLocalChangesByPK = function(successcallback, errorcallback) {
    sync.log.trace("Entering VCScope.vcunbonnet.prototype.rollbackPendingLocalChangesByPK function");
    var pks = this.getPKTable();
    VCScope.vcunbonnet.rollbackPendingLocalChangesByPK(pks, successcallback, errorcallback);
};
VCScope.vcunbonnet.rollbackPendingLocalChangesByPK = function(pks, successcallback, errorcallback) {
    sync.log.trace("Entering VCScope.vcunbonnet.rollbackPendingLocalChangesByPK->main function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    if (!kony.sync.validateInput(arguments, "VCScope.vcunbonnet.rollbackPendingLocalChangesByPK", "rollbackPendingLocalChangesByPK", errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = VCScope.vcunbonnet.getTableName();
    var wcs = [];
    if (VCScope.vcunbonnet.pkCheck(pks, wcs, errorcallback, "rollbacking") === false) {
        return;
    }
    kony.sync.konySyncRollBackPendingChanges(tbname, dbname, wcs, mySuccesscallback, pkNotFoundErrCallback);

    function mySuccesscallback(res) {
        sync.log.trace("Entering VCScope.vcunbonnet.rollbackPendingLocalChangesByPK->success callback function");
        kony.sync.verifyAndCallClosure(successcallback, res);
    }

    function pkNotFoundErrCallback() {
        sync.log.error("Entering VCScope.vcunbonnet.rollbackPendingLocalChangesByPK->PK not found callback");
        kony.sync.pkNotFoundErrCallback(errorcallback, tbname);
    }
};
/************************************************************************************
 * isRecordDeferredForUpload returns true or false depending on whether vcunbonnet's record  
 * with given primary key got deferred in last sync
 *************************************************************************************/
VCScope.vcunbonnet.prototype.isRecordDeferredForUpload = function(successcallback, errorcallback) {
    sync.log.trace("Entering  VCScope.vcunbonnet.prototype.isRecordDeferredForUpload function");
    var pks = this.getPKTable();
    VCScope.vcunbonnet.isRecordDeferredForUpload(pks, successcallback, errorcallback);
};
VCScope.vcunbonnet.isRecordDeferredForUpload = function(pks, successcallback, errorcallback) {
    sync.log.trace("Entering VCScope.vcunbonnet.isRecordDeferredForUpload->main function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    if (!kony.sync.validateInput(arguments, "VCScope.vcunbonnet.isRecordDeferredForUpload", "isRecordDeferredForUpload", errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = VCScope.vcunbonnet.getTableName();
    var wcs = [];
    var flag;
    if (VCScope.vcunbonnet.pkCheck(pks, wcs, errorcallback, "selecting") === false) {
        return;
    }
    var twcs = [];
    twcs = kony.sync.CreateCopy(wcs);
    kony.table.insert(twcs, {
        key: kony.sync.mainTableChangeTypeColumn,
        value: "9%",
        optype: "LIKE",
        comptype: "AND"
    });
    var query = kony.sync.qb_createQuery();
    kony.sync.qb_select(query, null);
    kony.sync.qb_from(query, tbname);
    kony.sync.qb_where(query, twcs);
    var query_compile = kony.sync.qb_compile(query);
    var sql = query_compile[0];
    var params = query_compile[1];
    kony.sync.single_select_execute(dbname, sql, params, mySuccesscallback, errorcallback);

    function mySuccesscallback(res) {
        sync.log.trace("Entering VCScope.vcunbonnet.isRecordDeferredForUpload->successcallback function");
        if (res.length === 1) {
            flag = true;
        } else {
            flag = false;
        }
        kony.sync.verifyAndCallClosure(successcallback, {
            deferred: flag
        });
    }
};
/************************************************************************************
 * isRecordPendingForUpload returns true or false depending on whether vcunbonnet's record  
 * with given primary key is pending for upload
 *************************************************************************************/
VCScope.vcunbonnet.prototype.isRecordPendingForUpload = function(successcallback, errorcallback) {
    sync.log.trace("Entering  VCScope.vcunbonnet.prototype.isRecordPendingForUpload function");
    var pks = this.getPKTable();
    VCScope.vcunbonnet.isRecordPendingForUpload(pks, successcallback, errorcallback);
};
VCScope.vcunbonnet.isRecordPendingForUpload = function(pks, successcallback, errorcallback) {
    sync.log.trace("Entering VCScope.vcunbonnet.isRecordPendingForUpload->main function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    if (!kony.sync.validateInput(arguments, "VCScope.vcunbonnet.isRecordPendingForUpload", "isRecordPendingForUpload", errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = VCScope.vcunbonnet.getTableName();
    var wcs = [];
    var flag;
    if (VCScope.vcunbonnet.pkCheck(pks, wcs, errorcallback, "selecting") === false) {
        return;
    }
    var twcs = [];
    twcs = kony.sync.CreateCopy(wcs);
    kony.table.insert(twcs, {
        key: kony.sync.mainTableChangeTypeColumn,
        value: "9%",
        optype: "NOT LIKE",
        comptype: "AND"
    });
    var query = kony.sync.qb_createQuery();
    kony.sync.qb_select(query, null);
    kony.sync.qb_from(query, tbname);
    kony.sync.qb_where(query, twcs);
    var query_compile = kony.sync.qb_compile(query);
    var sql = query_compile[0];
    var params = query_compile[1];
    kony.sync.single_select_execute(dbname, sql, params, mySuccesscallback, errorcallback);

    function mySuccesscallback(res) {
        sync.log.trace("Entering VCScope.vcunbonnet.isRecordPendingForUpload->successcallback function");
        if (res.length === 1) {
            flag = true;
        } else {
            flag = false;
        }
        kony.sync.verifyAndCallClosure(successcallback, {
            pending: flag
        });
    }
};
/************************************************************************************
 * Start of helper functions used internally, not to be used as ORMs
 *************************************************************************************/
//Deletes all the dependant tables in the relationship tables.Need to pass transaction handler as input
VCScope.vcunbonnet.removeCascade = function(tx, wcs, errorcallback, markForUpload, isCascade, parentTable, isLocal) {
    sync.log.trace("Entering VCScope.vcunbonnet.removeCascade function");
    var tbname = VCScope.vcunbonnet.getTableName();
    markForUpload = kony.sync.getUploadStatus(markForUpload);

    function removeCascadeChildren() {}
    if (isCascade) {
        if (removeCascadeChildren() === false) {
            return false;
        }
        if (kony.sync.deleteBatch(tx, tbname, wcs, isLocal, markForUpload, null) === false) {
            return false;
        }
        return true;
    } else {
        var sql = "select * from \"" + tbname + "\" " + wcs;
        var resultSet = kony.sync.executeSql(tx, sql, null);
        if (resultSet === false) {
            return false;
        }
        var num_records = resultSet.rows.length;
        if (num_records === 0) {
            return true;
        } else {
            sync.log.error(kony.sync.getReferetialIntegrityDeleteErrMessg(tbname, tbname, tbname, parentTable));
            errorcallback(kony.sync.getErrorTable(kony.sync.errorCodeReferentialIntegrity, kony.sync.getReferetialIntegrityDeleteErrMessg(tbname, tbname, tbname, parentTable)));
            return false;
        }
    }
};
VCScope.vcunbonnet.convertTableToObject = function(res) {
    sync.log.trace("Entering VCScope.vcunbonnet.convertTableToObject function");
    objMap = [];
    if (res !== null) {
        for (var i in res) {
            var obj = new VCScope.vcunbonnet();
            obj.unbid = res[i].unbid;
            obj.regno = res[i].regno;
            obj.email = res[i].email;
            obj.OilCheck = res[i].OilCheck;
            obj.CoolantLevel = res[i].CoolantLevel;
            obj.WasherBottle = res[i].WasherBottle;
            obj.BrakeFluid = res[i].BrakeFluid;
            obj.isDeleted = res[i].isDeleted;
            obj.lastModifiedDate = res[i].lastModifiedDate;
            obj.markForUpload = (Math.floor(res[i].konysyncchangetype / 10) == 9) ? false : true;
            objMap[i] = obj;
        }
    }
    return objMap;
};
VCScope.vcunbonnet.filterAttributes = function(valuestable, insert) {
    sync.log.trace("Entering VCScope.vcunbonnet.filterAttributes function");
    var attributeTable = {};
    attributeTable.unbid = "unbid";
    attributeTable.regno = "regno";
    attributeTable.email = "email";
    attributeTable.OilCheck = "OilCheck";
    attributeTable.CoolantLevel = "CoolantLevel";
    attributeTable.WasherBottle = "WasherBottle";
    attributeTable.BrakeFluid = "BrakeFluid";
    var PKTable = {};
    PKTable.unbid = {}
    PKTable.unbid.name = "unbid";
    PKTable.unbid.isAutoGen = true;
    var newvaluestable = {};
    for (var k in valuestable) {
        var v = valuestable[k];
        if (kony.sync.isNull(attributeTable[k])) {
            sync.log.warn("Ignoring the attribute " + k + " for the SyncObject vcunbonnet. " + k + " not defined as an attribute in SyncConfiguration.");
        } else if (!kony.sync.isNull(PKTable[k])) {
            if (insert === false) {
                sync.log.warn("Ignoring the primary key " + k + " for the SyncObject vcunbonnet. Primary Key should not be the part of the attributes to be updated in the local device database.");
            } else if (PKTable[k].isAutoGen) {
                sync.log.warn("Ignoring the auto-generated primary key " + k + " for the SyncObject vcunbonnet. Auto-generated Primary Key should not be the part of the attributes to be inserted in the local device database.");
            } else {
                newvaluestable[k] = v;
            }
        } else {
            newvaluestable[k] = v;
        }
    }
    return newvaluestable;
};
VCScope.vcunbonnet.formOrderByClause = function(orderByMap) {
    sync.log.trace("Entering VCScope.vcunbonnet.formOrderByClause function");
    if (!kony.sync.isNull(orderByMap)) {
        var valuestable = kony.sync.convertOrderByMapToValuesTable(orderByMap);
        //var filteredValuestable = VCScope.vcunbonnet.filterAttributes(valuestable, true);
        return kony.sync.convertToValuesTableOrderByMap(orderByMap, valuestable);
    }
    return null;
};
VCScope.vcunbonnet.prototype.getValuesTable = function(isInsert) {
    sync.log.trace("Entering VCScope.vcunbonnet.prototype.getValuesTable function");
    var valuesTable = {};
    if (isInsert === true) {
        valuesTable.unbid = this.unbid;
    }
    valuesTable.regno = this.regno;
    valuesTable.email = this.email;
    valuesTable.OilCheck = this.OilCheck;
    valuesTable.CoolantLevel = this.CoolantLevel;
    valuesTable.WasherBottle = this.WasherBottle;
    valuesTable.BrakeFluid = this.BrakeFluid;
    return valuesTable;
};
VCScope.vcunbonnet.prototype.getPKTable = function() {
    sync.log.trace("Entering VCScope.vcunbonnet.prototype.getPKTable function");
    var pkTable = {};
    pkTable.unbid = {
        key: "unbid",
        value: this.unbid
    };
    return pkTable;
};
VCScope.vcunbonnet.getPKTable = function() {
    sync.log.trace("Entering VCScope.vcunbonnet.getPKTable function");
    var pkTable = [];
    pkTable.push("unbid");
    return pkTable;
};
VCScope.vcunbonnet.pkCheck = function(pks, wcs, errorcallback, opName) {
    sync.log.trace("Entering VCScope.vcunbonnet.pkCheck function");
    var wc = [];
    if (kony.sync.isNull(pks)) {
        sync.log.error("Primary Key unbid not specified in  " + opName + "  an item in vcunbonnet");
        kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodePrimaryKeyNotSpecified, kony.sync.getPrimaryKeyNotSpecifiedMsg("unbid", opName, "vcunbonnet")));
        return false;
    } else if (kony.sync.isValidJSTable(pks)) {
        if (!kony.sync.isNull(pks.unbid)) {
            if (!kony.sync.isNull(pks.unbid.value)) {
                wc.key = "unbid";
                wc.value = pks.unbid.value;
            } else {
                wc.key = "unbid";
                wc.value = pks.unbid;
            }
        } else {
            sync.log.error("Primary Key unbid not specified in  " + opName + "  an item in vcunbonnet");
            kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodePrimaryKeyNotSpecified, kony.sync.getPrimaryKeyNotSpecifiedMsg("unbid", opName, "vcunbonnet")));
            return false;
        }
    } else {
        wc.key = "unbid";
        wc.value = pks;
    }
    kony.table.insert(wcs, wc);
    return true;
};
VCScope.vcunbonnet.validateNull = function(valuestable, errorcallback) {
    sync.log.trace("Entering VCScope.vcunbonnet.validateNull function");
    return true;
};
VCScope.vcunbonnet.validateNullInsert = function(valuestable, errorcallback) {
    sync.log.trace("Entering VCScope.vcunbonnet.validateNullInsert function");
    return true;
};
VCScope.vcunbonnet.getRelationshipMap = function(relationshipMap, valuestable) {
    sync.log.trace("Entering VCScope.vcunbonnet.getRelationshipMap function");
    var r1 = {};
    return relationshipMap;
};
VCScope.vcunbonnet.checkPKValueTables = function(valuetables) {
    var checkPksNotNullFlag = true;
    for (var i = 0; i < valuetables.length; i++) {
        if (kony.sync.isNull(valuetables[i])) {
            checkPksNotNullFlag = false;
            break;
        }
    }
    return checkPksNotNullFlag;
};
VCScope.vcunbonnet.getTableName = function() {
    return "vcunbonnet";
};
// **********************************End vcunbonnet's helper methods************************