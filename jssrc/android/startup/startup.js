//startup.js file
var globalhttpheaders = {};
var appConfig = {
    appId: "VehicleChecks",
    appName: "Vehicle Check",
    appVersion: "1.0",
    platformVersion: null,
    serverIp: "10.44.215.96",
    serverPort: "80",
    secureServerPort: "443",
    isDebug: false,
    middlewareContext: "VehicleChecks",
    isturlbase: "https://rentokil-initial.konycloud.com/services",
    isMFApp: true,
    appKey: "aae866ac2424dba8e54c06df5ce80cdc",
    appSecret: "77918377f7d455ee27387f708833ded7",
    serviceUrl: "https://100004898.auth.konycloud.com/appconfig",
    svcDoc: {
        "appId": "b236b97f-972d-44a7-af2b-98d3957157d6",
        "baseId": "461ca65a-2e2e-468d-9dfe-381134a710be",
        "name": "VehicleCheckProd",
        "selflink": "https://100004898.auth.konycloud.com/appconfig",
        "integsvc": {
            "getEmployee": "https://rentokil-initial.konycloud.com/services/getEmployee"
        },
        "sync": {
            "appId": "100004898461ca65a",
            "url": "https://rentokil-initial.sync.konycloud.com/syncservice/api/v1/100004898461ca65a"
        },
        "reportingsvc": {
            "custom": "https://rentokil-initial.konycloud.com/services/CMS",
            "session": "https://rentokil-initial.konycloud.com/services/IST"
        },
        "services_meta": {
            "getEmployee": {
                "version": "1.0",
                "url": "https://rentokil-initial.konycloud.com/services/getEmployee",
                "type": "integsvc"
            }
        }
    },
    svcDocRefresh: false,
    svcDocRefreshTimeSecs: -1,
    eventTypes: ["FormEntry", "FormExit", "Touch", "Gesture", "Orientation", "ServiceRequest", "ServiceResponse", "Error", "Crash"],
    url: "https://rentokil-initial.konycloud.com/VehicleChecks/MWServlet",
    secureurl: "https://rentokil-initial.konycloud.com/VehicleChecks/MWServlet"
};
sessionID = "";

function appInit(params) {
    skinsInit();
    initializetplRowSegSelectionList();
    initializetplSegVCheckRow();
    initializetplTempSegRow();
    initializetplTempSegRowDup();
    initializetplTempSegRowEmp();
    ContentGlobals();
    HomeGlobals();
    LoginGlobals();
    PopupConfirmationGlobals();
    PopupMessageGlobals();
    PopupProgressGlobals();
    setAppBehaviors();
};

function setAppBehaviors() {
    kony.application.setApplicationBehaviors({
        applyMarginPaddingInBCGMode: false,
        adherePercentageStrictly: true,
        retainSpaceOnHide: true,
        marginsIncludedInWidgetContainerWeight: true,
        APILevel: 7000
    })
};

function themeCallBack() {
    callAppMenu();
    initializeGlobalVariables();
    kony.application.setApplicationInitializationEvents({
        preappinit: AS_AppEvents_94d038b0047f47d4ad7fa4cbf55d9cf9,
        init: appInit,
        postappinit: AS_AppEvents_c7f27d4c66c2405cb49d5be262d68762,
        showstartupform: function() {
            Login.show();
        }
    });
};

function loadResources() {
    globalhttpheaders = {};
    kony.os.loadLibrary({
        "javaclassname": "com.konylabs.ffi.N_EmailChecker"
    });
    kony.os.loadLibrary({
        "javaclassname": "com.konylabs.ffi.N_NFCNoReader"
    });
    sdkInitConfig = {
        "appConfig": appConfig,
        "isMFApp": appConfig.isMFApp,
        "appKey": appConfig.appKey,
        "appSecret": appConfig.appSecret,
        "eventTypes": appConfig.eventTypes,
        "serviceUrl": appConfig.serviceUrl
    }
    kony.setupsdks(sdkInitConfig, onSuccessSDKCallBack, onSuccessSDKCallBack);
};

function onSuccessSDKCallBack() {
    //This is to define sync global variable if application has sync
    var mbaasSdkObj = kony.sdk && kony.sdk.getCurrentInstance();
    if (mbaasSdkObj.getSyncService()) {
        sync = mbaasSdkObj.getSyncService();
    }
    kony.theme.setCurrentTheme("default", themeCallBack, themeCallBack);
}

function onSuccess(oldlocalname, newlocalename, info) {
    loadResources();
};

function onFailure(errorcode, errormsg, info) {
    loadResources();
};
kony.application.setApplicationMode(constants.APPLICATION_MODE_NATIVE);
//If default locale is specified. This is set even before any other app life cycle event is called.
kony.i18n.setDefaultLocaleAsync("en_GB", onSuccess, onFailure, null);