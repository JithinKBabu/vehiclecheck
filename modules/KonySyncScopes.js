if(typeof(VCScope)=== "undefined"){ VCScope = {}; }
if(typeof(VCScope.VCScope)=== "undefined"){ VCScope.VCScope = {}; }

//API call will trigger VCScope reset
VCScope.VCScope.reset = function(successcallback,errorcallback){
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	kony.sync.scopeReset("VCScope",successcallback,errorcallback);
}



if(typeof(EmployeeScope)=== "undefined"){ EmployeeScope = {}; }

//API call will trigger EmployeeScope reset
EmployeeScope.reset = function(successcallback,errorcallback){
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	kony.sync.scopeReset("EmployeeScope",successcallback,errorcallback);
}




// **********************************Start Scope definition************************
konysyncClientSyncConfig ={
     "ArrayOfSyncScope": [
          {
               "DataSource": "265105",
               "ScopeName": "VCScope",
               "ScopeDatabaseName": "100004898461ca65a",
               "Strategy": "OTA_SYNC",
               "ScopeTables": [
                    {
                         "Name": "vcincab",
                         "Columns": [
                              {
                                   "Name": "cabid",
                                   "Autogenerated": "true",
                                   "IsPrimaryKey": true,
                                   "Length": 10,
                                   "IsNullable": false,
                                   "type": "integer"
                              },
                              {
                                   "Name": "regno",
                                   "Autogenerated": "false",
                                   "Length": 20,
                                   "IsNullable": true,
                                   "type": "string"
                              },
                              {
                                   "Name": "email",
                                   "Autogenerated": "false",
                                   "Length": 50,
                                   "IsNullable": true,
                                   "type": "string"
                              },
                              {
                                   "Name": "fuel",
                                   "Autogenerated": "false",
                                   "Length": 50,
                                   "IsNullable": true,
                                   "type": "string"
                              },
                              {
                                   "Name": "Mirrors",
                                   "Autogenerated": "false",
                                   "Length": 10,
                                   "IsNullable": true,
                                   "type": "string"
                              },
                              {
                                   "Name": "Horn",
                                   "Autogenerated": "false",
                                   "Length": 10,
                                   "IsNullable": true,
                                   "type": "string"
                              },
                              {
                                   "Name": "Indicators",
                                   "Autogenerated": "false",
                                   "Length": 10,
                                   "IsNullable": true,
                                   "type": "string"
                              },
                              {
                                   "Name": "GroupLightsA",
                                   "Autogenerated": "false",
                                   "Length": 10,
                                   "IsNullable": true,
                                   "type": "string"
                              },
                              {
                                   "Name": "GroupLightsB",
                                   "Autogenerated": "false",
                                   "Length": 10,
                                   "IsNullable": true,
                                   "type": "string"
                              },
                              {
                                   "Name": "GroupLightsC",
                                   "Autogenerated": "false",
                                   "Length": 10,
                                   "IsNullable": true,
                                   "type": "string"
                              },
                              {
                                   "Name": "Windscreen",
                                   "Autogenerated": "false",
                                   "Length": 10,
                                   "IsNullable": true,
                                   "type": "string"
                              },
                              {
                                   "Name": "Tachograph",
                                   "Autogenerated": "false",
                                   "Length": 10,
                                   "IsNullable": true,
                                   "type": "string"
                              },
                              {
                                   "Name": "Brakes",
                                   "Autogenerated": "false",
                                   "Length": 10,
                                   "IsNullable": true,
                                   "type": "string"
                              },
                              {
                                   "Name": "Wipers",
                                   "Autogenerated": "false",
                                   "Length": 10,
                                   "IsNullable": true,
                                   "type": "string"
                              },
                              {
                                   "Name": "Steering",
                                   "Autogenerated": "false",
                                   "Length": 10,
                                   "IsNullable": true,
                                   "type": "string"
                              },
                              {
                                   "Name": "GeneralSafety",
                                   "Autogenerated": "false",
                                   "Length": 10,
                                   "IsNullable": true,
                                   "type": "string"
                              }
                         ],
                         "Pk_Columns": ["cabid"],
                         "Relationships": {}
                    },
                    {
                         "Name": "vcdetails",
                         "Columns": [
                              {
                                   "Name": "vcId",
                                   "Autogenerated": "true",
                                   "IsPrimaryKey": true,
                                   "Length": 10,
                                   "IsNullable": false,
                                   "type": "integer"
                              },
                              {
                                   "Name": "regno",
                                   "Autogenerated": "false",
                                   "Length": 20,
                                   "IsNullable": true,
                                   "type": "string"
                              },
                              {
                                   "Name": "email",
                                   "Autogenerated": "false",
                                   "Length": 50,
                                   "IsNullable": true,
                                   "type": "string"
                              },
                              {
                                   "Name": "location",
                                   "Autogenerated": "false",
                                   "Length": 150,
                                   "IsNullable": true,
                                   "type": "string"
                              },
                              {
                                   "Name": "dateField",
                                   "Autogenerated": "false",
                                   "Length": 100,
                                   "IsNullable": true,
                                   "type": "string"
                              },
                              {
                                   "Name": "image",
                                   "Autogenerated": "false",
                                   "Length": 2147483647,
                                   "IsNullable": true,
                                   "type": "image"
                              },
                              {
                                   "Name": "latitude",
                                   "Autogenerated": "false",
                                   "Length": 80,
                                   "IsNullable": true,
                                   "type": "string"
                              },
                              {
                                   "Name": "longitude",
                                   "Autogenerated": "false",
                                   "Length": 80,
                                   "IsNullable": true,
                                   "type": "string"
                              },
                              {
                                   "Name": "accuracy",
                                   "Autogenerated": "false",
                                   "Length": 80,
                                   "IsNullable": true,
                                   "type": "string"
                              },
                              {
                                   "Name": "timestampField",
                                   "Autogenerated": "false",
                                   "Length": 100,
                                   "IsNullable": true,
                                   "type": "string"
                              },
                              {
                                   "Name": "mileage",
                                   "Autogenerated": "false",
                                   "Length": 10,
                                   "IsNullable": true,
                                   "type": "string"
                              },
                              {
                                   "Name": "branchNumber",
                                   "Autogenerated": "false",
                                   "Length": 100,
                                   "IsNullable": true,
                                   "type": "string"
                              }
                         ],
                         "Pk_Columns": ["vcId"],
                         "Relationships": {}
                    },
                    {
                         "Name": "vcunbonnet",
                         "Columns": [
                              {
                                   "Name": "unbid",
                                   "Autogenerated": "true",
                                   "IsPrimaryKey": true,
                                   "Length": 10,
                                   "IsNullable": false,
                                   "type": "integer"
                              },
                              {
                                   "Name": "regno",
                                   "Autogenerated": "false",
                                   "Length": 20,
                                   "IsNullable": true,
                                   "type": "string"
                              },
                              {
                                   "Name": "email",
                                   "Autogenerated": "false",
                                   "Length": 50,
                                   "IsNullable": true,
                                   "type": "string"
                              },
                              {
                                   "Name": "OilCheck",
                                   "Autogenerated": "false",
                                   "Length": 10,
                                   "IsNullable": true,
                                   "type": "string"
                              },
                              {
                                   "Name": "CoolantLevel",
                                   "Autogenerated": "false",
                                   "Length": 10,
                                   "IsNullable": true,
                                   "type": "string"
                              },
                              {
                                   "Name": "WasherBottle",
                                   "Autogenerated": "false",
                                   "Length": 10,
                                   "IsNullable": true,
                                   "type": "string"
                              },
                              {
                                   "Name": "BrakeFluid",
                                   "Autogenerated": "false",
                                   "Length": 10,
                                   "IsNullable": true,
                                   "type": "string"
                              }
                         ],
                         "Pk_Columns": ["unbid"],
                         "Relationships": {}
                    },
                    {
                         "Name": "vcexterior",
                         "Columns": [
                              {
                                   "Name": "extid",
                                   "Autogenerated": "true",
                                   "IsPrimaryKey": true,
                                   "Length": 10,
                                   "IsNullable": false,
                                   "type": "integer"
                              },
                              {
                                   "Name": "regno",
                                   "Autogenerated": "false",
                                   "Length": 20,
                                   "IsNullable": true,
                                   "type": "string"
                              },
                              {
                                   "Name": "email",
                                   "Autogenerated": "false",
                                   "Length": 50,
                                   "IsNullable": true,
                                   "type": "string"
                              },
                              {
                                   "Name": "TaxDisc",
                                   "Autogenerated": "false",
                                   "Length": 10,
                                   "IsNullable": true,
                                   "type": "string"
                              },
                              {
                                   "Name": "TyreCondition",
                                   "Autogenerated": "false",
                                   "Length": 10,
                                   "IsNullable": true,
                                   "type": "string"
                              },
                              {
                                   "Name": "WheelNuts",
                                   "Autogenerated": "false",
                                   "Length": 10,
                                   "IsNullable": true,
                                   "type": "string"
                              },
                              {
                                   "Name": "NumberPlate",
                                   "Autogenerated": "false",
                                   "Length": 10,
                                   "IsNullable": true,
                                   "type": "string"
                              },
                              {
                                   "Name": "BodyCondition",
                                   "Autogenerated": "false",
                                   "Length": 10,
                                   "IsNullable": true,
                                   "type": "string"
                              },
                              {
                                   "Name": "SideStep",
                                   "Autogenerated": "false",
                                   "Length": 10,
                                   "IsNullable": true,
                                   "type": "string"
                              }
                         ],
                         "Pk_Columns": ["extid"],
                         "Relationships": {}
                    }
               ]
          },
          {
               "DataSource": "c8bb1817-ef0d-43f5-b35b-47a23d047972",
               "ScopeName": "EmployeeScope",
               "ScopeDatabaseName": "100004898461ca65a",
               "Strategy": "OTA_SYNC",
               "ScopeTables": [{
                    "Name": "employeedetails",
                    "Columns": [
                         {
                              "Name": "branchNumber",
                              "Autogenerated": "false",
                              "IsNullable": true,
                              "type": "string"
                         },
                         {
                              "Name": "businessCode",
                              "Autogenerated": "false",
                              "IsPrimaryKey": true,
                              "IsNullable": true,
                              "type": "string"
                         },
                         {
                              "Name": "countryCode",
                              "Autogenerated": "false",
                              "IsPrimaryKey": true,
                              "IsNullable": true,
                              "type": "string"
                         },
                         {
                              "Name": "email",
                              "Autogenerated": "false",
                              "IsPrimaryKey": true,
                              "IsNullable": true,
                              "type": "string"
                         },
                         {
                              "Name": "employeeCode",
                              "Autogenerated": "false",
                              "IsNullable": true,
                              "type": "string"
                         },
                         {
                              "Name": "firstName",
                              "Autogenerated": "false",
                              "IsNullable": true,
                              "type": "string"
                         },
                         {
                              "Name": "isError",
                              "Autogenerated": "false",
                              "IsNullable": true,
                              "type": "string"
                         },
                         {
                              "Name": "languageCode",
                              "Autogenerated": "false",
                              "IsNullable": true,
                              "type": "string"
                         },
                         {
                              "Name": "lastName",
                              "Autogenerated": "false",
                              "IsNullable": true,
                              "type": "string"
                         }
                    ],
                    "Pk_Columns": [
                         "businessCode",
                         "countryCode",
                         "email"
                    ],
                    "Relationships": {}
               }]
          }
     ],
     "ArrayOfDataSource": [
          {
               "Database": {
                    "Name": "265105",
                    "Type": "MYSQL"
               },
               "ID": "265105",
               "type": "DATABASE"
          },
          {
               "ID": "c8bb1817-ef0d-43f5-b35b-47a23d047972",
               "type": "JSON"
          }
     ],
     "AppID": "100004898461ca65a",
     "Version": "922bf018a90dc84ec0735e74d1b5fd616de2a9b4509f930bdf50d42722aef3e1"
}
//**********************************End Scope definition************************
