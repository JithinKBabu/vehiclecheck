/**********************************************************************
*	Author  : RI - CTS363601 
*	Purpose : Module to refer all class definitions
***********************************************************************/

/** Class definition - User/Employee **/
function Employee(){
  	this.empId						= 0;
  	this.employeeCode				= "0";
	this.firstName					= "--NA--";
	this.lastName					= "--NA--";
	this.email						= "";
  	this.countryCode				= "UK";
  	this.businessCode				= "R";
  	this.branchNumber				= "";  	
    
  	this.setEmployeeCode			= function(idVal){
      	this.employeeCode			= idVal;
    };
  
  	this.setEmployeeId				= function(idVal){
      	this.empId					= idVal;
    };
  
  	this.getEmployeeName			= function(){
      	/***var empName					= "";
      	empName						+= (!isEmpty(this.firstName)) ? this.firstName : "";
      	empName						+= " ";
      	empName						+= (!isEmpty(this.lastName)) ? this.lastName : "";
      	return empName;***/
      	return this.email;
    };
}

/** Class definition - Category **/
function Category(anIndex){
  	this.sectionIndex				= anIndex;
  	this.code						= 0;
  	this.i18n						= 0;
	this.checkItems					= [];
}

/** Class definition - Vehicle check item **/
function CheckItem(){
  	this.code						= 0;
  	this.i18n						= 0;
	this.msg_i18n					= "";
	this.isOkay						= "";
}