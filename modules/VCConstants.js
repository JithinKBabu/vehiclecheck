/**********************************************************************
*	Author  : RI - CTS363601 
*	Purpose : Module to refer all constant declarations
***********************************************************************/

function VCConstant() {

}

VCConstant.APP_VERSION				= "1.0";
VCConstant.SERVICE_HTTP_STATUS		= 200;
VCConstant.SERVICE_OPSTATUS			= 0;
VCConstant.SERVICE_MSG_SUCCESS		= "SUCCESS";

VCConstant.DEF_COUNTRY_CODE			= "UK";
VCConstant.DEF_BUSINESS_CODE		= "D";
VCConstant.DEF_LOCALE				= "en_GB";
VCConstant.DEF_LANGUAGE_CODE		= "ENG";

VCConstant.FORM_HOME				= "Home";
VCConstant.FORM_CONTENT				= "Content";
VCConstant.FORM_LOGIN				= "Login";

VCConstant.ICON_COLAPSE				= "icon_minus.png";
VCConstant.ICON_EXPAND				= "icon_plus.png";
VCConstant.ICON_RADIO_OFF			= "radio_off.png";
VCConstant.ICON_RADIO_ON			= "radio_on.png";
VCConstant.ICON_CHECKBOX_OFF		= "icon_checkbox_off.png";
VCConstant.ICON_CHECKBOX_ON			= "icon_checkbox_on.png";

VCConstant.SYNC_USER_ID				= "";
VCConstant.SYNC_PASSWORD			= "";
VCConstant.SYNC_APP_ID				= "100004898461ca65a";
VCConstant.SYNC_SERVER_HOST			= "rentokil-initial.sync.konycloud.com";
VCConstant.SYNC_SERVER_PORT         ="";
VCConstant.SYNC_IS_SECURE			= true;
VCConstant.SYNC_DO_UPLOAD			= true;
VCConstant.SYNC_DO_DOWNLOAD			= false;
VCConstant.SYNC_UPLOAD_ERROR_POLICY	= "continueonerror";

VCConstant.LAST_SYNC_DATE			= "VC_LAST_SYNC_DATE";
VCConstant.STORE_VEHICLE_NO			= "VC_VEHICLE_NO";
VCConstant.PEST			            = "Pest";
VCConstant.HYGIENE		            = "Hygiene";
VCConstant.SELECT                   = "Select";

var static_content					= "<h1 style=\"color:#0e202f; font-family:helvetica; font-size:14px; font-weight:bold;\">About us</h1>";
	static_content					+= "<p style=\"color:#0e202f; font-family:helvetica; font-size:12px; font-weight:bold;\">Rentokil Initial provides services that protect people and enhance lives. We protect people from the dangers of pest-borne disease, the risks of poor hygiene or from injury in the workplace. We enhance lives with services that protect the health and wellbeing of people, and the reputation of our customers' brands. We are one of the largest business services companies in the world providing Pest Control, Hygiene and Workwear services. We operate in over 60 countries. Embedded within our approach is the ability to analyse the progress and performance of the company in three ways - by Region and Category which form part of our Business Model and by Quadrant which is how we execute our Strategy including capital allocation.</p>";
	static_content					+= "<p style=\"color:gray; font-family:helvetica; font-size:12px;\">We are one of the largest business services companies in the world providing Pest Control, Hygiene and Workwear services. We operate in over 60 countries.</p>";
	static_content					+= "<p style=\"color:gray; font-family:helvetica; font-size:12px;\">Embedded within our approach is the ability to analyse the progress and performance of the company in three ways – by Region and Category which form part of our Business Model and by Quadrant which is how we execute our Strategy including capital allocation.</p>";

	


