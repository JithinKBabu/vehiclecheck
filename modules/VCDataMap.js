/**********************************************************************
*	Author  : RI - CTS363601 
*	Purpose : Module to refer vehicle check options
***********************************************************************/

var statCheckLists_IN_CAB			= [{"code":"FUEL", "i18n":"common.label.fuel", "msg_i18n":"common.message.fuel", "dbfield":"fuel"},
                                       {"code":"MIRRORS", "i18n":"common.label.mirrors", "msg_i18n":"common.message.mirrors", "dbfield":"Mirrors"},
                                       {"code":"HORN", "i18n":"common.label.horn", "msg_i18n":"common.message.horn", "dbfield":"Horn"},
                                       {"code":"INDICATORS", "i18n":"common.label.indicators", "msg_i18n":"common.message.indicators", "dbfield":"Indicators"},
                                       {"code":"LIGHT_AND_REFLECTORS_A", "i18n":"common.label.lightAndReflectorsA", "msg_i18n":"common.message.lightAndReflectorsA","dbfield":"GroupLightsA"},
                                       {"code":"LIGHT_AND_REFLECTORS_B", "i18n":"common.label.lightAndReflectorsB", "msg_i18n":"common.message.lightAndReflectorsB","dbfield":"GroupLightsB"},
                                       {"code":"LIGHT_AND_REFLECTORS_C", "i18n":"common.label.lightAndReflectorsC", "msg_i18n":"common.message.lightAndReflectorsC","dbfield":"GroupLightsC"},
                                       {"code":"WINDSCREEN_AND_GLASS", "i18n":"common.label.windscreenAndGlass", "msg_i18n":"common.message.windscreenAndGlass","dbfield":"Windscreen"},
                                       {"code":"TACHOGRAPH", "i18n":"common.label.tachograph", "msg_i18n":"common.message.tachograph","dbfield":"Tachograph"},
                                       {"code":"BRAKES", "i18n":"common.label.brakes", "msg_i18n":"common.message.brakes","dbfield":"Brakes"},
                                       {"code":"WIPERS_AND_WASHERS", "i18n":"common.label.wipersAndWashers", "msg_i18n":"common.message.wipersAndWashers","dbfield":"Wipers"},
                                       {"code":"STEERING", "i18n":"common.label.steering", "msg_i18n":"common.message.steering","dbfield":"Steering"},
                                       {"code":"GENERAL_SAFETY", "i18n":"common.label.generalSafety", "msg_i18n":"common.message.generalSafety","dbfield":"GeneralSafety"},
                                      ];
var statCheckLists_UNDER_BONNET		= [{"code":"OIL_CHECK", "i18n":"common.label.oilCheck", "msg_i18n":"common.message.oilCheck","dbfield":"OilCheck"},
                                       {"code":"COOLANT_LEVEL", "i18n":"common.label.coolantLevel", "msg_i18n":"common.message.coolantLevel","dbfield":"CoolantLevel"},
                                       {"code":"WASHER_BOTTLE", "i18n":"common.label.washerBottle", "msg_i18n":"common.message.washerBottle","dbfield":"WasherBottle"},
                                       {"code":"BRAKE_FLUID", "i18n":"common.label.brakeFluid", "msg_i18n":"common.message.brakeFluid","dbfield":"BrakeFluid"}
                                      ];
var statCheckLists_EXTERIOR			= [{"code":"TAX_DISC", "i18n":"common.label.taxDisc", "msg_i18n":"common.message.taxDisc","dbfield":"TaxDisc"},
                                       {"code":"TYRE_CONDITION", "i18n":"common.label.tyreCondition", "msg_i18n":"common.message.tyreCondition","dbfield":"TyreCondition"},
                                       {"code":"WHEEL", "i18n":"common.label.wheel", "msg_i18n":"common.message.wheel","dbfield":"WheelNuts"},
                                       {"code":"NUMBER_PLATES", "i18n":"common.label.numberPlates", "msg_i18n":"common.message.numberPlates","dbfield":"NumberPlate"},
                                       {"code":"BODY_CONDITION", "i18n":"common.label.bodyCondition", "msg_i18n":"common.message.bodyCondition","dbfield":"BodyCondition"},
                                       {"code":"SIDE_STEP", "i18n":"common.label.sideStep", "msg_i18n":"common.message.sideStep","dbfield":"SideStep"}
                                      ];

var statCategories					= [{"code":"IN_CAB", "i18n":"common.category.inCab", "items":statCheckLists_IN_CAB}, 
                               		   {"code":"UNDER_BONNET", "i18n":"common.category.underBonnet", "items":statCheckLists_UNDER_BONNET},
                               		   {"code":"EXTERIOR", "i18n":"common.category.exterior", "items":statCheckLists_EXTERIOR},
                              		  ];

var businessCodes                  =[["Select","Select business"],["Pest","Pest"],["Hygiene","Specialist Hygiene"]];

var pestbranches  				   = [["Select","Select Branch"],["002 E-London","002 E-London"],["003 Essex","003 Essex"],["004 Home Counties","004 Home Counties"],["006 Fumigation","006 Fumigation"],["012 Watford","012 Watford"],["016 Uxbridge","016 Uxbridge"],["044 East Anglia","044 East Anglia"],["045 MK & Cambridge","045 MK & Cambridge"],["014 South Coast","014 South Coast"],["017 Oxford","017 Oxford"],["027 Devon Cornwall & Channel","027 Devon Cornwall & Channel"],["028 Three Counties","028 Three Counties"],["029 Bristol and Wiltshire","029 Bristol and Wiltshire"],["033 S&W-Wales","033 S&W-Wales"],["008 Kent","008 Kent"],["009 Sussex","009 Sussex"],["010 SW-Surrey London","010 SW-Surrey London"],["015 Surrey","015 Surrey"],["020 London Underground","020 London Underground"],["021 Mayfair","021 Mayfair"],["022 London West End","022 London West End"],["023 London The City","023 London The City"],["024 Fulham and Kensington","024 Fulham and Kensington"],["046 Leicester & Peterborough","046 Leicester & Peterborough"],["058 Hull","058 Hull"],["059 Leeds & Bradford","059 Leeds & Bradford"],["060 S&W-Yorkshire","060 S&W-Yorkshire"],["061 Teesside","061 Teesside"],["062 Newcastle","062 Newcastle"],["063 Leeds CC/Yorks Water","063 Leeds CC/Yorks Water"],["037 W-Midlands Central","037 W-Midlands Central"],["038 W-Midlands North","038 W-Midlands North"],["040 W-Midlands South","040 W-Midlands South"],["051 N-Wales"," Wirral & IoM","051 N-Wales"," Wirral & IoM"],["052 Liverpool","052 Liverpool"],["053 Manchester","053 Manchester"],["055 Cumbria","055 Cumbria"],["065 N-Glasgow","065 N-Glasgow"],["072 Edinburgh","072 Edinburgh"],["073 Aberdeen","073 Aberdeen"],["067 Belfast","067 Belfast"],["097 Hit Squad","097 Hit Squad"]];
	
var	hygienebranches 			   = [["Select","Select Branch"],["180 Birmingham","180 Birmingham"],["181 Bristol","181 Bristol"],["182 East Kilbride","182 East Kilbride"],["183 Harlow","183 Harlow"],["184 Haydock","184 Haydock"],["185 Leeds","185 Leeds"],["186 Newcastle","186 Newcastle"]];

