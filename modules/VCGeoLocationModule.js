/**********************************************************************
*	Author  : RI - CTS363601 
*	Purpose : Module to refer the geo location based methods
***********************************************************************/

/** To capture current location **/
/** timeout = 15 seconds and cached location age = 10 minutes **/
function captureCurrentLocation(successCallbackHandler, errorCallbackHandler){
  	var positionOptions 		= {timeout: 15000, maximumAge: 600000}; 
  	kony.location.getCurrentPosition(successCallbackHandler, errorCallbackHandler, positionOptions);
}

/** Success callback locaion capture - Start Normal work **/
function success_vCheckGeoLocCaptureCallback(position){
  
      //added for saving in DB
    gblLocationData.latitude 	= position.coords.latitude;
  	gblLocationData.longitude 	= position.coords.longitude;
  	gblLocationData.accuracy 	= position.coords.accuracy;
   printMessage("#######gblLocationData.latitude--->"+gblLocationData.latitude);
   printMessage("#######gblLocationData.longitude---->>>"+gblLocationData.longitude);
   printMessage("#######gblLocationData.accuracy----->>>"+gblLocationData.accuracy);
  
  	gblGeoPosition			= "";
  	gblGeoPosition 			= getI18nString("common.label.latitude") + " " + position.coords.latitude;
  	gblGeoPosition 			= gblGeoPosition + " " + getI18nString("common.label.longitude") + " " + position.coords.longitude;
    gblGeoPosition 			= gblGeoPosition + " " + getI18nString("common.label.accuracy") + " " + position.coords.accuracy;
    var d 					= new Date(position.timestamp).toString();
    gblGeoPosition 			= gblGeoPosition + " " + getI18nString("common.label.date") + " " + d;
  
      printMessage("#######gblGeoPosition----->>>"+gblGeoPosition);
  
  
  
  
  	saveVehicleChecks();
}

/** Failure callback locaion capture - Start Normal work **/
function failure_vCheckGeoLocCaptureCallback(positionerror){
     gblGeoPosition 			= getI18nString("common.label.errorCode") + " " + positionerror.code + " " + getI18nString("common.label.message") + " " + positionerror.message;
  	 gblLocationData.latitude  ="NA";
     gblLocationData.longitude ="NA";
     gblLocationData.accuracy  ="NA";
     saveVehicleChecks();
}