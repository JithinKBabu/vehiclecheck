/**********************************************************************
*	Author  : RI - CTS363601 
*	Purpose : Module to refer Popup related methods
***********************************************************************/

/** Popup for displayin warnings and messages **/
function showMessagePopup(msgKey, buttonHandlerMethod){
  	PopupMessage.lblTitle.text		= getI18nString("common.app.title");
  	PopupMessage.lblMessage.text	= getI18nString(msgKey);
    PopupMessage.btnOk.text			= getI18nString("common.label.ok");
  	/*if (!isEmpty(buttonHandlerMethod)){
      	PopupMessage.btnOk.onClick	= buttonHandlerMethod;
    } else {
      	PopupMessage.btnOk.onClick	= dismissMessagePopup;
    }*/
    PopupMessage.show();  
}

/** Popup - Ok button handler **/
function dismissMessagePopup(){
  	PopupMessage.dismiss(); 
}

/** Progress popup - show **/
function showProgressPopup(msgKey, titleKey){
  	PopupProgress.lblTitle.text		= getI18nString(titleKey);
    PopupProgress.lblMessage.text	= getI18nString(msgKey);
  	PopupProgress.show();
}

/** Progress popup - update message **/
function updateProgressMessage(msgKey){
  	 PopupProgress.lblMessage.text	= getI18nString(msgKey);
}

/** Progress popup - dismiss **/
function dismissProgressMessage(){
  	 PopupProgress.dismiss();
}


/** Popup for displayin warnings and messages **/
function showBranchPopup(){
  	Login.lblBranchTitle.text		= getI18nString("common.app.title");
  //	Login.lblMessage.text	= getI18nString(msgKey);
    Login.btnBranchOk.text			= getI18nString("common.label.ok");
  	/*if (!isEmpty(buttonHandlerMethod)){
      	PopupMessage.btnOk.onClick	= buttonHandlerMethod;
    } else {
      	PopupMessage.btnOk.onClick	= dismissMessagePopup;
    }*/
   // PopupBranch.show();  
}
