/**********************************************************************
*	Author  : RI - CTS363601 
*	Purpose : Module to refer web service based methods
***********************************************************************/

/** Integration service - AuthorizeUser  **/
function callAuthenticateUserIntService(inputData){
  	var integrationClient 		= null;
	var serviceName 			= inputData.serviceName;
	var operationName			= inputData.operationName;
  	var params					= inputData.inputs;
  	var headers					= null;
  	try {
		integrationClient 		= KNYMobileFabric.getIntegrationService(serviceName);
	} catch(exception){
		printMessage("Exception: " + exception.message);
	}
  	integrationClient.invokeOperation(operationName, headers, params, success_authorizeEmailCallback, failure_authorizeEmailCallback); 
}

/** Initialize Sync Module  **/
function initSyncModule(){ 	
  	printMessage("#***# Inside Init #***#");
  	try {
      	sync.init(success_initSyncModule, failure_initSyncModule);
    } catch(exception) {
      	printMessage("Exception: " + exception.message);
   	}
}