//****************Sync Version:MobileFabricInstaller-QA-7.2.0_v201610191639_r117*******************
// ****************Generated On Fri Nov 25 12:14:24 UTC 2016vcdetails*******************
// **********************************Start vcdetails's helper methods************************
if (typeof(kony) === "undefined") {
	kony = {};
}

if (typeof(kony.sync) === "undefined") {
	kony.sync = {};
}

if (typeof(kony.sync.log) === "undefined") {
	kony.sync.log = {};
}

if (typeof(sync) === "undefined") {
	sync = {};
}

if (typeof(sync.log) === "undefined") {
	sync.log = {};
}



if(typeof(VCScope)=== "undefined"){ VCScope = {}; }

/************************************************************************************
* Creates new vcdetails
*************************************************************************************/
VCScope.vcdetails = function(){
	this.vcId = null;
	this.regno = null;
	this.email = null;
	this.location = null;
	this.dateField = null;
	this.isDeleted = null;
	this.lastModifiedDate = null;
	this.image = null;
	this.latitude = null;
	this.longitude = null;
	this.accuracy = null;
	this.timestampField = null;
	this.mileage = null;
	this.branchNumber = null;
	this.markForUpload = true;
};

VCScope.vcdetails.prototype = {
	get vcId(){
		return this._vcId;
	},
	set vcId(val){
		if(!kony.sync.isEmptyString(val) && !kony.sync.isNull(val) && !kony.sync.isValidNumberType(val)){
			sync.log.error("Invalid data type for the attribute vcId in vcdetails.\nExpected:\"integer\"\nActual:\"" + kony.type(val) + "\"");
		}
		this._vcId = val;
	},
	get regno(){
		return this._regno;
	},
	set regno(val){
		this._regno = val;
	},
	get email(){
		return this._email;
	},
	set email(val){
		this._email = val;
	},
	get location(){
		return this._location;
	},
	set location(val){
		this._location = val;
	},
	get dateField(){
		return this._dateField;
	},
	set dateField(val){
		this._dateField = val;
	},
	get isDeleted(){
		return kony.sync.getBoolean(this._isDeleted)+"";
	},
	set isDeleted(val){
		if(!kony.sync.isEmptyString(val) && !kony.sync.isNull(val) && !kony.sync.isValidBooleanType(val)){
			sync.log.error("Invalid data type for the attribute isDeleted in vcdetails.\nExpected:\"boolean\"\nActual:\"" + kony.type(val) + "\"");
		}
		this._isDeleted = val;
	},
	get lastModifiedDate(){
		return this._lastModifiedDate;
	},
	set lastModifiedDate(val){
		this._lastModifiedDate = val;
	},
	get image(){
		return this._image;
	},
	set image(val){
		this._image = val;
	},
	get latitude(){
		return this._latitude;
	},
	set latitude(val){
		this._latitude = val;
	},
	get longitude(){
		return this._longitude;
	},
	set longitude(val){
		this._longitude = val;
	},
	get accuracy(){
		return this._accuracy;
	},
	set accuracy(val){
		this._accuracy = val;
	},
	get timestampField(){
		return this._timestampField;
	},
	set timestampField(val){
		this._timestampField = val;
	},
	get mileage(){
		return this._mileage;
	},
	set mileage(val){
		this._mileage = val;
	},
	get branchNumber(){
		return this._branchNumber;
	},
	set branchNumber(val){
		this._branchNumber = val;
	},
};

/************************************************************************************
* Retrieves all instances of vcdetails SyncObject present in local database with
* given limit and offset where limit indicates the number of records to be retrieved
* and offset indicates number of rows to be ignored before returning the records.
* e.g. var orderByMap = []
* orderByMap[0] = {};
* orderByMap[0].key = "vcId";
* orderByMap[0].sortType ="desc";
* orderByMap[1] = {};
* orderByMap[1].key = "regno";
* orderByMap[1].sortType ="asc";
* var limit = 20;
* var offset = 5;
* VCScope.vcdetails.getAll(successcallback,errorcallback, orderByMap, limit, offset)
*************************************************************************************/
VCScope.vcdetails.getAll = function(successcallback, errorcallback, orderByMap, limit, offset){
	sync.log.trace("Entering VCScope.vcdetails.getAll->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = VCScope.vcdetails.getTableName();
	orderByMap = kony.sync.formOrderByClause("vcdetails",orderByMap);
	var query = kony.sync.qb_createQuery();
				kony.sync.qb_select(query, null);	
				kony.sync.qb_from(query, tbname);
				kony.sync.qb_orderBy(query, orderByMap);
				kony.sync.qb_limitOffset(query,limit,offset);
	var query_compile = kony.sync.qb_compile(query);
	var sql = query_compile[0];
	var params = query_compile[1];

	function mySuccCallback(res){
		sync.log.trace("Entering VCScope.vcdetails.getAll->successcallback");
		successcallback(VCScope.vcdetails.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
	}	
	kony.sync.single_select_execute(dbname,sql, params,mySuccCallback,errorcallback);
};

/************************************************************************************
* Returns number of vcdetails present in local database.
*************************************************************************************/
VCScope.vcdetails.getAllCount = function(successcallback,errorcallback){
	sync.log.trace("Entering VCScope.vcdetails.getAllCount function");
	VCScope.vcdetails.getCount("",successcallback,errorcallback);
};

/************************************************************************************
* Returns number of vcdetails using where clause in the local Database
*************************************************************************************/
VCScope.vcdetails.getCount = function(wcs,successcallback,errorcallback){
	sync.log.trace("Entering VCScope.vcdetails.getCount->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "VCScope.vcdetails.getCount" , "getCount", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = VCScope.vcdetails.getTableName();
	wcs = kony.sync.validateWhereClause(wcs);
	var sql = "select count(*) from \"" + tbname + "\" " + wcs;
	kony.sync.single_execute_sql(dbname,sql, null, mySuccCallback, errorcallback);
	function mySuccCallback(res) {
		sync.log.trace("Entering VCScope.vcdetails.getCount->successcallback");
		if(null!==res){
			var count = null;
			count = res["count(*)"];
			kony.sync.verifyAndCallClosure(successcallback, {count:count});
		}
		else{
			sync.log.error("Some error occured while getting the count");
		}
	}
};

/************************************************************************************
* Creates a new instance of vcdetails in the local Database. The new record will 
* be merged with the enterprise datasource in the next Sync.
*************************************************************************************/
VCScope.vcdetails.prototype.create = function(successcallback,errorcallback){
	sync.log.trace("Entering  VCScope.vcdetails.prototype.create function");
	var valuestable = this.getValuesTable(true);
	VCScope.vcdetails.create(valuestable, successcallback,errorcallback,this.markForUpload);
};
VCScope.vcdetails.create = function(valuestable, successcallback,errorcallback,markForUpload){
	sync.log.trace("Entering  VCScope.vcdetails.create->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	
	if(!kony.sync.validateInput(arguments, "VCScope.vcdetails.create" , "create", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = VCScope.vcdetails.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);

	if(kony.sync.attributeValidation(valuestable,"vcdetails",errorcallback,true)===false){
		return;
	}
	
	function executeSuccess(){
		sync.log.trace("Entering  VCScope.vcdetails.create->success callback");
		kony.sync.single_insert_execute(dbname,tbname,valuestable,successcallback,errorcallback,markForUpload);
	}

	if(kony.sync.enableORMValidations){
		var relationshipMap={};  
		relationshipMap = VCScope.vcdetails.getRelationshipMap(relationshipMap,valuestable);
		kony.sync.checkIntegrity(dbname,relationshipMap,executeSuccess,errorcallback);
	}
	else{
		kony.sync.single_insert_execute(dbname,tbname,valuestable,successcallback,errorcallback,markForUpload);
	}
};

/************************************************************************************
* Creates number of new instances of vcdetails in the local Database. These new 
* records will be merged with the enterprise datasource in the next Sync. Based upon 
* kony.sync.enableORMValidations flag, validations will be enabled/disabled.
* e.g.	var valuesArray = [];
*		valuesArray[0] = {};
*		valuesArray[0].regno = "regno_0";
*		valuesArray[0].email = "email_0";
*		valuesArray[0].location = "location_0";
*		valuesArray[0].dateField = "dateField_0";
*		valuesArray[0].image = 0;
*		valuesArray[0].latitude = "latitude_0";
*		valuesArray[0].longitude = "longitude_0";
*		valuesArray[0].accuracy = "accuracy_0";
*		valuesArray[0].timestampField = "timestampField_0";
*		valuesArray[0].mileage = "mileage_0";
*		valuesArray[0].branchNumber = "branchNumber_0";
*		valuesArray[1] = {};
*		valuesArray[1].regno = "regno_1";
*		valuesArray[1].email = "email_1";
*		valuesArray[1].location = "location_1";
*		valuesArray[1].dateField = "dateField_1";
*		valuesArray[1].image = 1;
*		valuesArray[1].latitude = "latitude_1";
*		valuesArray[1].longitude = "longitude_1";
*		valuesArray[1].accuracy = "accuracy_1";
*		valuesArray[1].timestampField = "timestampField_1";
*		valuesArray[1].mileage = "mileage_1";
*		valuesArray[1].branchNumber = "branchNumber_1";
*		valuesArray[2] = {};
*		valuesArray[2].regno = "regno_2";
*		valuesArray[2].email = "email_2";
*		valuesArray[2].location = "location_2";
*		valuesArray[2].dateField = "dateField_2";
*		valuesArray[2].image = 2;
*		valuesArray[2].latitude = "latitude_2";
*		valuesArray[2].longitude = "longitude_2";
*		valuesArray[2].accuracy = "accuracy_2";
*		valuesArray[2].timestampField = "timestampField_2";
*		valuesArray[2].mileage = "mileage_2";
*		valuesArray[2].branchNumber = "branchNumber_2";
*		VCScope.vcdetails.createAll(valuesArray, successcallback, errorcallback, true);
*************************************************************************************/
VCScope.vcdetails.createAll = function(valuesArray, successcallback, errorcallback, markForUpload){
	sync.log.trace("Entering VCScope.vcdetails.createAll function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "VCScope.vcdetails.createAll" , "createAll", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = VCScope.vcdetails.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);
	var isProperData = true;
	var arrayLen = 0;
	var errorInfo = [];
	var arrayLength = valuesArray.length;
	var errObject = null;
	var isReferentialIntegrityFailure = false;
	var errMsg = null;
	if(kony.sync.enableORMValidations){
		var newValuesArray = [];

		//column level validations
		for (var i=0; valuesArray != null && i < arrayLength; i++ ){
			var valuestable = valuesArray[i];
			if(kony.sync.attributeValidation(valuestable,"vcdetails",errorcallback,true)===false){
				return;
			}

			newValuesArray[i] = valuestable;
		}
		valuesArray = newValuesArray;
		var connection = kony.sync.getConnectionOnly(dbname, dbname);
		kony.sync.startTransaction(connection, checkIntegrity, transactionSuccessCallback, transactionErrorCallback);
		var isError = false;
	}
	else{
		//copying by value
		var newValuesArray = [];
		arrayLength = valuesArray.length;
		for (var i=0; valuesArray != null && i < arrayLength; i++ ){
			newValuesArray[i] = kony.sync.CreateCopy(valuesArray[i]);
		}
		valuesArray = newValuesArray;
		kony.sync.massInsert(dbname, tbname, valuesArray, successcallback, errorcallback, markForUpload);
	}

	function transactionErrorCallback(){
		if(isError==true){
			//Statement error has occurred
				kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
		}
		else{
			//Transaction error has occurred
				kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodeTransaction, kony.sync.getErrorMessage(kony.sync.errorCodeTransaction), null));
		}
	}

	function transactionSuccessCallback(){
		sync.log.trace("Entering  VCScope.vcdetails.createAll->transactionSuccessCallback");
		if(!isError){
			kony.sync.massInsert(dbname, tbname, valuesArray, successcallback, errorcallback, markForUpload);
		}
		else{
			if(isReferentialIntegrityFailure){
				kony.sync.verifyAndCallClosure(errorcallback, errObject);
			}
		}
	}
	
	//foreign key constraints validations
	function checkIntegrity(tx){
		sync.log.trace("Entering  VCScope.vcdetails.createAll->checkIntegrity");
		arrayLength = valuesArray.length;
		for (var i=0; valuesArray != null && i < arrayLength; i++ ){
			var relationshipMap={};  
			relationshipMap = VCScope.vcdetails.getRelationshipMap(relationshipMap,valuesArray[i]);
			errObject = kony.sync.checkIntegrityinTransaction(tx, relationshipMap, null);
			if(errObject===false){
				isError = true;
				return; 
			}
			if(errObject!==true){
				isError = true;
				isReferentialIntegrityFailure = true;
				return;
			}
		}
	}
};
/************************************************************************************
* Updates vcdetails using primary key in the local Database. The update will be
* merged with the enterprise datasource in the next Sync.
*************************************************************************************/
VCScope.vcdetails.prototype.updateByPK = function(successcallback,errorcallback){
	sync.log.trace("Entering  VCScope.vcdetails.prototype.updateByPK function");
	var pks = this.getPKTable();
	var valuestable = this.getValuesTable(false);
	VCScope.vcdetails.updateByPK(pks,valuestable, successcallback,errorcallback,this.markForUpload);
};
VCScope.vcdetails.updateByPK = function(pks,valuestable, successcallback,errorcallback, markForUpload){
	sync.log.trace("Entering  VCScope.vcdetails.updateByPK-> main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "VCScope.vcdetails.updateByPK",  "updateByPk", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = VCScope.vcdetails.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);
	var wcs = [ ];

	if(VCScope.vcdetails.pkCheck(pks,wcs,errorcallback,"updating")===false){
		return;
	}

	if(kony.sync.attributeValidation(valuestable,"vcdetails",errorcallback,false)===false){
		return;
	}

	var relationshipMap={};  
	relationshipMap = VCScope.vcdetails.getRelationshipMap(relationshipMap,valuestable);

	kony.sync.updateByPK(tbname, dbname, relationshipMap, pks,valuestable, successcallback,errorcallback, markForUpload, wcs);
};

/************************************************************************************
* Updates vcdetails(s) using where clause in the local Database. The update(s)
* will be merged with the enterprise datasource in the next Sync.
*************************************************************************************/
VCScope.vcdetails.update = function(wcs, valuestable, successcallback,errorcallback,markForUpload){
	sync.log.trace("Entering VCScope.vcdetails.update function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "VCScope.vcdetails.update",  "update", errorcallback)){
		return;
	}

	var dbname = kony.sync.getDBName();
	var tbname = VCScope.vcdetails.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);
	wcs = kony.sync.validateWhereClause(wcs);

	if(kony.sync.attributeValidation(valuestable,"vcdetails",errorcallback,false)===false){
		return;
	}
	function executeSuccess(){
		sync.log.trace("Entering  VCScope.vcdetails.update-> success callback of Integrity Check");
		kony.sync.single_update_execute(dbname,tbname,valuestable,wcs,successcallback,errorcallback,true, markForUpload, VCScope.vcdetails.getPKTable());
	}

	if(kony.sync.enableORMValidations){
		var relationshipMap={};  
		relationshipMap = VCScope.vcdetails.getRelationshipMap(relationshipMap,valuestable);
		kony.sync.checkIntegrity(dbname,relationshipMap,executeSuccess,errorcallback);
	}
	else{
		kony.sync.single_update_execute(dbname,tbname,valuestable,wcs,successcallback,errorcallback,true, markForUpload, VCScope.vcdetails.getPKTable());
	}
};

/************************************************************************************
* Updates vcdetails(s) satisfying one or more where clauses in the local Database. 
* The update(s) will be merged with the enterprise datasource in the next Sync.
* Based upon kony.sync.enableORMValidations flag, validations will be enabled/disabled.
* e.g.	var inputArray = [];
*		inputArray[0] = {};
*		inputArray[0].changeSet = {};
*		inputArray[0].changeSet.regno = "regno_updated0";
*		inputArray[0].changeSet.email = "email_updated0";
*		inputArray[0].changeSet.location = "location_updated0";
*		inputArray[0].changeSet.dateField = "dateField_updated0";
*		inputArray[0].whereClause = "where vcId = 0";
*		inputArray[1] = {};
*		inputArray[1].changeSet = {};
*		inputArray[1].changeSet.regno = "regno_updated1";
*		inputArray[1].changeSet.email = "email_updated1";
*		inputArray[1].changeSet.location = "location_updated1";
*		inputArray[1].changeSet.dateField = "dateField_updated1";
*		inputArray[1].whereClause = "where vcId = 1";
*		inputArray[2] = {};
*		inputArray[2].changeSet = {};
*		inputArray[2].changeSet.regno = "regno_updated2";
*		inputArray[2].changeSet.email = "email_updated2";
*		inputArray[2].changeSet.location = "location_updated2";
*		inputArray[2].changeSet.dateField = "dateField_updated2";
*		inputArray[2].whereClause = "where vcId = 2";
*		VCScope.vcdetails.updateAll(inputArray,successcallback,errorcallback);
*************************************************************************************/
VCScope.vcdetails.updateAll = function(inputArray, successcallback, errorcallback, markForUpload) {
	sync.log.trace("Entering VCScope.vcdetails.updateAll function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "VCScope.vcdetails.updateAll",  "updateAll", errorcallback)){
		return;
	}
	var dbname = "100004898461ca65a";
	var tbname = "vcdetails";
	var isError = false;
	var errObject = null;
	if(markForUpload == false || markForUpload == "false"){
		markForUpload="false"
	}
	else{
		markForUpload="true"
	}
	if((kony.sync.enableORMValidations)){

		var newInputArray = [];
		for (var i=0; ((inputArray) != null) && i < inputArray.length; i++ ){
			var v = inputArray[i];
			var valuestable = v.changeSet;
			var isEmpty = true;
			for(var key in valuestable){
				isEmpty = false;
				break;
			}
			if(isEmpty){
				errorcallback(kony.sync.getErrorTable(kony.sync.errorCodeNullValue,kony.sync.getErrorMessage(kony.sync.errorCodeNullValue)));
				return;
			}
			var wcs = v.whereClause;
			var twcs = wcs;
			if(kony.sync.attributeValidation(valuestable,"vcdetails",errorcallback,false)===false){
				return;
			}

			newInputArray[i] = [];
			newInputArray[i].changeSet = valuestable;
			newInputArray[i].whereClause = wcs;
		}
		inputArray = newInputArray;
		var connection = kony.sync.getConnectionOnly(dbname, dbname);
		kony.sync.startTransaction(connection, checkIntegrity, transactionSuccessCallback, transactionErrorCallback);

	}
	else{
		//copying by value
		var newInputArray = [];
		for (var i=0; ((inputArray) != null) && i < inputArray.length; i++ ){
		    var v = inputArray[i];
		    newInputArray[i] = kony.sync.CreateCopy(v);
		}
		inputArray = newInputArray;
		kony.sync.massUpdate(dbname, tbname,inputArray,successcallback,errorcallback,markForUpload, VCScope.vcdetails.getPKTable());
	}
	
		function transactionSuccessCallback(){
		sync.log.trace("Entering  VCScope.vcdetails.updateAll->transactionSuccessCallback");
		if(!isError){
			kony.sync.massUpdate(dbname, tbname,inputArray,successcallback,transactionErrorCallback,markForUpload, VCScope.vcdetails.getPKTable());
		}
	}

	function transactionErrorCallback(){
		if(errObject===false){
			//Sql statement error has occcurred
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
			
		}
		else if(errObject!==null){
			// Referential integrity error has occurred
			kony.sync.verifyAndCallClosure(errorcallback, errObject);
		}
		else{
			//Transaction error has occurred
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodeTransaction, kony.sync.getErrorMessage(kony.sync.errorCodeTransaction), null));
		}
	}
	//foreign key constraints validations
	function checkIntegrity(tx){
		sync.log.trace("Entering  VCScope.vcdetails.updateAll->checkIntegrity");
		for (var i=0; ((inputArray) != null) && i < inputArray.length; i++ ){
			var relationshipMap={}; 
			relationshipMap = VCScope.vcdetails.getRelationshipMap(relationshipMap,inputArray[i].changeSet);
			sync.log.debug("Relationship Map for Integrity check created:", relationshipMap);
			errObject = kony.sync.checkIntegrityinTransaction(tx, relationshipMap, null);
			if(errObject===false){
				isError = true;
				return; 
			}
			if(errObject!==true){
				isError = true;
				kony.sync.rollbackTransaction(tx);
				return;
			}
		}
	}


}
/************************************************************************************
* Deletes vcdetails using primary key from the local Database. The record will be
* deleted from the enterprise datasource in the next Sync.
*************************************************************************************/
VCScope.vcdetails.prototype.deleteByPK = function(successcallback,errorcallback){
	sync.log.trace("Entering VCScope.vcdetails.prototype.deleteByPK function");
	var pks = this.getPKTable();
	VCScope.vcdetails.deleteByPK(pks,successcallback,errorcallback,this.markForUpload);
};
VCScope.vcdetails.deleteByPK = function(pks, successcallback,errorcallback, markForUpload){
	sync.log.trace("Entering VCScope.vcdetails.deleteByPK-> main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "VCScope.vcdetails.deleteByPK",  "deleteByPK", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = VCScope.vcdetails.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);
	var wcs = [];
	var isError = false;
	var pkNotFound = false;
	var twcs = [];
	var deletedRows;
	var record = "";
	if(VCScope.vcdetails.pkCheck(pks,wcs,errorcallback,"deleting")===false){
		return;
	}	
	twcs = kony.sync.CreateCopy(wcs);
	function vcdetailsTransactionCallback(tx){
		sync.log.trace("Entering VCScope.vcdetails.deleteByPK->vcdetails_PKPresent successcallback");
		record = kony.sync.getOriginalRow(tx, tbname, wcs, errorcallback);
		if(record===false){
			isError = true;
			return;
		}
		if (null !== record) {
		}else{
			pkNotFound = true;
		}
		var deletedRows = kony.sync.remove(tx, tbname, wcs, false, markForUpload, null);
			if(deletedRows === false){
				isError = true;
			}
	}
	
	function vcdetailsErrorCallback(){
		sync.log.error("Entering VCScope.vcdetails.deleteByPK->relationship failure callback");
		if(isError === false){
			kony.sync.verifyAndCallClosure(errorcallback);
		}
		if(kony.sync.errorObject != null){
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
		}

	}
	function vcdetailsSuccessCallback(){
		sync.log.trace("Entering VCScope.vcdetails.deleteByPK->relationship success callback");
		if(pkNotFound === true){
			kony.sync.verifyAndCallClosure(pkNotFoundErrCallback);
			return;
		}
		
		if(!isError){
			kony.sync.verifyAndCallClosure(successcallback, {rowsdeleted:1});
		}
	}
	
	function pkNotFoundErrCallback(){
		sync.log.error("Entering VCScope.vcdetails.deleteByPK->PK not found callback");
		kony.sync.pkNotFoundErrCallback(errorcallback,tbname);
	}
		
		var dbconnection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
		if(dbconnection != null){
			kony.sync.startTransaction(dbconnection, vcdetailsTransactionCallback, vcdetailsSuccessCallback, vcdetailsErrorCallback, "Single Execute");
		}

};

/************************************************************************************
* Deletes vcdetails(s) using where clause from the local Database. The record(s)
* will be deleted from the enterprise datasource in the next Sync.
* e.g. VCScope.vcdetails.remove("where regno like 'A%'", successcallback,errorcallback, true);
*************************************************************************************/
VCScope.vcdetails.remove = function(wcs, successcallback,errorcallback, markForUpload){
	sync.log.trace("Entering VCScope.vcdetails.remove->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "VCScope.vcdetails.remove",  "remove", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = VCScope.vcdetails.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);
	wcs = kony.sync.validateWhereClause(wcs);
	var twcs = wcs;
	var isError = false;
	var rowsDeleted;

	function vcdetails_removeTransactioncallback(tx){
		wcs = " " + wcs;
		rowsDeleted = kony.sync.deleteBatch(tx, tbname, wcs, false, markForUpload, errorcallback)
		if(rowsDeleted === false){
			isError = true;
		}
	}
	function vcdetails_removeSuccess(){
		sync.log.trace("Entering VCScope.vcdetails.remove->vcdetails_removeSuccess function");

		if(!isError){
			kony.sync.verifyAndCallClosure(successcallback, rowsDeleted);
		}
	}
	function errorcallbackWrapper(){
		sync.log.trace("Entering VCScope.vcdetails.remove->error callback function");
		if(!isError){
			kony.sync.showTransactionError(errorcallback);
		}
		if(kony.sync.errorObject != null){
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
		}
	}
	function deleteEntity(){
		sync.log.trace("Entering VCScope.vcdetails.remove->delete Entity function");
		var connection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
		if(connection != null){
			kony.sync.startTransaction(connection, vcdetails_removeTransactioncallback, vcdetails_removeSuccess, errorcallbackWrapper);
		}
	}
	deleteEntity();
};

/************************************************************************************
* Deletes vcdetails using primary key from the local Database. This will
* not have any effect in enterprise datasource in subsequent sync cycles
*************************************************************************************/
VCScope.vcdetails.prototype.removeDeviceInstanceByPK = function(successcallback,errorcallback){
	sync.log.trace("Entering VCScope.vcdetails.prototype.removeDeviceInstanceByPK function");
	var pks = this.getPKTable();
	VCScope.vcdetails.removeDeviceInstanceByPK(pks,successcallback,errorcallback);
};
VCScope.vcdetails.removeDeviceInstanceByPK = function(pks, successcallback,errorcallback){
	sync.log.trace("Entering VCScope.vcdetails.removeDeviceInstanceByPK function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "VCScope.vcdetails.removeDeviceInstanceByPK",  "removeDeviceInstanceByPK", errorcallback)){
		return;
	}	
	var dbname = kony.sync.getDBName();
	var tbname = VCScope.vcdetails.getTableName();
	var wcs = [];
	var isError = false;
	var pkNotFound = false;
	var deletedRows;
	if(VCScope.vcdetails.pkCheck(pks,wcs,errorcallback,"deleting")===false){
		return;
	}
	
	function vcdetailsTransactionCallback(tx){
		sync.log.trace("Entering VCScope.vcdetails.removeDeviceInstanceByPK -> vcdetailsTransactionCallback");
		var record = kony.sync.getOriginalRow(tx, tbname, wcs, errorcallback);
		if(null !== record && false !=record) {
			deletedRows = kony.sync.remove(tx, tbname, wcs, true, null, null);
			if(deletedRows === false){
				isError = true;
			}
		}else{
			pkNotFound = true;
		}
	}
	
	function vcdetailsErrorCallback(){
		sync.log.error("Entering VCScope.vcdetails.removeDeviceInstanceByPK -> vcdetailsErrorCallback");
		if(isError === false){
			kony.sync.verifyAndCallClosure(errorcallback);
		}
		if(kony.sync.errorObject != null){
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
		}
	}
	
	function vcdetailsSuccessCallback(){
		sync.log.trace("Entering VCScope.vcdetails.removeDeviceInstanceByPK -> vcdetailsSuccessCallback");
		if(pkNotFound === true){
			kony.sync.verifyAndCallClosure(pkNotFoundErrCallback);
			return;
		}
		
		if(!isError){
			kony.sync.verifyAndCallClosure(successcallback, {rowsdeleted:1});
		}
	}
	
	function pkNotFoundErrCallback(){
		sync.log.error("Entering VCScope.vcdetails.removeDeviceInstanceByPK -> PK not found callback");
		kony.sync.pkNotFoundErrCallback(errorcallback,tbname);
	}
	
	var dbconnection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
	if(dbconnection != null){
		kony.sync.startTransaction(dbconnection, vcdetailsTransactionCallback, vcdetailsSuccessCallback, vcdetailsErrorCallback, "Single Execute");
	}

};

/************************************************************************************
* Deletes vcdetails(s) using where clause from the local Database. This will
* not have any effect in enterprise datasource in subsequent sync cycles
*************************************************************************************/
VCScope.vcdetails.removeDeviceInstance = function(wcs, successcallback,errorcallback){
	sync.log.trace("Entering VCScope.vcdetails.removeDeviceInstance->main function");
	var dbname = kony.sync.getDBName();
	var tbname = VCScope.vcdetails.getTableName();
	wcs = kony.sync.validateWhereClause(wcs);
	var twcs = wcs;
	var isError = false;
	var rowsDeleted;

	function vcdetails_removeTransactioncallback(tx){
		wcs = " " + wcs;
		rowsDeleted = kony.sync.deleteBatch(tx, tbname, wcs, true, null, errorcallback)
		if(rowsDeleted === false){
			isError = true;
		}
	}
	function vcdetails_removeSuccess(){
		sync.log.trace("Entering VCScope.vcdetails.remove->vcdetails_removeSuccess function");

		if(!isError){
			kony.sync.verifyAndCallClosure(successcallback, rowsDeleted);
		}
	}
	function errorcallbackWrapper(){
		sync.log.trace("Entering VCScope.vcdetails.remove->error callback function");
		if(!isError){
			kony.sync.showTransactionError(errorcallback);
		}
		if(kony.sync.errorObject != null){
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
		}
	}
	function deleteEntity(){
		sync.log.trace("Entering VCScope.vcdetails.remove->delete Entity function");
		var connection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
		if(connection != null){
			kony.sync.startTransaction(connection, vcdetails_removeTransactioncallback, vcdetails_removeSuccess, errorcallbackWrapper);
		}
	}
	deleteEntity();
};

/************************************************************************************
* Retrieves vcdetails using primary key from the local Database. 
*************************************************************************************/
VCScope.vcdetails.prototype.getAllDetailsByPK = function(successcallback,errorcallback){
	sync.log.trace("Entering VCScope.vcdetails.prototype.getAllDetailsByPK function");
	var pks = this.getPKTable();
	VCScope.vcdetails.getAllDetailsByPK(pks,successcallback,errorcallback);
};
VCScope.vcdetails.getAllDetailsByPK = function(pks, successcallback,errorcallback){
	sync.log.trace("Entering VCScope.vcdetails.getAllDetailsByPK-> main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "VCScope.vcdetails.getAllDetailsByPK",  "getAllDetailsByPK", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = VCScope.vcdetails.getTableName();
	var wcs = [];
	if(VCScope.vcdetails.pkCheck(pks,wcs,errorcallback,"searching")===false){
		return;
	}
	twcs = kony.sync.CreateCopy(wcs);
	var query = kony.sync.qb_createQuery();
				kony.sync.qb_select(query, null);
				kony.sync.qb_from(query, tbname);
				kony.sync.qb_where(query, wcs);
	var query_compile = kony.sync.qb_compile(query);
	var sql = query_compile[0];
	var params = query_compile[1];
	function mySuccCallback(res){
		sync.log.trace("Entering VCScope.vcdetails.getAllDetailsByPK-> success callback function");
		successcallback(VCScope.vcdetails.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
	}	
	kony.sync.single_select_execute(dbname, sql, params, mySuccCallback, errorcallback);
};






/************************************************************************************
* Retrieves vcdetails(s) using where clause from the local Database. 
* e.g. VCScope.vcdetails.find("where regno like 'A%'", successcallback,errorcallback);
*************************************************************************************/
VCScope.vcdetails.find = function(wcs, successcallback,errorcallback){
	sync.log.trace("Entering VCScope.vcdetails.find function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "VCScope.vcdetails.find",  "find", errorcallback)){
		return;
	}
	//wcs will be a string formed by the user.
	var dbname = kony.sync.getDBName();
	var tbname = VCScope.vcdetails.getTableName();
	wcs = kony.sync.validateWhereClause(wcs);
	var sql = "select * from \"" + tbname + "\" " + wcs;
	function mySuccCallback(res){
		kony.sync.verifyAndCallClosure(successcallback, VCScope.vcdetails.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
	}
	kony.sync.single_select_execute(dbname, sql, null, mySuccCallback, errorcallback);
};

/************************************************************************************
* Marks instance of vcdetails with given primary key for upload. This will 
* enable deferred records to merge with the enterprise datasource in the next Sync.
*************************************************************************************/
VCScope.vcdetails.prototype.markForUploadbyPK = function(successcallback, errorcallback){
	sync.log.trace("Entering VCScope.vcdetails.prototype.markForUploadbyPK function");
	var pks = this.getPKTable();
	VCScope.vcdetails.markForUploadbyPK(pks, successcallback, errorcallback);
};
VCScope.vcdetails.markForUploadbyPK = function(pks, successcallback, errorcallback){
	sync.log.trace("Entering VCScope.vcdetails.markForUploadbyPK function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "VCScope.vcdetails.markForUploadbyPK",  "markForUploadbyPK", errorcallback)){
		return;
	}	
	var dbname = kony.sync.getDBName();
	var tbname = VCScope.vcdetails.getTableName();
	var isError = false;
	var recordsFound = false;
	var recordsMarkedForUpload = 0;
	var wcs = [];
	if(VCScope.vcdetails.pkCheck(pks, wcs, errorcallback, "marking for upload by PK")===false){
		return;
	}

	function markRecordForUpload(tx, record){
		var versionMapMain = [];
		versionMapMain[kony.sync.mainTableChangeTypeColumn] = kony.sync.getChangeTypeForUploadTrue(record[kony.sync.historyTableChangeTypeColumn]);
		var scopename = kony.sync.scopes.syncTableScopeDic[tbname];
		var versionNo = kony.sync.getseqnumber(tx, scopename);
		if(versionNo === false){
			return false;
		}
		versionMapMain[kony.sync.historyTableSyncVersionColumn] = versionNo.versionnumber;
		var query = kony.sync.qb_createQuery();
					kony.sync.qb_update(query, tbname);
					kony.sync.qb_set(query,versionMapMain);
					kony.sync.qb_where(query, wcs);
		var query_compile = kony.sync.qb_compile(query);
		var sql = query_compile[0];
		var params = query_compile[1];
		return kony.sync.executeSql(tx, sql, params);		
	}
	
	function markRecordForUploadHistory(tx, record){
		var versionMap = [];
		versionMap[kony.sync.historyTableChangeTypeColumn] = kony.sync.getChangeTypeForUploadTrue(record[kony.sync.historyTableChangeTypeColumn]);
		var scopename = kony.sync.scopes.syncTableScopeDic[tbname];
		var versionNo = kony.sync.getseqnumber(tx, scopename);
		if(versionNo === false){
			return false;
		}
		var twcs = [];
		twcs = wcs;
		kony.table.insert(twcs,{key : kony.sync.historyTableChangeTypeColumn, value : record[kony.sync.historyTableChangeTypeColumn], optype : "EQ",comptype : "AND"});
		versionMap[kony.sync.historyTableSyncVersionColumn] = versionNo.versionnumber;
		var query = kony.sync.qb_createQuery();
					kony.sync.qb_update(query, tbname + kony.sync.historyTableName);
					kony.sync.qb_set(query,versionMap);
					kony.sync.qb_where(query, twcs);
		kony.table.remove(twcs);
		var query_compile = kony.sync.qb_compile(query);
		var sql = query_compile[0];
		var params = query_compile[1];
		return kony.sync.executeSql(tx, sql, params);
	}
	
	function single_transaction_callback (tx){
		var query = kony.sync.qb_createQuery();
					kony.sync.qb_select(query, [kony.sync.historyTableChangeTypeColumn]);
					kony.sync.qb_from(query, tbname);
					kony.sync.qb_where(query, wcs);
		var query_compile = kony.sync.qb_compile(query);
		var sql = query_compile[0];
		var params = query_compile[1];
		var resultSet = kony.sync.executeSql(tx, sql, params);
		if(resultSet === false){
			isError = true;
			return;
		}

		var num_records = resultSet.rows.length;
		if(num_records > 0){
			recordsFound = true;
			var record = kony.db.sqlResultsetRowItem(tx, resultSet, 0);
			var changeType = record[kony.sync.mainTableChangeTypeColumn];
			if(!kony.sync.isNullOrUndefined(changeType) && kony.string.startsWith(""+changeType,"9")){
				recordsMarkedForUpload = 1;
				if(markRecordForUpload(tx, record) === false){
					isError = true;
					return;
				}
			}
		}
					
				
		var query1 =kony.sync.qb_createQuery();
					kony.sync.qb_select(query1, [kony.sync.historyTableChangeTypeColumn]);
					kony.sync.qb_from(query1, tbname + kony.sync.historyTableName);
					kony.sync.qb_where(query1, wcs);
		var query1_compile = kony.sync.qb_compile(query1);
		var sql1 = query1_compile[0];
		var params1 = query1_compile[1];
		var resultSet1 = kony.sync.executeSql (tx, sql1, params1);
		if(resultSet1!==false){
			var num_records = resultSet1.rows.length;
			for(var i = 0; i <= num_records - 1; i++ ){
				var record = kony.db.sqlResultsetRowItem(tx, resultSet1, i);
				if(markRecordForUploadHistory(tx, record) === false){
					isError = true;
					return;
				}
				recordsFound = true;
			}
		}
		else{
			isError = true;
		}
	}
	function single_transaction_success_callback(){
		if(recordsFound === true){
			kony.sync.verifyAndCallClosure(successcallback , {count:recordsMarkedForUpload});
		}
		else{
			kony.sync.pkNotFoundErrCallback(errorcallback, tbname);
		}
	}
	
	function single_transaction_error_callback(res){
		if (!isError) {
			kony.sync.showTransactionError(errorcallback);
		}else{
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
		}
	}
	
	var connection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
	if(connection != null){
		kony.sync.startTransaction(connection, single_transaction_callback, single_transaction_success_callback, single_transaction_error_callback);
	}
};

/************************************************************************************
* Marks instance(s) of vcdetails matching given where clause for upload. This will 
* enable deferred records to merge with the enterprise datasource in the next Sync.
*************************************************************************************/
VCScope.vcdetails.markForUpload = function(wcs, successcallback,errorcallback){
	sync.log.trace("Entering VCScope.vcdetails.markForUpload->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "VCScope.vcdetails.markForUpload",  "markForUpload", errorcallback)){
		return;
	}	
	var dbname = kony.sync.getDBName();
	var tbname = VCScope.vcdetails.getTableName();
	var isError = false;
	var num_records_main = 0;
	wcs = kony.sync.validateWhereClause(wcs);
	if(!kony.sync.isNull(wcs) && !kony.sync.isEmptyString(wcs)) {
		wcs = wcs + " and " + kony.sync.historyTableChangeTypeColumn + " like '9%'";
	}else{	
		wcs = "where " + kony.sync.historyTableChangeTypeColumn + " like '9%'";
	}
	
	function markRecordForUpload(tx, record){
		var versionMapMain = [];
		versionMapMain[kony.sync.mainTableChangeTypeColumn] = kony.sync.getChangeTypeForUploadTrue(record[kony.sync.historyTableChangeTypeColumn]);
		var scopename = kony.sync.scopes.syncTableScopeDic[tbname];
		var versionNo = kony.sync.getseqnumber(tx, scopename);
		if(versionNo === false){
			return false;
		}
		versionMapMain[kony.sync.historyTableSyncVersionColumn] = versionNo.versionnumber;
		var query = kony.sync.qb_createQuery();
					kony.sync.qb_update(query, tbname);
					kony.sync.qb_set(query,versionMapMain);
		var query_compile = kony.sync.qb_compile(query);
		var sql = query_compile[0] + " " + wcs;
		var params = query_compile[1];
		if(kony.sync.executeSql(tx, sql, params) === false){
			return false;
		}
	}
	
	function markRecordForUploadHistory(tx, record){
		var versionMap = [];
		versionMap[kony.sync.historyTableChangeTypeColumn] = kony.sync.getChangeTypeForUploadTrue(record[kony.sync.historyTableChangeTypeColumn]);
		var scopename = kony.sync.scopes.syncTableScopeDic[tbname];
		var versionNo = kony.sync.getseqnumber(tx, scopename);
		if(versionNo === false){
			return false;
		}
		var twcs = "";
		twcs = wcs;
		twcs = twcs + " AND " + kony.sync.historyTableChangeTypeColumn + " = " + record[kony.sync.historyTableChangeTypeColumn];
		versionMap[kony.sync.historyTableSyncVersionColumn] = versionNo.versionnumber;
		
		var query = kony.sync.qb_createQuery();
					kony.sync.qb_update(query, tbname + kony.sync.historyTableName);
					kony.sync.qb_set(query,versionMap);
		var query_compile = kony.sync.qb_compile(query);
		var sql = query_compile[0]  + " " + twcs;
		var params = query_compile[1];
		if(kony.sync.executeSql(tx, sql, params) === false){
			return false;
		}
	}
	
	function single_transaction_callback (tx){
		sync.log.trace("Entering VCScope.vcdetails.markForUpload->single_transaction_callback");
		//updating main table
		var sql = "select " + kony.sync.historyTableChangeTypeColumn + " from \"" + tbname + "\" " + wcs ;
		var resultSet = kony.sync.executeSql (tx, sql, null);
		if(resultSet === false){
			isError = true;
			return;
		}
		
		num_records_main = resultSet.rows.length;
		for(var i = 0; i < num_records_main; i++ ){
			var record = kony.db.sqlResultsetRowItem(tx, resultSet, i);
			if(markRecordForUpload(tx, record) === false){
				isError = true;
				return;
			}
		}
		
		//updating history table
		var sql = "select " + kony.sync.historyTableChangeTypeColumn + " from " + tbname + kony.sync.historyTableName + " " + wcs;
		var resultSet = kony.sync.executeSql (tx, sql, null);
		if(resultSet === false){
			isError = true;
			return;
		}

		var num_records = resultSet.rows.length;
		for ( var i = 0; i <= num_records - 1; i++ ){
			var record = kony.db.sqlResultsetRowItem(tx, resultSet, i);
			if(markRecordForUploadHistory(tx, record)=== false){
				isError = true;
				return;
			}
		}
	}
	
	function single_transaction_success_callback(){
		sync.log.trace("Entering VCScope.vcdetails.markForUpload->single_transaction_success_callback");
		kony.sync.verifyAndCallClosure(successcallback, {count:num_records_main});
	}
	
	function single_transaction_error_callback(){
		sync.log.error("Entering VCScope.vcdetails.markForUpload->single_transaction_error_callback");
		if(!isError) {
			kony.sync.showTransactionError(errorcallback);
		}else{
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
		}
	}
	
	var connection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
	if(connection != null){
		kony.sync.startTransaction(connection, single_transaction_callback, single_transaction_success_callback, single_transaction_error_callback);
	}
};

/************************************************************************************
* Retrieves instance(s) of vcdetails pending for upload. Records are marked for
* pending upload if they have been updated or created locally and the changes have
* not been merged with enterprise datasource.
*************************************************************************************/
VCScope.vcdetails.getPendingUpload = function(wcs, successcallback,errorcallback){
	sync.log.trace("Entering VCScope.vcdetails.getPendingUpload->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = VCScope.vcdetails.getTableName();
	var currentversion = kony.sync.getCurrentVersionNumber(tbname);
	var sql;
	if(typeof(wcs) === "string" && wcs != null){
		wcs = kony.sync.validateWhereClause(wcs);
		sql = "select * from \"" + tbname + "\" "+ wcs + " and " + kony.sync.mainTableChangeTypeColumn + " is not null AND "+kony.sync.mainTableChangeTypeColumn+" <> -1 AND "+kony.sync.mainTableSyncVersionColumn+" = "+currentversion+" AND "+kony.sync.mainTableChangeTypeColumn+" NOT LIKE '9%'"; 
	}else{
		errorcallback = successcallback;
		successcallback = wcs;
		sql = "select * from \"" + tbname + "\" WHERE " + kony.sync.mainTableChangeTypeColumn + " is not null AND "+kony.sync.mainTableChangeTypeColumn+" <> -1 AND "+kony.sync.mainTableSyncVersionColumn+" = "+currentversion+" AND "+kony.sync.mainTableChangeTypeColumn+" NOT LIKE '9%'"; 
	}
	kony.sync.single_select_execute(dbname, sql, null, mySuccesscallback, errorcallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering VCScope.vcdetails.getPendingUpload->successcallback function");
		kony.sync.verifyAndCallClosure(successcallback, VCScope.vcdetails.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
	}
};

/************************************************************************************
* Retrieves instance(s) of vcdetails pending for acknowledgement. This is relevant
* when the SyncObject is part of the SyncScope whose SyncStrategy is PersistentSync.
* In persistent Sync the  records in the local database are put into a pending 
* acknowledgement state after an upload.
*************************************************************************************/
VCScope.vcdetails.getPendingAcknowledgement = function(successcallback, errorcallback){
	sync.log.trace("Entering VCScope.vcdetails.getPendingAcknowledgement->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = VCScope.vcdetails.getTableName();
	var currentversion = kony.sync.getCurrentVersionNumber(tbname);
	var mysql="select * from \""+tbname+"\" WHERE "+kony.sync.mainTableChangeTypeColumn+ " is not null AND "+kony.sync.mainTableChangeTypeColumn+" <> -1 AND "+kony.sync.mainTableSyncVersionColumn+" <> "+currentversion+" AND "+kony.sync.mainTableChangeTypeColumn+" NOT LIKE '9%'"; 
	kony.sync.single_select_execute(dbname, mysql, null, mySuccesscallback, errorcallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering VCScope.vcdetails.getPendingAcknowledgement success callback function");
		kony.sync.verifyAndCallClosure(successcallback, VCScope.vcdetails.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
	}
};

/************************************************************************************
* Retrieves instance(s) of vcdetails deferred for upload.
*************************************************************************************/
VCScope.vcdetails.getDeferredUpload = function(wcs,successcallback,errorcallback){
	sync.log.trace("Entering VCScope.vcdetails.getDeferredUpload->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = VCScope.vcdetails.getTableName();
	var sql;
	if(typeof(wcs) === "string" && wcs != null ){
		wcs = kony.sync.validateWhereClause(wcs);
		sql = "select * from \"" + tbname +  "\" " + wcs + " and " + kony.sync.mainTableChangeTypeColumn+ " is not null AND "+kony.sync.mainTableChangeTypeColumn+" <> -1 AND "+kony.sync.mainTableChangeTypeColumn+" LIKE '9%'";
	}else{
		errorcallback = successcallback;
		successcallback = wcs;
		sql="select * from \""+tbname+"\" WHERE "+kony.sync.mainTableChangeTypeColumn+ " is not null AND "+kony.sync.mainTableChangeTypeColumn+" <> -1 AND "+kony.sync.mainTableChangeTypeColumn+" LIKE '9%'"; 
	}
	
	kony.sync.single_select_execute(dbname, sql, null, mySuccesscallback, errorcallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering VCScope.vcdetails.getDeferredUpload->success callback function");
		kony.sync.verifyAndCallClosure(successcallback, VCScope.vcdetails.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
	}	
};

/************************************************************************************
* Rollbacks all changes to vcdetails in local database to last synced state
*************************************************************************************/
VCScope.vcdetails.rollbackPendingLocalChanges = function(successcallback, errorcallback){
	sync.log.trace("Entering VCScope.vcdetails.rollbackPendingLocalChanges->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = VCScope.vcdetails.getTableName();
	kony.sync.konySyncRollBackPendingChanges(tbname, dbname, null, mySuccesscallback, errorcallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering VCScope.vcdetails.rollbackPendingLocalChanges->main function");
		kony.sync.verifyAndCallClosure(successcallback, res);
	}		
};

/************************************************************************************
* Rollbacks changes to vcdetails's record with given primary key in local 
* database to last synced state
*************************************************************************************/
VCScope.vcdetails.prototype.rollbackPendingLocalChangesByPK = function(successcallback,errorcallback){
	sync.log.trace("Entering VCScope.vcdetails.prototype.rollbackPendingLocalChangesByPK function");
	var pks = this.getPKTable();
	VCScope.vcdetails.rollbackPendingLocalChangesByPK(pks,successcallback,errorcallback);
};
VCScope.vcdetails.rollbackPendingLocalChangesByPK = function(pks, successcallback, errorcallback){
	sync.log.trace("Entering VCScope.vcdetails.rollbackPendingLocalChangesByPK->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "VCScope.vcdetails.rollbackPendingLocalChangesByPK",  "rollbackPendingLocalChangesByPK", errorcallback)){
		return;
	}	
	var dbname = kony.sync.getDBName();
	var tbname = VCScope.vcdetails.getTableName();
	var wcs = [];
	if(VCScope.vcdetails.pkCheck(pks,wcs,errorcallback,"rollbacking")===false){
		return;
	}	
	kony.sync.konySyncRollBackPendingChanges(tbname, dbname, wcs, mySuccesscallback, pkNotFoundErrCallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering VCScope.vcdetails.rollbackPendingLocalChangesByPK->success callback function");
		kony.sync.verifyAndCallClosure(successcallback, res);
	}	
	function pkNotFoundErrCallback(){
		sync.log.error("Entering VCScope.vcdetails.rollbackPendingLocalChangesByPK->PK not found callback");
		kony.sync.pkNotFoundErrCallback(errorcallback,tbname);
	}
};

/************************************************************************************
* isRecordDeferredForUpload returns true or false depending on whether vcdetails's record  
* with given primary key got deferred in last sync
*************************************************************************************/
VCScope.vcdetails.prototype.isRecordDeferredForUpload = function(successcallback,errorcallback){
	sync.log.trace("Entering  VCScope.vcdetails.prototype.isRecordDeferredForUpload function");
	var pks = this.getPKTable();
	VCScope.vcdetails.isRecordDeferredForUpload(pks,successcallback,errorcallback);
};
VCScope.vcdetails.isRecordDeferredForUpload = function(pks, successcallback, errorcallback){
	sync.log.trace("Entering VCScope.vcdetails.isRecordDeferredForUpload->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "VCScope.vcdetails.isRecordDeferredForUpload",  "isRecordDeferredForUpload", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = VCScope.vcdetails.getTableName();
	var wcs = [] ;
	var flag;
	if(VCScope.vcdetails.pkCheck(pks,wcs,errorcallback,"selecting")===false){
		return;
	}
	var twcs = [];
	twcs = kony.sync.CreateCopy(wcs);
	kony.table.insert(twcs, {
			key : kony.sync.mainTableChangeTypeColumn,
			value : "9%",
			optype : "LIKE",
			comptype : "AND"
		});
	var query = kony.sync.qb_createQuery();
				kony.sync.qb_select(query, null);
				kony.sync.qb_from(query, tbname);
				kony.sync.qb_where(query, twcs);
	var query_compile = kony.sync.qb_compile(query);
	var sql = query_compile[0];
	var params = query_compile[1];
	kony.sync.single_select_execute(dbname, sql, params, mySuccesscallback, errorcallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering VCScope.vcdetails.isRecordDeferredForUpload->successcallback function");
		if(res.length === 1){
			flag = true;
		}
		else{
			flag = false;
		}
		kony.sync.verifyAndCallClosure(successcallback, {deferred:flag});
	}
};

/************************************************************************************
* isRecordPendingForUpload returns true or false depending on whether vcdetails's record  
* with given primary key is pending for upload
*************************************************************************************/
VCScope.vcdetails.prototype.isRecordPendingForUpload = function(successcallback,errorcallback){
	sync.log.trace("Entering  VCScope.vcdetails.prototype.isRecordPendingForUpload function");
	var pks = this.getPKTable();
	VCScope.vcdetails.isRecordPendingForUpload(pks,successcallback,errorcallback);
};
VCScope.vcdetails.isRecordPendingForUpload = function(pks, successcallback, errorcallback){
	sync.log.trace("Entering VCScope.vcdetails.isRecordPendingForUpload->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "VCScope.vcdetails.isRecordPendingForUpload",  "isRecordPendingForUpload", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = VCScope.vcdetails.getTableName();
	var wcs = [] ;
	var flag;
	if(VCScope.vcdetails.pkCheck(pks,wcs,errorcallback,"selecting")===false){
		return;
	}
	var twcs = [];
	twcs = kony.sync.CreateCopy(wcs);
	kony.table.insert(twcs, {
			key : kony.sync.mainTableChangeTypeColumn,
			value : "9%",
			optype : "NOT LIKE",
			comptype : "AND"
		});
	var query = kony.sync.qb_createQuery();
				kony.sync.qb_select(query, null);
				kony.sync.qb_from(query, tbname);
				kony.sync.qb_where(query, twcs);
	var query_compile = kony.sync.qb_compile(query);
	var sql = query_compile[0];
	var params = query_compile[1];
	kony.sync.single_select_execute(dbname, sql, params, mySuccesscallback, errorcallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering VCScope.vcdetails.isRecordPendingForUpload->successcallback function");
		if(res.length === 1){
			flag = true;
		}
		else{
			flag = false;
		}
		kony.sync.verifyAndCallClosure(successcallback, {pending:flag});
	}
};




/************************************************************************************
* Start of helper functions used internally, not to be used as ORMs
*************************************************************************************/

//Deletes all the dependant tables in the relationship tables.Need to pass transaction handler as input
VCScope.vcdetails.removeCascade = function(tx, wcs, errorcallback, markForUpload, isCascade, parentTable, isLocal){
	sync.log.trace("Entering VCScope.vcdetails.removeCascade function");
	var tbname = VCScope.vcdetails.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);
	function removeCascadeChildren(){
	}
	if(isCascade){
		if(removeCascadeChildren()===false){
			return false;
		}
		if(kony.sync.deleteBatch(tx, tbname, wcs, isLocal,markForUpload, null)===false){
			return false;
		}
		return true;
	}else{
		var sql = "select * from \"" + tbname + "\" " + wcs;
		var resultSet = kony.sync.executeSql(tx, sql, null);
		if(resultSet===false){
			return false;
		}	
		var num_records = resultSet.rows.length;
		if(num_records === 0){
			return true;
		}else{
			sync.log.error(kony.sync.getReferetialIntegrityDeleteErrMessg(tbname,tbname,tbname,parentTable));
			errorcallback(kony.sync.getErrorTable(kony.sync.errorCodeReferentialIntegrity,kony.sync.getReferetialIntegrityDeleteErrMessg(tbname,tbname,tbname,parentTable)));
			return false;
		}
	}
};


VCScope.vcdetails.convertTableToObject = function(res){
	sync.log.trace("Entering VCScope.vcdetails.convertTableToObject function");
	objMap = [];
	if(res!==null){
		for(var i in res){
			var obj = new VCScope.vcdetails();
			obj.vcId = res[i].vcId;
			obj.regno = res[i].regno;
			obj.email = res[i].email;
			obj.location = res[i].location;
			obj.dateField = res[i].dateField;
			obj.isDeleted = res[i].isDeleted;
			obj.lastModifiedDate = res[i].lastModifiedDate;
			obj.image = res[i].image;
			obj.latitude = res[i].latitude;
			obj.longitude = res[i].longitude;
			obj.accuracy = res[i].accuracy;
			obj.timestampField = res[i].timestampField;
			obj.mileage = res[i].mileage;
			obj.branchNumber = res[i].branchNumber;
			obj.markForUpload = (Math.floor(res[i].konysyncchangetype/10)==9)? false:true;
			objMap[i] = obj;
		}
	}
	return objMap;
};

VCScope.vcdetails.filterAttributes = function(valuestable, insert){
	sync.log.trace("Entering VCScope.vcdetails.filterAttributes function");
	var attributeTable = {};
	attributeTable.vcId = "vcId";
	attributeTable.regno = "regno";
	attributeTable.email = "email";
	attributeTable.location = "location";
	attributeTable.dateField = "dateField";
	attributeTable.image = "image";
	attributeTable.latitude = "latitude";
	attributeTable.longitude = "longitude";
	attributeTable.accuracy = "accuracy";
	attributeTable.timestampField = "timestampField";
	attributeTable.mileage = "mileage";
	attributeTable.branchNumber = "branchNumber";

	var PKTable = {};
	PKTable.vcId = {}
	PKTable.vcId.name = "vcId";
	PKTable.vcId.isAutoGen = true;
	var newvaluestable = {};
	for (var k in valuestable){
		var v = valuestable[k];
		if(kony.sync.isNull(attributeTable[k])) { 
			sync.log.warn("Ignoring the attribute " + k + " for the SyncObject vcdetails. "  + k + " not defined as an attribute in SyncConfiguration.");
		}else if(!kony.sync.isNull(PKTable[k])) {
			if(insert===false){
				sync.log.warn("Ignoring the primary key " + k + " for the SyncObject vcdetails. Primary Key should not be the part of the attributes to be updated in the local device database.");
			}else if(PKTable[k].isAutoGen){
				sync.log.warn("Ignoring the auto-generated primary key " + k + " for the SyncObject vcdetails. Auto-generated Primary Key should not be the part of the attributes to be inserted in the local device database.");
			}else{
				newvaluestable[k] = v;
			}
		}
		else{
			newvaluestable[k] = v;
		}
	}
	return newvaluestable;
};

VCScope.vcdetails.formOrderByClause = function(orderByMap){
	sync.log.trace("Entering VCScope.vcdetails.formOrderByClause function");
	if(!kony.sync.isNull(orderByMap)){
		var valuestable = kony.sync.convertOrderByMapToValuesTable(orderByMap);
		//var filteredValuestable = VCScope.vcdetails.filterAttributes(valuestable, true);
		return kony.sync.convertToValuesTableOrderByMap(orderByMap,valuestable);
	}
	return null;
};

VCScope.vcdetails.prototype.getValuesTable = function(isInsert){
	sync.log.trace("Entering VCScope.vcdetails.prototype.getValuesTable function");
	var valuesTable = {};
	if(isInsert===true){
		valuesTable.vcId = this.vcId;
	}
	valuesTable.regno = this.regno;
	valuesTable.email = this.email;
	valuesTable.location = this.location;
	valuesTable.dateField = this.dateField;
	valuesTable.image = this.image;
	valuesTable.latitude = this.latitude;
	valuesTable.longitude = this.longitude;
	valuesTable.accuracy = this.accuracy;
	valuesTable.timestampField = this.timestampField;
	valuesTable.mileage = this.mileage;
	valuesTable.branchNumber = this.branchNumber;
	return valuesTable;
};

VCScope.vcdetails.prototype.getPKTable = function(){
	sync.log.trace("Entering VCScope.vcdetails.prototype.getPKTable function");
	var pkTable = {};
	pkTable.vcId = {key:"vcId",value:this.vcId};
	return pkTable;
};

VCScope.vcdetails.getPKTable = function(){
	sync.log.trace("Entering VCScope.vcdetails.getPKTable function");
	var pkTable = [];
	pkTable.push("vcId");
	return pkTable;
};

VCScope.vcdetails.pkCheck = function(pks,wcs,errorcallback,opName){
	sync.log.trace("Entering VCScope.vcdetails.pkCheck function");
	var wc = [];
	if(kony.sync.isNull(pks)){
		sync.log.error("Primary Key vcId not specified in  " + opName + "  an item in vcdetails");
		kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodePrimaryKeyNotSpecified,kony.sync.getPrimaryKeyNotSpecifiedMsg("vcId",opName,"vcdetails")));
		return false;	
	}
	else if(kony.sync.isValidJSTable(pks)){
		if(!kony.sync.isNull(pks.vcId)){
			if(!kony.sync.isNull(pks.vcId.value)){
				wc.key = "vcId";
				wc.value = pks.vcId.value;
			}
			else{
				wc.key = "vcId";
				wc.value = pks.vcId;
			}
		}else{
			sync.log.error("Primary Key vcId not specified in  " + opName + "  an item in vcdetails");
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodePrimaryKeyNotSpecified,kony.sync.getPrimaryKeyNotSpecifiedMsg("vcId",opName,"vcdetails")));
			return false;
		}
	}
	else{
		wc.key = "vcId";
		wc.value = pks;
	}	
	kony.table.insert(wcs,wc);
	return true;
};

VCScope.vcdetails.validateNull = function (valuestable,errorcallback){
	sync.log.trace("Entering VCScope.vcdetails.validateNull function");
	return true;
};

VCScope.vcdetails.validateNullInsert = function (valuestable,errorcallback){
	sync.log.trace("Entering VCScope.vcdetails.validateNullInsert function");
	return true;
};

VCScope.vcdetails.getRelationshipMap = function(relationshipMap,valuestable){
	sync.log.trace("Entering VCScope.vcdetails.getRelationshipMap function");
	var r1 = {};

	return relationshipMap;
};


VCScope.vcdetails.checkPKValueTables = function (valuetables)	{
	var checkPksNotNullFlag = true;
	for(var i = 0; i < valuetables.length; i++)	{
		if(kony.sync.isNull(valuetables[i])){
			checkPksNotNullFlag = false;
			break;
		}
	}
	return checkPksNotNullFlag;
};

VCScope.vcdetails.getTableName = function(){
	return "vcdetails";
};




// **********************************End vcdetails's helper methods************************