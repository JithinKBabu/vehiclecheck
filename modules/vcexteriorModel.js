//****************Sync Version:MobileFabricInstaller-QA-7.2.0_v201610191639_r117*******************
// ****************Generated On Fri Nov 25 12:14:24 UTC 2016vcexterior*******************
// **********************************Start vcexterior's helper methods************************
if (typeof(kony) === "undefined") {
	kony = {};
}

if (typeof(kony.sync) === "undefined") {
	kony.sync = {};
}

if (typeof(kony.sync.log) === "undefined") {
	kony.sync.log = {};
}

if (typeof(sync) === "undefined") {
	sync = {};
}

if (typeof(sync.log) === "undefined") {
	sync.log = {};
}



if(typeof(VCScope)=== "undefined"){ VCScope = {}; }

/************************************************************************************
* Creates new vcexterior
*************************************************************************************/
VCScope.vcexterior = function(){
	this.extid = null;
	this.regno = null;
	this.email = null;
	this.TaxDisc = null;
	this.TyreCondition = null;
	this.WheelNuts = null;
	this.NumberPlate = null;
	this.BodyCondition = null;
	this.SideStep = null;
	this.isDeleted = null;
	this.lastModifiedDate = null;
	this.markForUpload = true;
};

VCScope.vcexterior.prototype = {
	get extid(){
		return this._extid;
	},
	set extid(val){
		if(!kony.sync.isEmptyString(val) && !kony.sync.isNull(val) && !kony.sync.isValidNumberType(val)){
			sync.log.error("Invalid data type for the attribute extid in vcexterior.\nExpected:\"integer\"\nActual:\"" + kony.type(val) + "\"");
		}
		this._extid = val;
	},
	get regno(){
		return this._regno;
	},
	set regno(val){
		this._regno = val;
	},
	get email(){
		return this._email;
	},
	set email(val){
		this._email = val;
	},
	get TaxDisc(){
		return this._TaxDisc;
	},
	set TaxDisc(val){
		this._TaxDisc = val;
	},
	get TyreCondition(){
		return this._TyreCondition;
	},
	set TyreCondition(val){
		this._TyreCondition = val;
	},
	get WheelNuts(){
		return this._WheelNuts;
	},
	set WheelNuts(val){
		this._WheelNuts = val;
	},
	get NumberPlate(){
		return this._NumberPlate;
	},
	set NumberPlate(val){
		this._NumberPlate = val;
	},
	get BodyCondition(){
		return this._BodyCondition;
	},
	set BodyCondition(val){
		this._BodyCondition = val;
	},
	get SideStep(){
		return this._SideStep;
	},
	set SideStep(val){
		this._SideStep = val;
	},
	get isDeleted(){
		return kony.sync.getBoolean(this._isDeleted)+"";
	},
	set isDeleted(val){
		if(!kony.sync.isEmptyString(val) && !kony.sync.isNull(val) && !kony.sync.isValidBooleanType(val)){
			sync.log.error("Invalid data type for the attribute isDeleted in vcexterior.\nExpected:\"boolean\"\nActual:\"" + kony.type(val) + "\"");
		}
		this._isDeleted = val;
	},
	get lastModifiedDate(){
		return this._lastModifiedDate;
	},
	set lastModifiedDate(val){
		this._lastModifiedDate = val;
	},
};

/************************************************************************************
* Retrieves all instances of vcexterior SyncObject present in local database with
* given limit and offset where limit indicates the number of records to be retrieved
* and offset indicates number of rows to be ignored before returning the records.
* e.g. var orderByMap = []
* orderByMap[0] = {};
* orderByMap[0].key = "extid";
* orderByMap[0].sortType ="desc";
* orderByMap[1] = {};
* orderByMap[1].key = "regno";
* orderByMap[1].sortType ="asc";
* var limit = 20;
* var offset = 5;
* VCScope.vcexterior.getAll(successcallback,errorcallback, orderByMap, limit, offset)
*************************************************************************************/
VCScope.vcexterior.getAll = function(successcallback, errorcallback, orderByMap, limit, offset){
	sync.log.trace("Entering VCScope.vcexterior.getAll->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = VCScope.vcexterior.getTableName();
	orderByMap = kony.sync.formOrderByClause("vcexterior",orderByMap);
	var query = kony.sync.qb_createQuery();
				kony.sync.qb_select(query, null);	
				kony.sync.qb_from(query, tbname);
				kony.sync.qb_orderBy(query, orderByMap);
				kony.sync.qb_limitOffset(query,limit,offset);
	var query_compile = kony.sync.qb_compile(query);
	var sql = query_compile[0];
	var params = query_compile[1];

	function mySuccCallback(res){
		sync.log.trace("Entering VCScope.vcexterior.getAll->successcallback");
		successcallback(VCScope.vcexterior.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
	}	
	kony.sync.single_select_execute(dbname,sql, params,mySuccCallback,errorcallback);
};

/************************************************************************************
* Returns number of vcexterior present in local database.
*************************************************************************************/
VCScope.vcexterior.getAllCount = function(successcallback,errorcallback){
	sync.log.trace("Entering VCScope.vcexterior.getAllCount function");
	VCScope.vcexterior.getCount("",successcallback,errorcallback);
};

/************************************************************************************
* Returns number of vcexterior using where clause in the local Database
*************************************************************************************/
VCScope.vcexterior.getCount = function(wcs,successcallback,errorcallback){
	sync.log.trace("Entering VCScope.vcexterior.getCount->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "VCScope.vcexterior.getCount" , "getCount", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = VCScope.vcexterior.getTableName();
	wcs = kony.sync.validateWhereClause(wcs);
	var sql = "select count(*) from \"" + tbname + "\" " + wcs;
	kony.sync.single_execute_sql(dbname,sql, null, mySuccCallback, errorcallback);
	function mySuccCallback(res) {
		sync.log.trace("Entering VCScope.vcexterior.getCount->successcallback");
		if(null!==res){
			var count = null;
			count = res["count(*)"];
			kony.sync.verifyAndCallClosure(successcallback, {count:count});
		}
		else{
			sync.log.error("Some error occured while getting the count");
		}
	}
};

/************************************************************************************
* Creates a new instance of vcexterior in the local Database. The new record will 
* be merged with the enterprise datasource in the next Sync.
*************************************************************************************/
VCScope.vcexterior.prototype.create = function(successcallback,errorcallback){
	sync.log.trace("Entering  VCScope.vcexterior.prototype.create function");
	var valuestable = this.getValuesTable(true);
	VCScope.vcexterior.create(valuestable, successcallback,errorcallback,this.markForUpload);
};
VCScope.vcexterior.create = function(valuestable, successcallback,errorcallback,markForUpload){
	sync.log.trace("Entering  VCScope.vcexterior.create->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	
	if(!kony.sync.validateInput(arguments, "VCScope.vcexterior.create" , "create", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = VCScope.vcexterior.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);

	if(kony.sync.attributeValidation(valuestable,"vcexterior",errorcallback,true)===false){
		return;
	}
	
	function executeSuccess(){
		sync.log.trace("Entering  VCScope.vcexterior.create->success callback");
		kony.sync.single_insert_execute(dbname,tbname,valuestable,successcallback,errorcallback,markForUpload);
	}

	if(kony.sync.enableORMValidations){
		var relationshipMap={};  
		relationshipMap = VCScope.vcexterior.getRelationshipMap(relationshipMap,valuestable);
		kony.sync.checkIntegrity(dbname,relationshipMap,executeSuccess,errorcallback);
	}
	else{
		kony.sync.single_insert_execute(dbname,tbname,valuestable,successcallback,errorcallback,markForUpload);
	}
};

/************************************************************************************
* Creates number of new instances of vcexterior in the local Database. These new 
* records will be merged with the enterprise datasource in the next Sync. Based upon 
* kony.sync.enableORMValidations flag, validations will be enabled/disabled.
* e.g.	var valuesArray = [];
*		valuesArray[0] = {};
*		valuesArray[0].regno = "regno_0";
*		valuesArray[0].email = "email_0";
*		valuesArray[0].TaxDisc = "TaxDisc_0";
*		valuesArray[0].TyreCondition = "TyreCondition_0";
*		valuesArray[0].WheelNuts = "WheelNuts_0";
*		valuesArray[0].NumberPlate = "NumberPlate_0";
*		valuesArray[0].BodyCondition = "BodyCondition_0";
*		valuesArray[0].SideStep = "SideStep_0";
*		valuesArray[1] = {};
*		valuesArray[1].regno = "regno_1";
*		valuesArray[1].email = "email_1";
*		valuesArray[1].TaxDisc = "TaxDisc_1";
*		valuesArray[1].TyreCondition = "TyreCondition_1";
*		valuesArray[1].WheelNuts = "WheelNuts_1";
*		valuesArray[1].NumberPlate = "NumberPlate_1";
*		valuesArray[1].BodyCondition = "BodyCondition_1";
*		valuesArray[1].SideStep = "SideStep_1";
*		valuesArray[2] = {};
*		valuesArray[2].regno = "regno_2";
*		valuesArray[2].email = "email_2";
*		valuesArray[2].TaxDisc = "TaxDisc_2";
*		valuesArray[2].TyreCondition = "TyreCondition_2";
*		valuesArray[2].WheelNuts = "WheelNuts_2";
*		valuesArray[2].NumberPlate = "NumberPlate_2";
*		valuesArray[2].BodyCondition = "BodyCondition_2";
*		valuesArray[2].SideStep = "SideStep_2";
*		VCScope.vcexterior.createAll(valuesArray, successcallback, errorcallback, true);
*************************************************************************************/
VCScope.vcexterior.createAll = function(valuesArray, successcallback, errorcallback, markForUpload){
	sync.log.trace("Entering VCScope.vcexterior.createAll function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "VCScope.vcexterior.createAll" , "createAll", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = VCScope.vcexterior.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);
	var isProperData = true;
	var arrayLen = 0;
	var errorInfo = [];
	var arrayLength = valuesArray.length;
	var errObject = null;
	var isReferentialIntegrityFailure = false;
	var errMsg = null;
	if(kony.sync.enableORMValidations){
		var newValuesArray = [];

		//column level validations
		for (var i=0; valuesArray != null && i < arrayLength; i++ ){
			var valuestable = valuesArray[i];
			if(kony.sync.attributeValidation(valuestable,"vcexterior",errorcallback,true)===false){
				return;
			}

			newValuesArray[i] = valuestable;
		}
		valuesArray = newValuesArray;
		var connection = kony.sync.getConnectionOnly(dbname, dbname);
		kony.sync.startTransaction(connection, checkIntegrity, transactionSuccessCallback, transactionErrorCallback);
		var isError = false;
	}
	else{
		//copying by value
		var newValuesArray = [];
		arrayLength = valuesArray.length;
		for (var i=0; valuesArray != null && i < arrayLength; i++ ){
			newValuesArray[i] = kony.sync.CreateCopy(valuesArray[i]);
		}
		valuesArray = newValuesArray;
		kony.sync.massInsert(dbname, tbname, valuesArray, successcallback, errorcallback, markForUpload);
	}

	function transactionErrorCallback(){
		if(isError==true){
			//Statement error has occurred
				kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
		}
		else{
			//Transaction error has occurred
				kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodeTransaction, kony.sync.getErrorMessage(kony.sync.errorCodeTransaction), null));
		}
	}

	function transactionSuccessCallback(){
		sync.log.trace("Entering  VCScope.vcexterior.createAll->transactionSuccessCallback");
		if(!isError){
			kony.sync.massInsert(dbname, tbname, valuesArray, successcallback, errorcallback, markForUpload);
		}
		else{
			if(isReferentialIntegrityFailure){
				kony.sync.verifyAndCallClosure(errorcallback, errObject);
			}
		}
	}
	
	//foreign key constraints validations
	function checkIntegrity(tx){
		sync.log.trace("Entering  VCScope.vcexterior.createAll->checkIntegrity");
		arrayLength = valuesArray.length;
		for (var i=0; valuesArray != null && i < arrayLength; i++ ){
			var relationshipMap={};  
			relationshipMap = VCScope.vcexterior.getRelationshipMap(relationshipMap,valuesArray[i]);
			errObject = kony.sync.checkIntegrityinTransaction(tx, relationshipMap, null);
			if(errObject===false){
				isError = true;
				return; 
			}
			if(errObject!==true){
				isError = true;
				isReferentialIntegrityFailure = true;
				return;
			}
		}
	}
};
/************************************************************************************
* Updates vcexterior using primary key in the local Database. The update will be
* merged with the enterprise datasource in the next Sync.
*************************************************************************************/
VCScope.vcexterior.prototype.updateByPK = function(successcallback,errorcallback){
	sync.log.trace("Entering  VCScope.vcexterior.prototype.updateByPK function");
	var pks = this.getPKTable();
	var valuestable = this.getValuesTable(false);
	VCScope.vcexterior.updateByPK(pks,valuestable, successcallback,errorcallback,this.markForUpload);
};
VCScope.vcexterior.updateByPK = function(pks,valuestable, successcallback,errorcallback, markForUpload){
	sync.log.trace("Entering  VCScope.vcexterior.updateByPK-> main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "VCScope.vcexterior.updateByPK",  "updateByPk", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = VCScope.vcexterior.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);
	var wcs = [ ];

	if(VCScope.vcexterior.pkCheck(pks,wcs,errorcallback,"updating")===false){
		return;
	}

	if(kony.sync.attributeValidation(valuestable,"vcexterior",errorcallback,false)===false){
		return;
	}

	var relationshipMap={};  
	relationshipMap = VCScope.vcexterior.getRelationshipMap(relationshipMap,valuestable);

	kony.sync.updateByPK(tbname, dbname, relationshipMap, pks,valuestable, successcallback,errorcallback, markForUpload, wcs);
};

/************************************************************************************
* Updates vcexterior(s) using where clause in the local Database. The update(s)
* will be merged with the enterprise datasource in the next Sync.
*************************************************************************************/
VCScope.vcexterior.update = function(wcs, valuestable, successcallback,errorcallback,markForUpload){
	sync.log.trace("Entering VCScope.vcexterior.update function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "VCScope.vcexterior.update",  "update", errorcallback)){
		return;
	}

	var dbname = kony.sync.getDBName();
	var tbname = VCScope.vcexterior.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);
	wcs = kony.sync.validateWhereClause(wcs);

	if(kony.sync.attributeValidation(valuestable,"vcexterior",errorcallback,false)===false){
		return;
	}
	function executeSuccess(){
		sync.log.trace("Entering  VCScope.vcexterior.update-> success callback of Integrity Check");
		kony.sync.single_update_execute(dbname,tbname,valuestable,wcs,successcallback,errorcallback,true, markForUpload, VCScope.vcexterior.getPKTable());
	}

	if(kony.sync.enableORMValidations){
		var relationshipMap={};  
		relationshipMap = VCScope.vcexterior.getRelationshipMap(relationshipMap,valuestable);
		kony.sync.checkIntegrity(dbname,relationshipMap,executeSuccess,errorcallback);
	}
	else{
		kony.sync.single_update_execute(dbname,tbname,valuestable,wcs,successcallback,errorcallback,true, markForUpload, VCScope.vcexterior.getPKTable());
	}
};

/************************************************************************************
* Updates vcexterior(s) satisfying one or more where clauses in the local Database. 
* The update(s) will be merged with the enterprise datasource in the next Sync.
* Based upon kony.sync.enableORMValidations flag, validations will be enabled/disabled.
* e.g.	var inputArray = [];
*		inputArray[0] = {};
*		inputArray[0].changeSet = {};
*		inputArray[0].changeSet.regno = "regno_updated0";
*		inputArray[0].changeSet.email = "email_updated0";
*		inputArray[0].changeSet.TaxDisc = "TaxDisc_updated0";
*		inputArray[0].changeSet.TyreCondition = "TyreCondition_updated0";
*		inputArray[0].whereClause = "where extid = 0";
*		inputArray[1] = {};
*		inputArray[1].changeSet = {};
*		inputArray[1].changeSet.regno = "regno_updated1";
*		inputArray[1].changeSet.email = "email_updated1";
*		inputArray[1].changeSet.TaxDisc = "TaxDisc_updated1";
*		inputArray[1].changeSet.TyreCondition = "TyreCondition_updated1";
*		inputArray[1].whereClause = "where extid = 1";
*		inputArray[2] = {};
*		inputArray[2].changeSet = {};
*		inputArray[2].changeSet.regno = "regno_updated2";
*		inputArray[2].changeSet.email = "email_updated2";
*		inputArray[2].changeSet.TaxDisc = "TaxDisc_updated2";
*		inputArray[2].changeSet.TyreCondition = "TyreCondition_updated2";
*		inputArray[2].whereClause = "where extid = 2";
*		VCScope.vcexterior.updateAll(inputArray,successcallback,errorcallback);
*************************************************************************************/
VCScope.vcexterior.updateAll = function(inputArray, successcallback, errorcallback, markForUpload) {
	sync.log.trace("Entering VCScope.vcexterior.updateAll function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "VCScope.vcexterior.updateAll",  "updateAll", errorcallback)){
		return;
	}
	var dbname = "100004898461ca65a";
	var tbname = "vcexterior";
	var isError = false;
	var errObject = null;
	if(markForUpload == false || markForUpload == "false"){
		markForUpload="false"
	}
	else{
		markForUpload="true"
	}
	if((kony.sync.enableORMValidations)){

		var newInputArray = [];
		for (var i=0; ((inputArray) != null) && i < inputArray.length; i++ ){
			var v = inputArray[i];
			var valuestable = v.changeSet;
			var isEmpty = true;
			for(var key in valuestable){
				isEmpty = false;
				break;
			}
			if(isEmpty){
				errorcallback(kony.sync.getErrorTable(kony.sync.errorCodeNullValue,kony.sync.getErrorMessage(kony.sync.errorCodeNullValue)));
				return;
			}
			var wcs = v.whereClause;
			var twcs = wcs;
			if(kony.sync.attributeValidation(valuestable,"vcexterior",errorcallback,false)===false){
				return;
			}

			newInputArray[i] = [];
			newInputArray[i].changeSet = valuestable;
			newInputArray[i].whereClause = wcs;
		}
		inputArray = newInputArray;
		var connection = kony.sync.getConnectionOnly(dbname, dbname);
		kony.sync.startTransaction(connection, checkIntegrity, transactionSuccessCallback, transactionErrorCallback);

	}
	else{
		//copying by value
		var newInputArray = [];
		for (var i=0; ((inputArray) != null) && i < inputArray.length; i++ ){
		    var v = inputArray[i];
		    newInputArray[i] = kony.sync.CreateCopy(v);
		}
		inputArray = newInputArray;
		kony.sync.massUpdate(dbname, tbname,inputArray,successcallback,errorcallback,markForUpload, VCScope.vcexterior.getPKTable());
	}
	
		function transactionSuccessCallback(){
		sync.log.trace("Entering  VCScope.vcexterior.updateAll->transactionSuccessCallback");
		if(!isError){
			kony.sync.massUpdate(dbname, tbname,inputArray,successcallback,transactionErrorCallback,markForUpload, VCScope.vcexterior.getPKTable());
		}
	}

	function transactionErrorCallback(){
		if(errObject===false){
			//Sql statement error has occcurred
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
			
		}
		else if(errObject!==null){
			// Referential integrity error has occurred
			kony.sync.verifyAndCallClosure(errorcallback, errObject);
		}
		else{
			//Transaction error has occurred
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodeTransaction, kony.sync.getErrorMessage(kony.sync.errorCodeTransaction), null));
		}
	}
	//foreign key constraints validations
	function checkIntegrity(tx){
		sync.log.trace("Entering  VCScope.vcexterior.updateAll->checkIntegrity");
		for (var i=0; ((inputArray) != null) && i < inputArray.length; i++ ){
			var relationshipMap={}; 
			relationshipMap = VCScope.vcexterior.getRelationshipMap(relationshipMap,inputArray[i].changeSet);
			sync.log.debug("Relationship Map for Integrity check created:", relationshipMap);
			errObject = kony.sync.checkIntegrityinTransaction(tx, relationshipMap, null);
			if(errObject===false){
				isError = true;
				return; 
			}
			if(errObject!==true){
				isError = true;
				kony.sync.rollbackTransaction(tx);
				return;
			}
		}
	}


}
/************************************************************************************
* Deletes vcexterior using primary key from the local Database. The record will be
* deleted from the enterprise datasource in the next Sync.
*************************************************************************************/
VCScope.vcexterior.prototype.deleteByPK = function(successcallback,errorcallback){
	sync.log.trace("Entering VCScope.vcexterior.prototype.deleteByPK function");
	var pks = this.getPKTable();
	VCScope.vcexterior.deleteByPK(pks,successcallback,errorcallback,this.markForUpload);
};
VCScope.vcexterior.deleteByPK = function(pks, successcallback,errorcallback, markForUpload){
	sync.log.trace("Entering VCScope.vcexterior.deleteByPK-> main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "VCScope.vcexterior.deleteByPK",  "deleteByPK", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = VCScope.vcexterior.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);
	var wcs = [];
	var isError = false;
	var pkNotFound = false;
	var twcs = [];
	var deletedRows;
	var record = "";
	if(VCScope.vcexterior.pkCheck(pks,wcs,errorcallback,"deleting")===false){
		return;
	}	
	twcs = kony.sync.CreateCopy(wcs);
	function vcexteriorTransactionCallback(tx){
		sync.log.trace("Entering VCScope.vcexterior.deleteByPK->vcexterior_PKPresent successcallback");
		record = kony.sync.getOriginalRow(tx, tbname, wcs, errorcallback);
		if(record===false){
			isError = true;
			return;
		}
		if (null !== record) {
		}else{
			pkNotFound = true;
		}
		var deletedRows = kony.sync.remove(tx, tbname, wcs, false, markForUpload, null);
			if(deletedRows === false){
				isError = true;
			}
	}
	
	function vcexteriorErrorCallback(){
		sync.log.error("Entering VCScope.vcexterior.deleteByPK->relationship failure callback");
		if(isError === false){
			kony.sync.verifyAndCallClosure(errorcallback);
		}
		if(kony.sync.errorObject != null){
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
		}

	}
	function vcexteriorSuccessCallback(){
		sync.log.trace("Entering VCScope.vcexterior.deleteByPK->relationship success callback");
		if(pkNotFound === true){
			kony.sync.verifyAndCallClosure(pkNotFoundErrCallback);
			return;
		}
		
		if(!isError){
			kony.sync.verifyAndCallClosure(successcallback, {rowsdeleted:1});
		}
	}
	
	function pkNotFoundErrCallback(){
		sync.log.error("Entering VCScope.vcexterior.deleteByPK->PK not found callback");
		kony.sync.pkNotFoundErrCallback(errorcallback,tbname);
	}
		
		var dbconnection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
		if(dbconnection != null){
			kony.sync.startTransaction(dbconnection, vcexteriorTransactionCallback, vcexteriorSuccessCallback, vcexteriorErrorCallback, "Single Execute");
		}

};

/************************************************************************************
* Deletes vcexterior(s) using where clause from the local Database. The record(s)
* will be deleted from the enterprise datasource in the next Sync.
* e.g. VCScope.vcexterior.remove("where regno like 'A%'", successcallback,errorcallback, true);
*************************************************************************************/
VCScope.vcexterior.remove = function(wcs, successcallback,errorcallback, markForUpload){
	sync.log.trace("Entering VCScope.vcexterior.remove->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "VCScope.vcexterior.remove",  "remove", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = VCScope.vcexterior.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);
	wcs = kony.sync.validateWhereClause(wcs);
	var twcs = wcs;
	var isError = false;
	var rowsDeleted;

	function vcexterior_removeTransactioncallback(tx){
		wcs = " " + wcs;
		rowsDeleted = kony.sync.deleteBatch(tx, tbname, wcs, false, markForUpload, errorcallback)
		if(rowsDeleted === false){
			isError = true;
		}
	}
	function vcexterior_removeSuccess(){
		sync.log.trace("Entering VCScope.vcexterior.remove->vcexterior_removeSuccess function");

		if(!isError){
			kony.sync.verifyAndCallClosure(successcallback, rowsDeleted);
		}
	}
	function errorcallbackWrapper(){
		sync.log.trace("Entering VCScope.vcexterior.remove->error callback function");
		if(!isError){
			kony.sync.showTransactionError(errorcallback);
		}
		if(kony.sync.errorObject != null){
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
		}
	}
	function deleteEntity(){
		sync.log.trace("Entering VCScope.vcexterior.remove->delete Entity function");
		var connection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
		if(connection != null){
			kony.sync.startTransaction(connection, vcexterior_removeTransactioncallback, vcexterior_removeSuccess, errorcallbackWrapper);
		}
	}
	deleteEntity();
};

/************************************************************************************
* Deletes vcexterior using primary key from the local Database. This will
* not have any effect in enterprise datasource in subsequent sync cycles
*************************************************************************************/
VCScope.vcexterior.prototype.removeDeviceInstanceByPK = function(successcallback,errorcallback){
	sync.log.trace("Entering VCScope.vcexterior.prototype.removeDeviceInstanceByPK function");
	var pks = this.getPKTable();
	VCScope.vcexterior.removeDeviceInstanceByPK(pks,successcallback,errorcallback);
};
VCScope.vcexterior.removeDeviceInstanceByPK = function(pks, successcallback,errorcallback){
	sync.log.trace("Entering VCScope.vcexterior.removeDeviceInstanceByPK function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "VCScope.vcexterior.removeDeviceInstanceByPK",  "removeDeviceInstanceByPK", errorcallback)){
		return;
	}	
	var dbname = kony.sync.getDBName();
	var tbname = VCScope.vcexterior.getTableName();
	var wcs = [];
	var isError = false;
	var pkNotFound = false;
	var deletedRows;
	if(VCScope.vcexterior.pkCheck(pks,wcs,errorcallback,"deleting")===false){
		return;
	}
	
	function vcexteriorTransactionCallback(tx){
		sync.log.trace("Entering VCScope.vcexterior.removeDeviceInstanceByPK -> vcexteriorTransactionCallback");
		var record = kony.sync.getOriginalRow(tx, tbname, wcs, errorcallback);
		if(null !== record && false !=record) {
			deletedRows = kony.sync.remove(tx, tbname, wcs, true, null, null);
			if(deletedRows === false){
				isError = true;
			}
		}else{
			pkNotFound = true;
		}
	}
	
	function vcexteriorErrorCallback(){
		sync.log.error("Entering VCScope.vcexterior.removeDeviceInstanceByPK -> vcexteriorErrorCallback");
		if(isError === false){
			kony.sync.verifyAndCallClosure(errorcallback);
		}
		if(kony.sync.errorObject != null){
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
		}
	}
	
	function vcexteriorSuccessCallback(){
		sync.log.trace("Entering VCScope.vcexterior.removeDeviceInstanceByPK -> vcexteriorSuccessCallback");
		if(pkNotFound === true){
			kony.sync.verifyAndCallClosure(pkNotFoundErrCallback);
			return;
		}
		
		if(!isError){
			kony.sync.verifyAndCallClosure(successcallback, {rowsdeleted:1});
		}
	}
	
	function pkNotFoundErrCallback(){
		sync.log.error("Entering VCScope.vcexterior.removeDeviceInstanceByPK -> PK not found callback");
		kony.sync.pkNotFoundErrCallback(errorcallback,tbname);
	}
	
	var dbconnection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
	if(dbconnection != null){
		kony.sync.startTransaction(dbconnection, vcexteriorTransactionCallback, vcexteriorSuccessCallback, vcexteriorErrorCallback, "Single Execute");
	}

};

/************************************************************************************
* Deletes vcexterior(s) using where clause from the local Database. This will
* not have any effect in enterprise datasource in subsequent sync cycles
*************************************************************************************/
VCScope.vcexterior.removeDeviceInstance = function(wcs, successcallback,errorcallback){
	sync.log.trace("Entering VCScope.vcexterior.removeDeviceInstance->main function");
	var dbname = kony.sync.getDBName();
	var tbname = VCScope.vcexterior.getTableName();
	wcs = kony.sync.validateWhereClause(wcs);
	var twcs = wcs;
	var isError = false;
	var rowsDeleted;

	function vcexterior_removeTransactioncallback(tx){
		wcs = " " + wcs;
		rowsDeleted = kony.sync.deleteBatch(tx, tbname, wcs, true, null, errorcallback)
		if(rowsDeleted === false){
			isError = true;
		}
	}
	function vcexterior_removeSuccess(){
		sync.log.trace("Entering VCScope.vcexterior.remove->vcexterior_removeSuccess function");

		if(!isError){
			kony.sync.verifyAndCallClosure(successcallback, rowsDeleted);
		}
	}
	function errorcallbackWrapper(){
		sync.log.trace("Entering VCScope.vcexterior.remove->error callback function");
		if(!isError){
			kony.sync.showTransactionError(errorcallback);
		}
		if(kony.sync.errorObject != null){
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
		}
	}
	function deleteEntity(){
		sync.log.trace("Entering VCScope.vcexterior.remove->delete Entity function");
		var connection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
		if(connection != null){
			kony.sync.startTransaction(connection, vcexterior_removeTransactioncallback, vcexterior_removeSuccess, errorcallbackWrapper);
		}
	}
	deleteEntity();
};

/************************************************************************************
* Retrieves vcexterior using primary key from the local Database. 
*************************************************************************************/
VCScope.vcexterior.prototype.getAllDetailsByPK = function(successcallback,errorcallback){
	sync.log.trace("Entering VCScope.vcexterior.prototype.getAllDetailsByPK function");
	var pks = this.getPKTable();
	VCScope.vcexterior.getAllDetailsByPK(pks,successcallback,errorcallback);
};
VCScope.vcexterior.getAllDetailsByPK = function(pks, successcallback,errorcallback){
	sync.log.trace("Entering VCScope.vcexterior.getAllDetailsByPK-> main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "VCScope.vcexterior.getAllDetailsByPK",  "getAllDetailsByPK", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = VCScope.vcexterior.getTableName();
	var wcs = [];
	if(VCScope.vcexterior.pkCheck(pks,wcs,errorcallback,"searching")===false){
		return;
	}
	twcs = kony.sync.CreateCopy(wcs);
	var query = kony.sync.qb_createQuery();
				kony.sync.qb_select(query, null);
				kony.sync.qb_from(query, tbname);
				kony.sync.qb_where(query, wcs);
	var query_compile = kony.sync.qb_compile(query);
	var sql = query_compile[0];
	var params = query_compile[1];
	function mySuccCallback(res){
		sync.log.trace("Entering VCScope.vcexterior.getAllDetailsByPK-> success callback function");
		successcallback(VCScope.vcexterior.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
	}	
	kony.sync.single_select_execute(dbname, sql, params, mySuccCallback, errorcallback);
};






/************************************************************************************
* Retrieves vcexterior(s) using where clause from the local Database. 
* e.g. VCScope.vcexterior.find("where regno like 'A%'", successcallback,errorcallback);
*************************************************************************************/
VCScope.vcexterior.find = function(wcs, successcallback,errorcallback){
	sync.log.trace("Entering VCScope.vcexterior.find function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "VCScope.vcexterior.find",  "find", errorcallback)){
		return;
	}
	//wcs will be a string formed by the user.
	var dbname = kony.sync.getDBName();
	var tbname = VCScope.vcexterior.getTableName();
	wcs = kony.sync.validateWhereClause(wcs);
	var sql = "select * from \"" + tbname + "\" " + wcs;
	function mySuccCallback(res){
		kony.sync.verifyAndCallClosure(successcallback, VCScope.vcexterior.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
	}
	kony.sync.single_select_execute(dbname, sql, null, mySuccCallback, errorcallback);
};

/************************************************************************************
* Marks instance of vcexterior with given primary key for upload. This will 
* enable deferred records to merge with the enterprise datasource in the next Sync.
*************************************************************************************/
VCScope.vcexterior.prototype.markForUploadbyPK = function(successcallback, errorcallback){
	sync.log.trace("Entering VCScope.vcexterior.prototype.markForUploadbyPK function");
	var pks = this.getPKTable();
	VCScope.vcexterior.markForUploadbyPK(pks, successcallback, errorcallback);
};
VCScope.vcexterior.markForUploadbyPK = function(pks, successcallback, errorcallback){
	sync.log.trace("Entering VCScope.vcexterior.markForUploadbyPK function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "VCScope.vcexterior.markForUploadbyPK",  "markForUploadbyPK", errorcallback)){
		return;
	}	
	var dbname = kony.sync.getDBName();
	var tbname = VCScope.vcexterior.getTableName();
	var isError = false;
	var recordsFound = false;
	var recordsMarkedForUpload = 0;
	var wcs = [];
	if(VCScope.vcexterior.pkCheck(pks, wcs, errorcallback, "marking for upload by PK")===false){
		return;
	}

	function markRecordForUpload(tx, record){
		var versionMapMain = [];
		versionMapMain[kony.sync.mainTableChangeTypeColumn] = kony.sync.getChangeTypeForUploadTrue(record[kony.sync.historyTableChangeTypeColumn]);
		var scopename = kony.sync.scopes.syncTableScopeDic[tbname];
		var versionNo = kony.sync.getseqnumber(tx, scopename);
		if(versionNo === false){
			return false;
		}
		versionMapMain[kony.sync.historyTableSyncVersionColumn] = versionNo.versionnumber;
		var query = kony.sync.qb_createQuery();
					kony.sync.qb_update(query, tbname);
					kony.sync.qb_set(query,versionMapMain);
					kony.sync.qb_where(query, wcs);
		var query_compile = kony.sync.qb_compile(query);
		var sql = query_compile[0];
		var params = query_compile[1];
		return kony.sync.executeSql(tx, sql, params);		
	}
	
	function markRecordForUploadHistory(tx, record){
		var versionMap = [];
		versionMap[kony.sync.historyTableChangeTypeColumn] = kony.sync.getChangeTypeForUploadTrue(record[kony.sync.historyTableChangeTypeColumn]);
		var scopename = kony.sync.scopes.syncTableScopeDic[tbname];
		var versionNo = kony.sync.getseqnumber(tx, scopename);
		if(versionNo === false){
			return false;
		}
		var twcs = [];
		twcs = wcs;
		kony.table.insert(twcs,{key : kony.sync.historyTableChangeTypeColumn, value : record[kony.sync.historyTableChangeTypeColumn], optype : "EQ",comptype : "AND"});
		versionMap[kony.sync.historyTableSyncVersionColumn] = versionNo.versionnumber;
		var query = kony.sync.qb_createQuery();
					kony.sync.qb_update(query, tbname + kony.sync.historyTableName);
					kony.sync.qb_set(query,versionMap);
					kony.sync.qb_where(query, twcs);
		kony.table.remove(twcs);
		var query_compile = kony.sync.qb_compile(query);
		var sql = query_compile[0];
		var params = query_compile[1];
		return kony.sync.executeSql(tx, sql, params);
	}
	
	function single_transaction_callback (tx){
		var query = kony.sync.qb_createQuery();
					kony.sync.qb_select(query, [kony.sync.historyTableChangeTypeColumn]);
					kony.sync.qb_from(query, tbname);
					kony.sync.qb_where(query, wcs);
		var query_compile = kony.sync.qb_compile(query);
		var sql = query_compile[0];
		var params = query_compile[1];
		var resultSet = kony.sync.executeSql(tx, sql, params);
		if(resultSet === false){
			isError = true;
			return;
		}

		var num_records = resultSet.rows.length;
		if(num_records > 0){
			recordsFound = true;
			var record = kony.db.sqlResultsetRowItem(tx, resultSet, 0);
			var changeType = record[kony.sync.mainTableChangeTypeColumn];
			if(!kony.sync.isNullOrUndefined(changeType) && kony.string.startsWith(""+changeType,"9")){
				recordsMarkedForUpload = 1;
				if(markRecordForUpload(tx, record) === false){
					isError = true;
					return;
				}
			}
		}
					
				
		var query1 =kony.sync.qb_createQuery();
					kony.sync.qb_select(query1, [kony.sync.historyTableChangeTypeColumn]);
					kony.sync.qb_from(query1, tbname + kony.sync.historyTableName);
					kony.sync.qb_where(query1, wcs);
		var query1_compile = kony.sync.qb_compile(query1);
		var sql1 = query1_compile[0];
		var params1 = query1_compile[1];
		var resultSet1 = kony.sync.executeSql (tx, sql1, params1);
		if(resultSet1!==false){
			var num_records = resultSet1.rows.length;
			for(var i = 0; i <= num_records - 1; i++ ){
				var record = kony.db.sqlResultsetRowItem(tx, resultSet1, i);
				if(markRecordForUploadHistory(tx, record) === false){
					isError = true;
					return;
				}
				recordsFound = true;
			}
		}
		else{
			isError = true;
		}
	}
	function single_transaction_success_callback(){
		if(recordsFound === true){
			kony.sync.verifyAndCallClosure(successcallback , {count:recordsMarkedForUpload});
		}
		else{
			kony.sync.pkNotFoundErrCallback(errorcallback, tbname);
		}
	}
	
	function single_transaction_error_callback(res){
		if (!isError) {
			kony.sync.showTransactionError(errorcallback);
		}else{
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
		}
	}
	
	var connection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
	if(connection != null){
		kony.sync.startTransaction(connection, single_transaction_callback, single_transaction_success_callback, single_transaction_error_callback);
	}
};

/************************************************************************************
* Marks instance(s) of vcexterior matching given where clause for upload. This will 
* enable deferred records to merge with the enterprise datasource in the next Sync.
*************************************************************************************/
VCScope.vcexterior.markForUpload = function(wcs, successcallback,errorcallback){
	sync.log.trace("Entering VCScope.vcexterior.markForUpload->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "VCScope.vcexterior.markForUpload",  "markForUpload", errorcallback)){
		return;
	}	
	var dbname = kony.sync.getDBName();
	var tbname = VCScope.vcexterior.getTableName();
	var isError = false;
	var num_records_main = 0;
	wcs = kony.sync.validateWhereClause(wcs);
	if(!kony.sync.isNull(wcs) && !kony.sync.isEmptyString(wcs)) {
		wcs = wcs + " and " + kony.sync.historyTableChangeTypeColumn + " like '9%'";
	}else{	
		wcs = "where " + kony.sync.historyTableChangeTypeColumn + " like '9%'";
	}
	
	function markRecordForUpload(tx, record){
		var versionMapMain = [];
		versionMapMain[kony.sync.mainTableChangeTypeColumn] = kony.sync.getChangeTypeForUploadTrue(record[kony.sync.historyTableChangeTypeColumn]);
		var scopename = kony.sync.scopes.syncTableScopeDic[tbname];
		var versionNo = kony.sync.getseqnumber(tx, scopename);
		if(versionNo === false){
			return false;
		}
		versionMapMain[kony.sync.historyTableSyncVersionColumn] = versionNo.versionnumber;
		var query = kony.sync.qb_createQuery();
					kony.sync.qb_update(query, tbname);
					kony.sync.qb_set(query,versionMapMain);
		var query_compile = kony.sync.qb_compile(query);
		var sql = query_compile[0] + " " + wcs;
		var params = query_compile[1];
		if(kony.sync.executeSql(tx, sql, params) === false){
			return false;
		}
	}
	
	function markRecordForUploadHistory(tx, record){
		var versionMap = [];
		versionMap[kony.sync.historyTableChangeTypeColumn] = kony.sync.getChangeTypeForUploadTrue(record[kony.sync.historyTableChangeTypeColumn]);
		var scopename = kony.sync.scopes.syncTableScopeDic[tbname];
		var versionNo = kony.sync.getseqnumber(tx, scopename);
		if(versionNo === false){
			return false;
		}
		var twcs = "";
		twcs = wcs;
		twcs = twcs + " AND " + kony.sync.historyTableChangeTypeColumn + " = " + record[kony.sync.historyTableChangeTypeColumn];
		versionMap[kony.sync.historyTableSyncVersionColumn] = versionNo.versionnumber;
		
		var query = kony.sync.qb_createQuery();
					kony.sync.qb_update(query, tbname + kony.sync.historyTableName);
					kony.sync.qb_set(query,versionMap);
		var query_compile = kony.sync.qb_compile(query);
		var sql = query_compile[0]  + " " + twcs;
		var params = query_compile[1];
		if(kony.sync.executeSql(tx, sql, params) === false){
			return false;
		}
	}
	
	function single_transaction_callback (tx){
		sync.log.trace("Entering VCScope.vcexterior.markForUpload->single_transaction_callback");
		//updating main table
		var sql = "select " + kony.sync.historyTableChangeTypeColumn + " from \"" + tbname + "\" " + wcs ;
		var resultSet = kony.sync.executeSql (tx, sql, null);
		if(resultSet === false){
			isError = true;
			return;
		}
		
		num_records_main = resultSet.rows.length;
		for(var i = 0; i < num_records_main; i++ ){
			var record = kony.db.sqlResultsetRowItem(tx, resultSet, i);
			if(markRecordForUpload(tx, record) === false){
				isError = true;
				return;
			}
		}
		
		//updating history table
		var sql = "select " + kony.sync.historyTableChangeTypeColumn + " from " + tbname + kony.sync.historyTableName + " " + wcs;
		var resultSet = kony.sync.executeSql (tx, sql, null);
		if(resultSet === false){
			isError = true;
			return;
		}

		var num_records = resultSet.rows.length;
		for ( var i = 0; i <= num_records - 1; i++ ){
			var record = kony.db.sqlResultsetRowItem(tx, resultSet, i);
			if(markRecordForUploadHistory(tx, record)=== false){
				isError = true;
				return;
			}
		}
	}
	
	function single_transaction_success_callback(){
		sync.log.trace("Entering VCScope.vcexterior.markForUpload->single_transaction_success_callback");
		kony.sync.verifyAndCallClosure(successcallback, {count:num_records_main});
	}
	
	function single_transaction_error_callback(){
		sync.log.error("Entering VCScope.vcexterior.markForUpload->single_transaction_error_callback");
		if(!isError) {
			kony.sync.showTransactionError(errorcallback);
		}else{
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
		}
	}
	
	var connection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
	if(connection != null){
		kony.sync.startTransaction(connection, single_transaction_callback, single_transaction_success_callback, single_transaction_error_callback);
	}
};

/************************************************************************************
* Retrieves instance(s) of vcexterior pending for upload. Records are marked for
* pending upload if they have been updated or created locally and the changes have
* not been merged with enterprise datasource.
*************************************************************************************/
VCScope.vcexterior.getPendingUpload = function(wcs, successcallback,errorcallback){
	sync.log.trace("Entering VCScope.vcexterior.getPendingUpload->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = VCScope.vcexterior.getTableName();
	var currentversion = kony.sync.getCurrentVersionNumber(tbname);
	var sql;
	if(typeof(wcs) === "string" && wcs != null){
		wcs = kony.sync.validateWhereClause(wcs);
		sql = "select * from \"" + tbname + "\" "+ wcs + " and " + kony.sync.mainTableChangeTypeColumn + " is not null AND "+kony.sync.mainTableChangeTypeColumn+" <> -1 AND "+kony.sync.mainTableSyncVersionColumn+" = "+currentversion+" AND "+kony.sync.mainTableChangeTypeColumn+" NOT LIKE '9%'"; 
	}else{
		errorcallback = successcallback;
		successcallback = wcs;
		sql = "select * from \"" + tbname + "\" WHERE " + kony.sync.mainTableChangeTypeColumn + " is not null AND "+kony.sync.mainTableChangeTypeColumn+" <> -1 AND "+kony.sync.mainTableSyncVersionColumn+" = "+currentversion+" AND "+kony.sync.mainTableChangeTypeColumn+" NOT LIKE '9%'"; 
	}
	kony.sync.single_select_execute(dbname, sql, null, mySuccesscallback, errorcallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering VCScope.vcexterior.getPendingUpload->successcallback function");
		kony.sync.verifyAndCallClosure(successcallback, VCScope.vcexterior.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
	}
};

/************************************************************************************
* Retrieves instance(s) of vcexterior pending for acknowledgement. This is relevant
* when the SyncObject is part of the SyncScope whose SyncStrategy is PersistentSync.
* In persistent Sync the  records in the local database are put into a pending 
* acknowledgement state after an upload.
*************************************************************************************/
VCScope.vcexterior.getPendingAcknowledgement = function(successcallback, errorcallback){
	sync.log.trace("Entering VCScope.vcexterior.getPendingAcknowledgement->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = VCScope.vcexterior.getTableName();
	var currentversion = kony.sync.getCurrentVersionNumber(tbname);
	var mysql="select * from \""+tbname+"\" WHERE "+kony.sync.mainTableChangeTypeColumn+ " is not null AND "+kony.sync.mainTableChangeTypeColumn+" <> -1 AND "+kony.sync.mainTableSyncVersionColumn+" <> "+currentversion+" AND "+kony.sync.mainTableChangeTypeColumn+" NOT LIKE '9%'"; 
	kony.sync.single_select_execute(dbname, mysql, null, mySuccesscallback, errorcallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering VCScope.vcexterior.getPendingAcknowledgement success callback function");
		kony.sync.verifyAndCallClosure(successcallback, VCScope.vcexterior.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
	}
};

/************************************************************************************
* Retrieves instance(s) of vcexterior deferred for upload.
*************************************************************************************/
VCScope.vcexterior.getDeferredUpload = function(wcs,successcallback,errorcallback){
	sync.log.trace("Entering VCScope.vcexterior.getDeferredUpload->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = VCScope.vcexterior.getTableName();
	var sql;
	if(typeof(wcs) === "string" && wcs != null ){
		wcs = kony.sync.validateWhereClause(wcs);
		sql = "select * from \"" + tbname +  "\" " + wcs + " and " + kony.sync.mainTableChangeTypeColumn+ " is not null AND "+kony.sync.mainTableChangeTypeColumn+" <> -1 AND "+kony.sync.mainTableChangeTypeColumn+" LIKE '9%'";
	}else{
		errorcallback = successcallback;
		successcallback = wcs;
		sql="select * from \""+tbname+"\" WHERE "+kony.sync.mainTableChangeTypeColumn+ " is not null AND "+kony.sync.mainTableChangeTypeColumn+" <> -1 AND "+kony.sync.mainTableChangeTypeColumn+" LIKE '9%'"; 
	}
	
	kony.sync.single_select_execute(dbname, sql, null, mySuccesscallback, errorcallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering VCScope.vcexterior.getDeferredUpload->success callback function");
		kony.sync.verifyAndCallClosure(successcallback, VCScope.vcexterior.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
	}	
};

/************************************************************************************
* Rollbacks all changes to vcexterior in local database to last synced state
*************************************************************************************/
VCScope.vcexterior.rollbackPendingLocalChanges = function(successcallback, errorcallback){
	sync.log.trace("Entering VCScope.vcexterior.rollbackPendingLocalChanges->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = VCScope.vcexterior.getTableName();
	kony.sync.konySyncRollBackPendingChanges(tbname, dbname, null, mySuccesscallback, errorcallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering VCScope.vcexterior.rollbackPendingLocalChanges->main function");
		kony.sync.verifyAndCallClosure(successcallback, res);
	}		
};

/************************************************************************************
* Rollbacks changes to vcexterior's record with given primary key in local 
* database to last synced state
*************************************************************************************/
VCScope.vcexterior.prototype.rollbackPendingLocalChangesByPK = function(successcallback,errorcallback){
	sync.log.trace("Entering VCScope.vcexterior.prototype.rollbackPendingLocalChangesByPK function");
	var pks = this.getPKTable();
	VCScope.vcexterior.rollbackPendingLocalChangesByPK(pks,successcallback,errorcallback);
};
VCScope.vcexterior.rollbackPendingLocalChangesByPK = function(pks, successcallback, errorcallback){
	sync.log.trace("Entering VCScope.vcexterior.rollbackPendingLocalChangesByPK->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "VCScope.vcexterior.rollbackPendingLocalChangesByPK",  "rollbackPendingLocalChangesByPK", errorcallback)){
		return;
	}	
	var dbname = kony.sync.getDBName();
	var tbname = VCScope.vcexterior.getTableName();
	var wcs = [];
	if(VCScope.vcexterior.pkCheck(pks,wcs,errorcallback,"rollbacking")===false){
		return;
	}	
	kony.sync.konySyncRollBackPendingChanges(tbname, dbname, wcs, mySuccesscallback, pkNotFoundErrCallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering VCScope.vcexterior.rollbackPendingLocalChangesByPK->success callback function");
		kony.sync.verifyAndCallClosure(successcallback, res);
	}	
	function pkNotFoundErrCallback(){
		sync.log.error("Entering VCScope.vcexterior.rollbackPendingLocalChangesByPK->PK not found callback");
		kony.sync.pkNotFoundErrCallback(errorcallback,tbname);
	}
};

/************************************************************************************
* isRecordDeferredForUpload returns true or false depending on whether vcexterior's record  
* with given primary key got deferred in last sync
*************************************************************************************/
VCScope.vcexterior.prototype.isRecordDeferredForUpload = function(successcallback,errorcallback){
	sync.log.trace("Entering  VCScope.vcexterior.prototype.isRecordDeferredForUpload function");
	var pks = this.getPKTable();
	VCScope.vcexterior.isRecordDeferredForUpload(pks,successcallback,errorcallback);
};
VCScope.vcexterior.isRecordDeferredForUpload = function(pks, successcallback, errorcallback){
	sync.log.trace("Entering VCScope.vcexterior.isRecordDeferredForUpload->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "VCScope.vcexterior.isRecordDeferredForUpload",  "isRecordDeferredForUpload", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = VCScope.vcexterior.getTableName();
	var wcs = [] ;
	var flag;
	if(VCScope.vcexterior.pkCheck(pks,wcs,errorcallback,"selecting")===false){
		return;
	}
	var twcs = [];
	twcs = kony.sync.CreateCopy(wcs);
	kony.table.insert(twcs, {
			key : kony.sync.mainTableChangeTypeColumn,
			value : "9%",
			optype : "LIKE",
			comptype : "AND"
		});
	var query = kony.sync.qb_createQuery();
				kony.sync.qb_select(query, null);
				kony.sync.qb_from(query, tbname);
				kony.sync.qb_where(query, twcs);
	var query_compile = kony.sync.qb_compile(query);
	var sql = query_compile[0];
	var params = query_compile[1];
	kony.sync.single_select_execute(dbname, sql, params, mySuccesscallback, errorcallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering VCScope.vcexterior.isRecordDeferredForUpload->successcallback function");
		if(res.length === 1){
			flag = true;
		}
		else{
			flag = false;
		}
		kony.sync.verifyAndCallClosure(successcallback, {deferred:flag});
	}
};

/************************************************************************************
* isRecordPendingForUpload returns true or false depending on whether vcexterior's record  
* with given primary key is pending for upload
*************************************************************************************/
VCScope.vcexterior.prototype.isRecordPendingForUpload = function(successcallback,errorcallback){
	sync.log.trace("Entering  VCScope.vcexterior.prototype.isRecordPendingForUpload function");
	var pks = this.getPKTable();
	VCScope.vcexterior.isRecordPendingForUpload(pks,successcallback,errorcallback);
};
VCScope.vcexterior.isRecordPendingForUpload = function(pks, successcallback, errorcallback){
	sync.log.trace("Entering VCScope.vcexterior.isRecordPendingForUpload->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "VCScope.vcexterior.isRecordPendingForUpload",  "isRecordPendingForUpload", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = VCScope.vcexterior.getTableName();
	var wcs = [] ;
	var flag;
	if(VCScope.vcexterior.pkCheck(pks,wcs,errorcallback,"selecting")===false){
		return;
	}
	var twcs = [];
	twcs = kony.sync.CreateCopy(wcs);
	kony.table.insert(twcs, {
			key : kony.sync.mainTableChangeTypeColumn,
			value : "9%",
			optype : "NOT LIKE",
			comptype : "AND"
		});
	var query = kony.sync.qb_createQuery();
				kony.sync.qb_select(query, null);
				kony.sync.qb_from(query, tbname);
				kony.sync.qb_where(query, twcs);
	var query_compile = kony.sync.qb_compile(query);
	var sql = query_compile[0];
	var params = query_compile[1];
	kony.sync.single_select_execute(dbname, sql, params, mySuccesscallback, errorcallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering VCScope.vcexterior.isRecordPendingForUpload->successcallback function");
		if(res.length === 1){
			flag = true;
		}
		else{
			flag = false;
		}
		kony.sync.verifyAndCallClosure(successcallback, {pending:flag});
	}
};




/************************************************************************************
* Start of helper functions used internally, not to be used as ORMs
*************************************************************************************/

//Deletes all the dependant tables in the relationship tables.Need to pass transaction handler as input
VCScope.vcexterior.removeCascade = function(tx, wcs, errorcallback, markForUpload, isCascade, parentTable, isLocal){
	sync.log.trace("Entering VCScope.vcexterior.removeCascade function");
	var tbname = VCScope.vcexterior.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);
	function removeCascadeChildren(){
	}
	if(isCascade){
		if(removeCascadeChildren()===false){
			return false;
		}
		if(kony.sync.deleteBatch(tx, tbname, wcs, isLocal,markForUpload, null)===false){
			return false;
		}
		return true;
	}else{
		var sql = "select * from \"" + tbname + "\" " + wcs;
		var resultSet = kony.sync.executeSql(tx, sql, null);
		if(resultSet===false){
			return false;
		}	
		var num_records = resultSet.rows.length;
		if(num_records === 0){
			return true;
		}else{
			sync.log.error(kony.sync.getReferetialIntegrityDeleteErrMessg(tbname,tbname,tbname,parentTable));
			errorcallback(kony.sync.getErrorTable(kony.sync.errorCodeReferentialIntegrity,kony.sync.getReferetialIntegrityDeleteErrMessg(tbname,tbname,tbname,parentTable)));
			return false;
		}
	}
};


VCScope.vcexterior.convertTableToObject = function(res){
	sync.log.trace("Entering VCScope.vcexterior.convertTableToObject function");
	objMap = [];
	if(res!==null){
		for(var i in res){
			var obj = new VCScope.vcexterior();
			obj.extid = res[i].extid;
			obj.regno = res[i].regno;
			obj.email = res[i].email;
			obj.TaxDisc = res[i].TaxDisc;
			obj.TyreCondition = res[i].TyreCondition;
			obj.WheelNuts = res[i].WheelNuts;
			obj.NumberPlate = res[i].NumberPlate;
			obj.BodyCondition = res[i].BodyCondition;
			obj.SideStep = res[i].SideStep;
			obj.isDeleted = res[i].isDeleted;
			obj.lastModifiedDate = res[i].lastModifiedDate;
			obj.markForUpload = (Math.floor(res[i].konysyncchangetype/10)==9)? false:true;
			objMap[i] = obj;
		}
	}
	return objMap;
};

VCScope.vcexterior.filterAttributes = function(valuestable, insert){
	sync.log.trace("Entering VCScope.vcexterior.filterAttributes function");
	var attributeTable = {};
	attributeTable.extid = "extid";
	attributeTable.regno = "regno";
	attributeTable.email = "email";
	attributeTable.TaxDisc = "TaxDisc";
	attributeTable.TyreCondition = "TyreCondition";
	attributeTable.WheelNuts = "WheelNuts";
	attributeTable.NumberPlate = "NumberPlate";
	attributeTable.BodyCondition = "BodyCondition";
	attributeTable.SideStep = "SideStep";

	var PKTable = {};
	PKTable.extid = {}
	PKTable.extid.name = "extid";
	PKTable.extid.isAutoGen = true;
	var newvaluestable = {};
	for (var k in valuestable){
		var v = valuestable[k];
		if(kony.sync.isNull(attributeTable[k])) { 
			sync.log.warn("Ignoring the attribute " + k + " for the SyncObject vcexterior. "  + k + " not defined as an attribute in SyncConfiguration.");
		}else if(!kony.sync.isNull(PKTable[k])) {
			if(insert===false){
				sync.log.warn("Ignoring the primary key " + k + " for the SyncObject vcexterior. Primary Key should not be the part of the attributes to be updated in the local device database.");
			}else if(PKTable[k].isAutoGen){
				sync.log.warn("Ignoring the auto-generated primary key " + k + " for the SyncObject vcexterior. Auto-generated Primary Key should not be the part of the attributes to be inserted in the local device database.");
			}else{
				newvaluestable[k] = v;
			}
		}
		else{
			newvaluestable[k] = v;
		}
	}
	return newvaluestable;
};

VCScope.vcexterior.formOrderByClause = function(orderByMap){
	sync.log.trace("Entering VCScope.vcexterior.formOrderByClause function");
	if(!kony.sync.isNull(orderByMap)){
		var valuestable = kony.sync.convertOrderByMapToValuesTable(orderByMap);
		//var filteredValuestable = VCScope.vcexterior.filterAttributes(valuestable, true);
		return kony.sync.convertToValuesTableOrderByMap(orderByMap,valuestable);
	}
	return null;
};

VCScope.vcexterior.prototype.getValuesTable = function(isInsert){
	sync.log.trace("Entering VCScope.vcexterior.prototype.getValuesTable function");
	var valuesTable = {};
	if(isInsert===true){
		valuesTable.extid = this.extid;
	}
	valuesTable.regno = this.regno;
	valuesTable.email = this.email;
	valuesTable.TaxDisc = this.TaxDisc;
	valuesTable.TyreCondition = this.TyreCondition;
	valuesTable.WheelNuts = this.WheelNuts;
	valuesTable.NumberPlate = this.NumberPlate;
	valuesTable.BodyCondition = this.BodyCondition;
	valuesTable.SideStep = this.SideStep;
	return valuesTable;
};

VCScope.vcexterior.prototype.getPKTable = function(){
	sync.log.trace("Entering VCScope.vcexterior.prototype.getPKTable function");
	var pkTable = {};
	pkTable.extid = {key:"extid",value:this.extid};
	return pkTable;
};

VCScope.vcexterior.getPKTable = function(){
	sync.log.trace("Entering VCScope.vcexterior.getPKTable function");
	var pkTable = [];
	pkTable.push("extid");
	return pkTable;
};

VCScope.vcexterior.pkCheck = function(pks,wcs,errorcallback,opName){
	sync.log.trace("Entering VCScope.vcexterior.pkCheck function");
	var wc = [];
	if(kony.sync.isNull(pks)){
		sync.log.error("Primary Key extid not specified in  " + opName + "  an item in vcexterior");
		kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodePrimaryKeyNotSpecified,kony.sync.getPrimaryKeyNotSpecifiedMsg("extid",opName,"vcexterior")));
		return false;	
	}
	else if(kony.sync.isValidJSTable(pks)){
		if(!kony.sync.isNull(pks.extid)){
			if(!kony.sync.isNull(pks.extid.value)){
				wc.key = "extid";
				wc.value = pks.extid.value;
			}
			else{
				wc.key = "extid";
				wc.value = pks.extid;
			}
		}else{
			sync.log.error("Primary Key extid not specified in  " + opName + "  an item in vcexterior");
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodePrimaryKeyNotSpecified,kony.sync.getPrimaryKeyNotSpecifiedMsg("extid",opName,"vcexterior")));
			return false;
		}
	}
	else{
		wc.key = "extid";
		wc.value = pks;
	}	
	kony.table.insert(wcs,wc);
	return true;
};

VCScope.vcexterior.validateNull = function (valuestable,errorcallback){
	sync.log.trace("Entering VCScope.vcexterior.validateNull function");
	return true;
};

VCScope.vcexterior.validateNullInsert = function (valuestable,errorcallback){
	sync.log.trace("Entering VCScope.vcexterior.validateNullInsert function");
	return true;
};

VCScope.vcexterior.getRelationshipMap = function(relationshipMap,valuestable){
	sync.log.trace("Entering VCScope.vcexterior.getRelationshipMap function");
	var r1 = {};

	return relationshipMap;
};


VCScope.vcexterior.checkPKValueTables = function (valuetables)	{
	var checkPksNotNullFlag = true;
	for(var i = 0; i < valuetables.length; i++)	{
		if(kony.sync.isNull(valuetables[i])){
			checkPksNotNullFlag = false;
			break;
		}
	}
	return checkPksNotNullFlag;
};

VCScope.vcexterior.getTableName = function(){
	return "vcexterior";
};




// **********************************End vcexterior's helper methods************************