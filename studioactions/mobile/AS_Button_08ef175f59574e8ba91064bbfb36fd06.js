function AS_Button_08ef175f59574e8ba91064bbfb36fd06(eventobject) {
    function SHOW_ALERT__5948e992d6094bd5a5d187462fca45aa_True() {
        onClickLogout.call(this);
    }

    function SHOW_ALERT__5948e992d6094bd5a5d187462fca45aa_False() {
        dismissMessagePopup.call(this);
    }

    function SHOW_ALERT__5948e992d6094bd5a5d187462fca45aa_Callback(response) {
        if (response == true) {
            SHOW_ALERT__5948e992d6094bd5a5d187462fca45aa_True()
        } else {
            SHOW_ALERT__5948e992d6094bd5a5d187462fca45aa_False()
        };
    }
    kony.ui.Alert({
        "alertType": constants.ALERT_TYPE_CONFIRMATION,
        "alertTitle": "",
        "yesLabel": "Yes",
        "noLabel": "No",
        "alertIcon": "logout_icon.png",
        "message": "Are you sure want to log out the application?",
        "alertHandler": SHOW_ALERT__5948e992d6094bd5a5d187462fca45aa_Callback
    }, {
        "iconPosition": constants.ALERT_ICON_POSITION_LEFT
    })
}