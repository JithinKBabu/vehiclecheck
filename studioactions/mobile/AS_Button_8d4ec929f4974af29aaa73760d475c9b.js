function AS_Button_8d4ec929f4974af29aaa73760d475c9b(eventobject) {
    function SHOW_ALERT_ide_onClick_b41e43c3aaf147ec912760580561ff10_True() {
        onClickLogout.call(this);
    }

    function SHOW_ALERT_ide_onClick_b41e43c3aaf147ec912760580561ff10_False() {
        dismissMessagePopup.call(this);
    }

    function SHOW_ALERT_ide_onClick_b41e43c3aaf147ec912760580561ff10_Callback(response) {
        if (response == true) {
            SHOW_ALERT_ide_onClick_b41e43c3aaf147ec912760580561ff10_True()
        } else {
            SHOW_ALERT_ide_onClick_b41e43c3aaf147ec912760580561ff10_False()
        };
    }
    kony.ui.Alert({
        "alertType": constants.ALERT_TYPE_CONFIRMATION,
        "alertTitle": "",
        "yesLabel": "Yes",
        "noLabel": "No",
        "alertIcon": "logout_icon.png",
        "message": "Are you sure want to log out the application?",
        "alertHandler": SHOW_ALERT_ide_onClick_b41e43c3aaf147ec912760580561ff10_Callback
    }, {
        "iconPosition": constants.ALERT_ICON_POSITION_LEFT
    })
}